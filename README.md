# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : Repository for Demo Automation

* Version : 1.0.0

### How do I get set up? ###

* Summary of set up 

1. Install the java and maven in your system

2. Set Environment Variable for Java as  JAVA_Home = %PATH of JDK folder%.

3. Set %PATH of %JDK folder% and %PATH of %JRE folder% in the system variable.

4. Set %PATH of %Maven folder% in the system variable.

* Configuration :


1. Replace the project name across entire suite from "Demo" to "YourProjectName"

2. Update api.properties with appropriate key values for test configuration

3. Run build command as mentioned in the Deployment instructions section

4. Make sure to give environment name as a parameter before starting execution

5. Insert required test classes and test group in testng.xml

6. Execute the test suite.

* Deployment instructions 

    Execute command in order:
    
    mvn clean install -DskipTests=true -DDemo_AUTOMATION_VERSION=Demo_AUTOMATION_1.0.0
    
    mvn idea:clean idea:idea [If you use Intellij Idea as an IDE]
    
    mvn eclipse:clean eclipse:eclipse [If you use Eclipse as an IDE]
    
* How to run tests

1. Once we executed "mvn clean install -DskipTests=true -DDemo_AUTOMATION_VERSION=Demo_AUTOMATION_1.0.0" command

2. Go to "target\archive-folder" folder

3. Execute "run.bat" command

### HTML report Location ? ###


1. Once we executed run.bat command

2. Folder called output will be  created inside we can find HTML report with name " Demo-automation "

### Log file Location ? ###

1. Once we executed run.bat command

2. Folder called logs will be  created inside we can find log file with name " Demo-automation "



### How do Set it up for CMD ###

1. Change the Env in AbstractTest Config file location

2. Change Feature File Location and extent-config.xml location in RunCukesTest

3. Change Testng location in pom.xml

4. Driver Location

5. RunCukesTest locations


