Feature: Number_Generator Service 


@Configuration
  Scenario Outline:  To get the configuration
    Given RequestType "/number-generator" and Port <Port> and Method is <RequestMethod>
     When CONFIG
     Then "should get configuration of number genarator" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7001 | GET           |


@GeneratesNumber 
  Scenario Outline: Create number 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When configuration is given
     Then "Number should be generated" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7008 | POST          |

     
      
 @GenerateNestedNumber
  Scenario Outline: generates nested number
    Given RequestType "" and Port <Port> and Method is <RequestMethod> 
     When Generate Number with entityId and subEntityIds 
     Then "Number genertaed with subentityid" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
    	| 200        | 7008 | POST          |

 