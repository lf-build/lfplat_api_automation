Feature: Token generator Service 

  @IssueToken 
  Scenario Outline: Issue the token 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     Then "Token is isssued" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7066 | POST          | 
  
  @ValidateToken
  Scenario Outline: Validate the token 
    Given RequestType "/validate" and Port <Port> and Method is <RequestMethod> 
     When Token is issued
     Then "Token Validated" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7066 | POST          | 
  
