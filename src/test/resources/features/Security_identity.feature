Feature: Security Identity Service 

@CreateBackOfficeUser 
  Scenario Outline: Create Back Office User 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Create User with Role <Role>
     Then "User should be created" and Status Code should be <StatusCode>
    Examples: 
      | Role          | StatusCode | Port | RequestMethod | 
      | Administrator | 200        | 7018 | PUT           | 
  
  @LoginToBackOffice
  Scenario Outline: Login with the Created User 
    Given RequestType "/login" and Port <Port> and Method is <RequestMethod> 
     When Portal is <Portal>
     Then "User should be able to Login" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | Portal      | RequestMethod | 
      | 200        | 7018 | back-office | POST          | 
  
  @CheckCurrentUser
  Scenario Outline: Check the current User
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When User is LoggedIn
     Then "User details should be available" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7018 | GET           | 
  
  @ChangePassword
  Scenario Outline: Change Password 
    Given RequestType "/change-password" and Port <Port> and Method is <RequestMethod> 
     When User is LoggedIn
     Then "User should be able to change password" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7018 | PUT           | 
  
  @LogoutCurrentUser
  Scenario Outline: Logout the current User
    Given RequestType "/logout" and Port <Port> and Method is <RequestMethod>
     When User is LoggedIn
     Then "User should be logged out" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7018 | GET           | 
  
  @RequestResetPasswordToken
  Scenario Outline: Request Reset Password token 
    Given RequestType "/request-token" and Port <Port> and Method is <RequestMethod> 
     When Portal is <Portal>
     Then "User should be able to generate a new token" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | Portal      | RequestMethod | 
      | 202        | 7018 | back-office | POST          | 
  
  @GetUserDetailsByUsername
  Scenario Outline: Get User details by Username 
    Given RequestType "/user/by-username/" and Port <Port> and Method is <RequestMethod> 
     When Get active User
     Then "We should be able to get the User details" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7018 | GET           | 
  
  @ResetPassword
  Scenario Outline: Reset Password 
    Given RequestType "/reset-password/" and Port <Port> and Method is <RequestMethod> 
     When The token is requested
     Then "User should be able to reset password" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7018 | POST          | 
  
  @DeactivateUser
  Scenario Outline: Deactivate User 
    Given RequestType "/deactivate/" and Port <Port> and Method is <RequestMethod> 
     When Get active User
     Then "We should be able to Deactivate User" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7018 | PUT           | 
  
  @LoginforDeactivatedUser
  Scenario Outline: Login with the Deactivated User 
    Given RequestType "/login" and Port <Port> and Method is <RequestMethod> 
     When Portal is <Portal>
     Then "User should not be able to Login" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | Portal      | RequestMethod | 
      | 400        | 7018 | back-office | POST          | 
  
  @ActivateUser
  Scenario Outline: Activate User 
    Given RequestType "/activate/" and Port <Port> and Method is <RequestMethod> 
     When Get active User
     Then "We should be able to Deactivate User" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7018 | PUT           | 
  
  @BackOfficeLoginforActivatedUser
  Scenario Outline: Login with the Deactivated User 
    Given RequestType "/login" and Port <Port> and Method is <RequestMethod> 
     When Portal is <Portal>
     Then "User should not be able to Login" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | Portal      | RequestMethod | 
      | 200        | 7018 | back-office | POST          | 
  
  @GetUserDetailsById
  Scenario Outline: Get User details by Username 
    Given RequestType "/user/" and Port <Port> and Method is <RequestMethod> 
     When Get active User Id
     Then "We should be able to get the User details" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7018 | GET           | 
  
  @ValidationForCreateUser
  Scenario Outline: Get User details by Username 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Run Validation for :
      | Name                   | <Name>                   | 
      | Email                  | <Email>                  | 
      | Username               | <Username>               | 
      | Password               | <Password>               | 
      | Role                   | <Role>                   | 
      | PasswordExpirationDate | <PasswordExpirationDate> | 
     Then "We should be able to get the User details" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | Name   | Email                | Username | Password  | Role          | PasswordExpirationDate   | 
      | 200        | 7018 | GET           | blajee | gbalajee@yopmail.com | balajif  | Sigma@123 | Administrator | 2018-03-22T06:35:10.647Z | 
  
