Feature: LookUp Service 
  

  @GetsAllEntities
  Scenario Outline: Gets the entities 
    Given RequestType "/entities" and Port <Port> and Method is <RequestMethod>  
     When all the entries are done
     Then "all entity should be available" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7005 | GET           |
      
      
@GetLookupEntries 
  Scenario Outline: Gets the lookup entries 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Entity is given
     Then "Entries should be available" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7005 | GET           |
      
      
  
  @AddSpecifiedEntity
  Scenario Outline: Adds the specified entity 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When Entity is given
     Then "entity should be added" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  |
      | 200        | 7005 | POST           |
      
  
  @GetsLookupEntry
  Scenario Outline: Gets the lookup entry
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When Entity is given and code is <code>
     Then "should able to get lokup entries" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7005 | GET           |
         
      
  @DeleleSpecifiedEntity
  Scenario Outline: Deleles the specified entity
    Given RequestType "/<entity>/<code>" and Port <Port> and Method is <RequestMethod>
     When Entity is given and code is <code>
     Then "entity should be deleted" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod    | entity      | code                  |
      | 204        | 7005 | DELETE           | sampletest1 | additionalProp1       |
      
      
  @DelelesAll
  Scenario Outline: Deleles all Entity
    Given RequestType "/<entity>" and Port <Port> and Method is <RequestMethod>
     When Entity is given
     Then "entity should get deleted" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod    | entity     |
      | 204        | 7005 | DELETE           | sampletest |
       
  @GetbyLookupEntity
  Scenario Outline: Gets the lookup entries 
    Given RequestType "/entities/" and Port <Port> and Method is <RequestMethod> 
     When Entity is given
     Then "We should be able to get the lookup entries" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7005 | GET           |
      
  
  @CheckEntryExist
  Scenario Outline: lookup entry exist or not
    Given RequestType "/exist" and Port <Port> and Method is <RequestMethod> 
     When  code is <code>
     Then "return boolean values" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7005 | GET           |
      
      
  
  