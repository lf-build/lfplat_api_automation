Feature: Template Manager Service 

@AddTemplate 
  Scenario Outline: Add a Template
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When template name is given
     Then "Template should be added" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7014 | POST          | 
  
  @ProcessesTemplate
  Scenario Outline: Processes the template 
    Given RequestType "/process" and Port <Port> and Method is <RequestMethod> 
     When template should be added
     Then "template should get processed" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7014 | POST          | 
  
  @GetAllTemplate
  Scenario Outline: Get all template
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     Then "template details should be available" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7014 | GET           | 
  
  @GetTemplateProperties
  Scenario Outline: Get the template Properties 
    Given RequestType "/properties" and Port <Port> and Method is <RequestMethod> 
     When version and format is given
     Then "should get all the template properties" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7014 | GET           | 
  
  @GetSpecifiedTemplate
  Scenario Outline: Get Specified Template
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Template version and format is given
     Then " shouls get details of specified template" and Status Code should be <StatusCode>
    Examples:
      | StatusCode | Port | RequestMethod | 
      | 200        | 7014 | GET           | 
  
  @UpdateTemplate
  Scenario Outline: Update a template
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When Template version and format is given
     Then "Template should get updated" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7014 | PUT           | 
  
  @DeleteTemplate
  Scenario Outline: Get User details by Username 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When Template version and format is given
     Then "template should get deleted" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  | 
      | 204        | 7014 | DELETE         | 
  
 