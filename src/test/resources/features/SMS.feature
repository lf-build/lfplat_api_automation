Feature: SMS Service 

@SendspecifiedEntitytype 
  Scenario Outline: Sends the specified entitytype 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When SMS Template and entity is given
     Then "should send SMS" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7063 | POST          |
  
  @SendsSMS
  Scenario Outline: Sends the SMS
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When SMS entity is given
     Then "should send SMS" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7063 | POST          |
  
  