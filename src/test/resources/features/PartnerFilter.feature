Feature: Partner Filter Service 

@GetsAll
  Scenario Outline: Gets all
    Given RequestType "/all" and Port <Port> and Method is <RequestMethod>
     When gets all
     Then "should get all the details of partner" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7050 | GET           |

  
  @GetsByStatus
  Scenario Outline: Get  by status
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When Status is  given
     Then "should get the details of partner by status" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7050 | GET           |


  
  @GetAllByStatus
  Scenario Outline: Gets all by status
    Given RequestType "/status/" and Port <Port> and Method is <RequestMethod>
     When Status is  given
     Then "should get all the details of partner by status" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7050 | GET           | 


  