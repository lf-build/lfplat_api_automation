Feature: Data Attributes Service 

@SetsAttributeByName 
  Scenario Outline: Sets the attribute by name
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Name and secondary name is given
     Then "attributes by name are set successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7012 | POST          | 
  
  @SetsSchema
  Scenario Outline: Sets the schema 
    Given RequestType "/schema" and Port <Port> and Method is <RequestMethod> 
     When attribute is set
     Then "Schema is set successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7012 | POST          | 
  
  @SetsAttribute
  Scenario Outline: Sets the attribute
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When attributes are given
     Then "set successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7012 | POST          | 
  
  @GetsAllAttributes
  Scenario Outline: Gets all attributes
    Given RequestType "/all" and Port <Port> and Method is <RequestMethod> 
     When properties and attributes are set
     Then "should get all the attributes" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7012 | GET           | 
  
  @GetsAttributeByNames
  Scenario Outline:  Gets the attribute by names
    Given RequestType "/names" and Port <Port> and Method is <RequestMethod>
     When aatributes by name is set
     Then "should get the details of attribute by name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7012 | POST          | 
  
  @GetsAttributeBySpecificNames
  Scenario Outline: Gets the attribute by Specific names
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When name is given
     Then "should get the details of attribute specific name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7012 | GET           | 
  
  @GetsAttribute
  Scenario Outline: Gets the attribute 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When Name and secondary name is given
     Then "details getting succesfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7012 | GET           | 
  
  @RemovesAttribute
  Scenario Outline: Removes the attribute
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When attribute is already present
     Then "Attribute removed" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7012 | DELETE        | 
  
