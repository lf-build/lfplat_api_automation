Feature: Activity Log Service 

  @GetActivitiesWithProductId
  Scenario Outline: Gets the activities with productId 
    Given RequestType "/by/product/" and Port <Port> and Method is <RequestMethod>
     When Log Entries with ProductId Is Given
     Then "Should get activities" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7016 | GET           | 
  
  @GetAllActivitiesWithProductId
  Scenario Outline: Gets all activities with ProductId 
    Given RequestType "/all" and Port <Port> and Method is <RequestMethod> 
     When activityLog ProductId is given
     Then "Should get activities" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7016 | GET           | 
  
  @GetsActivitiesByTypeAndId
  Scenario Outline: Gets the activities by Type And Id
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Log Entries without ProductId
     Then "Should get activities" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7016 | GET           | 
  
  @GetsAllActivitiesWithoutProductId
  Scenario Outline: Gets all activities without productId
    Given RequestType "/all" and Port <Port> and Method is <RequestMethod>
     When Without ProductId
     Then "Should get activities" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7016 | GET           | 
  
  @GetsActivityAllCleanEntries
  Scenario Outline: Gets the activities.byType And Id all clean entries
    Given RequestType "/all/clean" and Port <Port> and Method is <RequestMethod>
     When Without ProductId
     Then "Should get activities" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7016 | GET           | 

  
  @GetsActivitiesByTagsWithProductid
  Scenario Outline: Gets the activities by tags with Productid 
    Given RequestType "/tag/" and Port <Port> and Method is <RequestMethod> 
     When tags withProductId
     Then "Should get activities" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7016 | GET           | 
  
  @GetsActivitiesByNotagsWithProductId
  Scenario Outline: Gets the activities by no tags with ProductId
    Given RequestType "/no-tags" and Port <Port> and Method is <RequestMethod> 
     When activityLog ProductId is given
     Then "Should get activities" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7016 | GET           | 
  
  @GetsActivitiesByTagsWithoutProductid
  Scenario Outline: Gets the activities by tags without Productid 
    Given RequestType "/tag/" and Port <Port> and Method is <RequestMethod> 
     When tags without ProductId
     Then "Should get activities" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7016 | GET           | 
  
  @GetsActivitiesByNotagsWithoutProductId
  Scenario Outline: Gets the activities by no tags without ProductId 
    Given RequestType "/no-tags" and Port <Port> and Method is <RequestMethod> 
     When Without ProductId
     Then "Should get activities" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7016 | GET           | 
  
  @GetsTagsWithProductId
  Scenario Outline: Gets the tags with ProductId
    Given RequestType "/tags" and Port <Port> and Method is <RequestMethod> 
     When activityLog ProductId is given
     Then "Should get activities" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7016 | GET           | 
  
  @GetsTagsWithoutProductId
  Scenario Outline: Gets the tags without ProductId
    Given RequestType "/tags" and Port <Port> and Method is <RequestMethod> 
     When Without ProductId
     Then "Should get activities" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7016 | GET           | 
  

  
