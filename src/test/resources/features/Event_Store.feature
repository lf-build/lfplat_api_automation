Feature: Event Store Service 

@GetsDataByIdentifier
  Scenario Outline: Gets the data by identifier
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When EventStore Id is given
     Then "User should get details" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7004 | GET           | 
  
