Feature: Consumer Service 

@GetReferenceEntityByIdAndType
  Scenario Outline: Get reference entity by entity id and entity type
    Given RequestType "/reference-entity/" and Port <Port> and Method is <RequestMethod>
     When EntityId and EntityType is given
     Then "getting details if refernece entity" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7070 | GET           |

      
@GetReferenceEntityByConsumerIDParams
  Scenario Outline: Get reference entity by consumer ids params
    Given RequestType "/reference-entity/by_consumerid" and Port <Port> and Method is <RequestMethod> 
     When ConsumerID id usimg paramas
     Then "getting datails of refernece entity" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7070 | POST          |
 

@GetConsumerByID
  Scenario Outline: Get consumer data according to specify by id
    Given RequestType "/consumer/" and Port <Port> and Method is <RequestMethod>
     When ConsumerID is <consumers>
     Then "getting details of consumer" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7070 | GET           |

  
  @GetDuplicateConsumerByField
  Scenario Outline: Get duplicate consumer data according to specify by field params
    Given RequestType "/consumer/duplicate/<field>" and Port <Port> and Method is <RequestMethod> 
     When field is <field>
     Then "getting details of duplicate consumer" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | field |
      | 200        | 7070 | POST          | null  |

  
  @GetDuplicateConsumerByFilter
  Scenario Outline: Get duplicate consumer data according to specify by filter by params
    Given RequestType "/consumer/duplicate" and Port <Port> and Method is <RequestMethod>
     When duplicate is done by filters
     Then "getting details of duplicate consumer" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7070 | POST          | 

  
  @GetReferenceEntityByID
  Scenario Outline: Get reference entity by id
    Given RequestType "/reference-entity/" and Port <Port> and Method is <RequestMethod> 
     When Id is <id>
     Then "getting details of reference entity" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7070 | GET           |
      
  
  @GetReferenceEntityByConsumerID
  Scenario Outline: Get reference entity by consumer id 
    Given RequestType "/reference-entity/by_consumerid/" and Port <Port> and Method is <RequestMethod> 
     When ConsumerID is <consumers>
     Then "getting datails if refernece entity" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7070 | GET           |


  
  