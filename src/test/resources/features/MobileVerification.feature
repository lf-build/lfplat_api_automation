Feature: Mobile Verification Service 


@InitiatesVerification 
  Scenario Outline: Initiates the mobile verification
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When entityType is <entityType>
     Then "initiated" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7062 | POST          |


  
  @VerifiesMobile
  Scenario Outline: Verifies the mobile
    Given RequestType "/verify/" and Port <Port> and Method is <RequestMethod> 
     When reference number is <referenceNumber> 
     Then "already verified" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 404        | 7062 | POST          |

  @GetsVerificationDetails
  Scenario Outline: Gets Mobile Verification details based on reference number
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When using refNumber
     Then "successfully getting the details" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7062 | GET           |

  
  