Feature: Email verification Service 

@InitiateEmailVerification 
  Scenario Outline: Initiate Email Verification
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When emailId is given
     Then "Should get reference number" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7061 | POST          | 
  
  @VerifiesEmail
  Scenario Outline: Verifies the email 
    Given RequestType "/verification/" and Port <Port> and Method is <RequestMethod> 
     When emailId is Intiated
     Then "email should be verified" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7061 | POST          | 
  
  @GetsVerificationStatus
  Scenario Outline: Gets the verification status
    Given RequestType "/verificationstatus/" and Port <Port> and Method is <RequestMethod>
     When email is verified
     Then "verification details should be available" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7061 | GET           | 
  
