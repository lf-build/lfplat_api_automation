Feature: Document Generator Service 


@GenerateSpecifiedTemplate
  Scenario Outline: Generates the specified template name 
    Given RequestType "/generate" and Port <Port> and Method is <RequestMethod>
     When templatename is <name>
     Then "Document should be generated with the speceified template name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7015 | POST          | 
  
  @GenerateSpecifiedFormat
  Scenario Outline: Generates the specified format. 
    Given RequestType "/<format>/generate" and Port <Port> and Method is <RequestMethod> 
     When format is <format>
     Then "file should get downloaded in specific format" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | format |
      | 200        | 7015 | POST          | Pdf    | 
      
      
  @GenerateHTmlFormat
  Scenario Outline: Generates the specified format. 
    Given RequestType "/<format>/generate" and Port <Port> and Method is <RequestMethod> 
     When format is <format>
     Then "Unable to generate document" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | format  |
      | 500        | 7015 | POST          | Html    |
  

