Feature: Product Rule Service 

@AddRuleDefinition
  Scenario Outline: Add rule definition by entity type and product id
    Given RequestType "/ruledefinition" and Port <Port> and Method is <RequestMethod>
     When ProductRule ProductId is given
     Then "rule defination is added" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | POST          | 
  
  @UpdateRuleDefinition
  Scenario Outline: Update rule definition by rule id
    Given RequestType "/ruledefinition" and Port <Port> and Method is <RequestMethod> 
     When Added rule defination
     Then "Updated rule defination" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | PUT           | 
  
  @GetRuleDefinitionById
  Scenario Outline: Get rule definition by id
    Given RequestType "/ruledefinition" and Port <Port> and Method is <RequestMethod>
     When Added rule defination
     Then "successfully getting details of rule edefination" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @GetAllRuleDefinitionsByRuleType
  Scenario Outline: Get all rule definitions by rule type
    Given RequestType "/ruledefinitions" and Port <Port> and Method is <RequestMethod> 
     When ruleType is given
     Then "getting details by ruletype" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
      
  @GetProductRuleDetail
  Scenario Outline: Get product rule detail by entity type and entity id
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When entity type and entity id is given
     Then "Details of product rule is displayed" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  
  @GetRuleDefinitionByName
  Scenario Outline: Get rule definition by name
    Given RequestType "/ruledefinition" and Port <Port> and Method is <RequestMethod>
     When ruleName is given
     Then "getting details by rule name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @GetRuleDefinitionDetails
  Scenario Outline: Get rule definition details
    Given RequestType "/ruledetails/" and Port <Port> and Method is <RequestMethod> 
     When className is given
     Then "getting details by class name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @ExecuteWithRuleName
  Scenario Outline:Execute with run rule on specify by product id with rule name
    Given RequestType "/rule" and Port <Port> and Method is <RequestMethod> 
     When productId and ruleName is given
     Then "executed successfully with rule name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 404        | 7042 | POST          | 
  
  @ExecuteWithRuleNameAndEventDataAsBodyParams
  Scenario Outline: Execute with run rule on specify by product id with rule name and event data as body params
    Given RequestType "/ruledetails" and Port <Port> and Method is <RequestMethod> 
     When productId and ruleName is given
     Then "executed successfully by eventdata as body params" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 404        | 7042 | POST          | 
  
  @AddScoreCard
  Scenario Outline: Add score card on specify by entity type, productId and score card request
    Given RequestType "/scorecard" and Port <Port> and Method is <RequestMethod> 
     When ProductRule ProductId is given
     Then "addded score card successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | POST          |

  
  @GetScoreCardByName
  Scenario Outline: Get score card by name 
    Given RequestType "/scorecard" and Port <Port> and Method is <RequestMethod> 
     When scorecard name is given
     Then "getting score card details by score card name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @UpdateScoreCardById
  Scenario Outline: Update score card by id
    Given RequestType "/scorecard" and Port <Port> and Method is <RequestMethod> 
     When added score card
     Then "updated score card successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | PUT           | 
  
  @GetScoreCardDetailById
  Scenario Outline: Get score card detail by id
    Given RequestType "/scorecard" and Port <Port> and Method is <RequestMethod> 
     When added score card
     Then "getting details of score crd by id" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @RunScoreCard
  Scenario Outline: Run score card on specify by product id with score card id 
    Given RequestType "/scorecard" and Port <Port> and Method is <RequestMethod> 
     When productId and scoreCardId is given
     Then "executed score xcard successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 404        | 7042 | POST          | 
  
  @AddScoreCardVariable
  Scenario Outline: Add score card variable by id
    Given RequestType "/scorecardvariable" and Port <Port> and Method is <RequestMethod> 
     When scorecardid is given
     Then "Added scorecard variable successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | POST          | 
  
  @UpdateScoreCardVariableByVariableId
  Scenario Outline: Update score card variable by score card id and variable id
    Given RequestType "/scorecardvariable" and Port <Port> and Method is <RequestMethod>
     When added score card variable
     Then "updated score card variable successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7042 | PUT           |
  
	@GetScoreCardVariableByVariableId
  Scenario Outline: Get score card variable by score card id and variable id
    Given RequestType "/scorecardvariable" and Port <Port> and Method is <RequestMethod>
     When added score card variable
     Then "getting details of score card by variable id" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 

  @GetAllScoreCardByEntityType
  Scenario Outline: Get all score card by entity type
    Given RequestType "/all/scorecard" and Port <Port> and Method is <RequestMethod>
     When score card by entity type
     Then "getting score card details by entity type" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @ValidateScoreCard
  Scenario Outline: Validate score card by entity type, product id and score card id 
    Given RequestType "/validatescorecard" and Port <Port> and Method is <RequestMethod> 
     When entity type, product id and score card id  is given
     Then "validates score card successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 500        | 7042 | POST          | 
  
  @AddPricingStrategy
  Scenario Outline: Add pricing strategy
    Given RequestType "/pricingstrategy" and Port <Port> and Method is <RequestMethod>
     When ProductRule ProductId is given
     Then "added pricig strategy successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | POST          | 
  
  @UpdatePricingStrategy
  Scenario Outline: Update pricing strategy
    Given RequestType "/pricingstrategy" and Port <Port> and Method is <RequestMethod> 
     When added pricing strategy
     Then "updated pricing strategy successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | PUT           | 
  
  @GetPricingStrategyById
  Scenario Outline: Get pricing strategy by id
    Given RequestType "/pricingstrategy" and Port <Port> and Method is <RequestMethod> 
     When added pricing strategy
     Then "getting details of pricing strategy by id" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @RunPricingStrategy
  Scenario Outline: Run pricing strategy
    Given RequestType "/pricingstrategy" and Port <Port> and Method is <RequestMethod> 
     When productId and pricingStrategyName is given
     Then "executed pricing strategy successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 404        | 7042 | POST          | 
  
  @GetAllPricingStrategy
  Scenario Outline: Get all pricing strategy 
    Given RequestType "/all/pricingstrategy" and Port <Port> and Method is <RequestMethod> 
     When all pricing strategy 
     Then "getting details of all pricing strategy " and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @GetStrategyHistory
  Scenario Outline: Get strategy history
    Given RequestType "/strategyhistory" and Port <Port> and Method is <RequestMethod> 
     When strategy history
     Then "getting details of strategy history" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @AddPricingStrategySteps
  Scenario Outline: Add pricing strategy steps 
    Given RequestType "/pricingstrategystep" and Port <Port> and Method is <RequestMethod> 
     When princing strategyId is given
     Then "added pricing strategy steps successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | POST          | 
  
  @UpdatePricingStrategyStep
  Scenario Outline: 24) Update pricing strategy step. 
    Given RequestType "/pricingstrategystep" and Port <Port> and Method is <RequestMethod> 
     When added Pricindg strategy step
     Then "updated pricing strategy steps successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | PUT           | 
  
  @GetPricingStrategyStep
  Scenario Outline: Get pricing strategy step
    Given RequestType "/pricingstrategystep" and Port <Port> and Method is <RequestMethod> 
     When added Pricindg strategy step
     Then "getting details of pricing strategy steps by id" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @AddSegment
  Scenario Outline: Add Segment with request model data
    Given RequestType "/segment" and Port <Port> and Method is <RequestMethod>
     When ProductRule ProductId is given
     Then "added  segment successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7042 | POST          |
 
  @GetSegmentByName
  Scenario Outline: Get Segment by name
    Given RequestType "/segment" and Port <Port> and Method is <RequestMethod> 
     When segment name is given
     Then "getting segment details by segment name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  
  @UpdateSegmentById
  Scenario Outline: Update segment by id
    Given RequestType "/segment" and Port <Port> and Method is <RequestMethod>
     When added segment
     Then "updated segment successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | PUT           | 
  
  @GetSegmentById
  Scenario Outline: Get segment by id
    Given RequestType "/segment" and Port <Port> and Method is <RequestMethod> 
     When added segment
     Then "getting segment details by id" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @GetAllSegments
  Scenario Outline: Get all segments
    Given RequestType "/all/segment" and Port <Port> and Method is <RequestMethod>
     When all segments
     Then "getting all segment details successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  

  @RunSegment
  Scenario Outline: Run segment on specify by product id with segment id
    Given RequestType "/segment" and Port <Port> and Method is <RequestMethod>
     When product id with segmentid is given
     Then "executed segment successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 404        | 7042 | POST          | 
  
  @AddLookup
  Scenario Outline: Add lookup by entity type and product id
    Given RequestType "/lookup" and Port <Port> and Method is <RequestMethod> 
     When ProductRule ProductId is given
     Then "Added lookup successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | POST          | 
  
  @UpdateLookup
  Scenario Outline: 33) Update lookup by lookup id.
    Given RequestType "/lookup" and Port <Port> and Method is <RequestMethod> 
     When Added lookup
     Then "updated lookup successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | PUT           | 
  
  @GetLookupByLookupId
  Scenario Outline: Get lookup by lookup id
    Given RequestType "/lookup" and Port <Port> and Method is <RequestMethod> 
     When Added lookup
     Then "getting lookup details succesfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @GetLookupByName
  Scenario Outline: Get lookup by name
    Given RequestType "/lookup" and Port <Port> and Method is <RequestMethod> 
     When lookup name is given
     Then "getting lookup details by lookup name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @GetAllLookupDataByEntityType
  Scenario Outline: Get all lookup data by entity type
    Given RequestType "/all/lookup" and Port <Port> and Method is <RequestMethod> 
     When lookup data by entity type
     Then "getting details of all lookup" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @RunLookup
  Scenario Outline: Run lookup on specify by product id with lookup id
    Given RequestType "/lookup" and Port <Port> and Method is <RequestMethod> 
     When product id with lookup id is given
     Then "executed lookup successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 404        | 7042 | POST          | 
  
  @UploadLookupData
  Scenario Outline: Upload lookup data on specify by lookup id. 
    Given RequestType "/upload" and Port <Port> and Method is <RequestMethod> 
     When Added lookup
     Then "uploaded lookup data" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | POST          | 
  
  @AddTemplate
  Scenario Outline: Add template by entity type and product id 
    Given RequestType "/template" and Port <Port> and Method is <RequestMethod> 
     When ProductRule ProductId is given
     Then "Added template successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | POST          | 
  
  @UpdateTemplate
  Scenario Outline: Update template by template id. 
    Given RequestType "/template" and Port <Port> and Method is <RequestMethod>
     When added template
     Then "updated template successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7042 | PUT           |     
   
   @GetAllTemplateByEntityType
  Scenario Outline: Get all template by entity type
    Given RequestType "/all/template" and Port <Port> and Method is <RequestMethod>
     When template data by entity type
     Then "getting all details of template" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7042 | GET           | 
  
  @GetTemplateByEventName
  Scenario Outline: Get template by event name
    Given RequestType "/template" and Port <Port> and Method is <RequestMethod> 
     When Event name is given
     Then "getting template details by event name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | GET           | 
  
  @DeleteScoreCardVariable
  Scenario Outline: Delete score card variable by score card id and variable id
    Given RequestType "/scorecardvariable" and Port <Port> and Method is <RequestMethod> 
     When added score card variable
     Then "deleted score card variable successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | DELETE        | 
  
  @DeleteScoreCard
  Scenario Outline: Delete score card by id
    Given RequestType "/scorecard" and Port <Port> and Method is <RequestMethod>
     When added score card
     Then "deleted score card successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | DELETE        | 
  
  @DeletePricingStrategyStep
  Scenario Outline: Delete pricing strategy step
    Given RequestType "/pricingstrategystep" and Port <Port> and Method is <RequestMethod> 
     When added Pricindg strategy step
     Then "deleted pricing strategy step successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | DELETE        | 
  
  @DeletePricingStrategy
  Scenario Outline: Delete pricing strategy
    Given RequestType "/pricingstrategy" and Port <Port> and Method is <RequestMethod> 
     When added pricing strategy
     Then "deleted pricing strategy successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | DELETE        | 
  
  @DeleteSegment
  Scenario Outline: Delete segment by id
    Given RequestType "/segment" and Port <Port> and Method is <RequestMethod> 
     When added segment
     Then "deleted segment successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | DELETE        | 
  
  @DeleteRuleDefinition
  Scenario Outline: Delete rule definition by rule id
    Given RequestType "/ruledefinition" and Port <Port> and Method is <RequestMethod> 
     When Added rule defination
     Then "delted rule defination successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | DELETE        | 
  
  @DeleteLookup
  Scenario Outline: Delete lookup by lookup id
    Given RequestType "/lookup" and Port <Port> and Method is <RequestMethod> 
     When Added lookup
     Then "deleted lookup successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7042 | DELETE        | 
  
