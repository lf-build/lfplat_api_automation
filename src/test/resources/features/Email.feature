Feature: Email Service 
 
   @GetsHistory
  Scenario Outline: To get history of send mail 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When Get history entityType <entityType>
     Then "getting history of send mail" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7054 | GET           |
 
 
 	@SendsSpecifiedTemplateName 
  Scenario Outline: Sends Mail with specific template name 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Sends template name <templateName> and <templateVersion>
     Then "mail sholld be sent" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7054 | POST          |
  
  @SendsSpecificEntityId
  Scenario Outline: Sends mail with  specific entity type 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When Sends entityType <entityType>
     Then "sends with entitytype" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7054 | POST          |
  
  @SendsMail
  Scenario Outline: Send mail
    Given RequestType "/mail" and Port <Port> and Method is <RequestMethod>
     When Sends mail 
     Then "sent mail" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | entityType  | entityId  | 
      | 200        | 7054 | POST          | application | DV0007076 |
 
  
  @GetsHistoryByTemplate
  Scenario Outline: using template
    Given RequestType "/by/template/" and Port <Port> and Method is <RequestMethod>
     When History by templatename <templateName>
     Then "getting history using template name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |  
      | 200        | 7054 | GET           |
  
  