Feature: ACH Service 

@Queuesinstruction
  Scenario Outline: Queues the specified instruction requesT
    Given RequestType "/queue" and Port <Port> and Method is <RequestMethod>
     Then "instruction is queued" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7068 | PUT           | 
  
  @DequeuesinstructionById
  Scenario Outline: Dequeues the specified instruction identifier
    Given RequestType "/queue/" and Port <Port> and Method is <RequestMethod> 
     When INSTRUCTION ID IS GIVEN
     Then "Deques instruction successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7068 | DELETE        | 
      
  @Queuesinstruction
  Scenario Outline: Queues the specified instruction requesT
    Given RequestType "/queue" and Port <Port> and Method is <RequestMethod>
     Then "instruction is queued" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7068 | PUT           | 
      
 @DequeuesByInternalRefNum
 Scenario Outline: Dequeues the by internal reference number
   Given RequestType "/queue/by/internal-reference-number/" and Port <Port> and Method is <RequestMethod> 
    When Internal reference number is given
    Then "instruction is dequeued" and Status Code should be <StatusCode>
   Examples: 
     | StatusCode | Port | RequestMethod | 
     | 204        | 7068 | DELETE        | 
 
 @Queuesinstruction
 Scenario Outline: Queues the specified instruction requesT
   Given RequestType "/queue" and Port <Port> and Method is <RequestMethod>
    Then "instruction is queued" and Status Code should be <StatusCode>
   Examples: 
     | StatusCode | Port | RequestMethod | 
     | 200        | 7068 | PUT           |
  
  @CreatesFile
  Scenario Outline: Creates the specified funding source identifier.
    Given RequestType "/file/by-funding-source/" and Port <Port> and Method is <RequestMethod>
     When instruction is queued
     Then "Creates the file" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7068 | POST          | 
  
  @SendsFile
  Scenario Outline: Sends the specified identifier. 
    Given RequestType "/file/" and Port <Port> and Method is <RequestMethod> 
     When file is created
     Then "File is sent" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7068 | POST          | 
  
  @Cancelsfile
  Scenario Outline: Cancels 
    Given RequestType "/file/" and Port <Port> and Method is <RequestMethod>
     When file is sent
     Then "file is cancelled successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7068 | POST          | 
  
  @Processesfile
  Scenario Outline: Processes the file 
    Given RequestType "/file/process" and Port <Port> and Method is <RequestMethod> 
     When File is rejected
     Then "no file found to process" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 404        | 7068 | POST          | 
  
  @GetsAchNocReturnData
  Scenario Outline: Gets the ach noc return data 
    Given RequestType "/file/noc-received" and Port <Port> and Method is <RequestMethod> 
     When file is noc return type
     Then "No data found for Noc Received" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 404        | 7068 | GET           | 
  
  @GetsInstructionByRefNum
  Scenario Outline: Gets instruction by reference number. 
    Given RequestType "/instruction/referenceNumber/" and Port <Port> and Method is <RequestMethod> 
     When refNum and status is given
     Then "getting details of that refnum" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7068 | GET           | 
  
  @GetsInstructionByInternalrefNum
  Scenario Outline: Gets instruction by  internalreference number 
    Given RequestType "/instruction/internalReferenceNumber" and Port <Port> and Method is <RequestMethod> 
     Then "We should be able to Deactivate User" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7068 | POST          | 
  
  @GetsInstructionByFundingSource 
  Scenario Outline: Gets instruction by funding source identifier 
    Given RequestType "/instruction/" and Port <Port> and Method is <RequestMethod> 
     When sourceId is given
     Then "getting instruction successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7068 | GET           | 
  
  @RejectsInstructionByInternalRefNum
  Scenario Outline: Rejects instruction by internal reference number
    Given RequestType "/instruction/by/internal-reference-number/" and Port <Port> and Method is <RequestMethod> 
     When Internal ref num is given
     Then "instruction should get rejected" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7068 | POST          | 
  
  @GetsPauseBylRefNum
  Scenario Outline: Gets the pause by loan reference number 
    Given RequestType "/ach/" and Port <Port> and Method is <RequestMethod> 
     When paused payment ref num is given
     Then "Getting details of paused payments" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 404       | 7068 | GET           | 
  
  @PausespaymentByReferenceNumber
  Scenario Outline: Pauses the specified loan reference number
    Given RequestType "/ach/" and Port <Port> and Method is <RequestMethod> 
     When Ref number is given for paused payment
     Then "Payment is paused sucessfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7068 | POST          | 
  
  @ResumesPayment
  Scenario Outline: Resumes the specified loan reference number
    Given RequestType "/ach/" and Port <Port> and Method is <RequestMethod>
     When payment is paused
     Then "payment is resumed successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7068 | DELETE        |
      
  @GetsPauses
  Scenario Outline: Gets the pauses
    Given RequestType "/ach/pauses" and Port <Port> and Method is <RequestMethod>
     Then "Getting details of all the paused payments" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7068 | GET           |
  
