Feature: Assignment Engine Service 


  @CreateBackOfficeuserOne
  Scenario Outline: Create Back Office User 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Create UserOne with Role <Role>
     Then "userOne should be created" and Status Code should be <StatusCode>
    Examples: 
      | Role     | StatusCode | Port | RequestMethod | 
      | CEO      | 200        | 7018 | PUT           | 
  
  @LoginToBackOfficeuserOne
  Scenario Outline: Login with the Created User 
    Given RequestType "/login" and Port <Port> and Method is <RequestMethod> 
     When UserOne Portal
     Then "userOne should be able to Login" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | Portal      | RequestMethod | 
      | 200        | 7018 | back-office | POST          | 
      
      
 @CreateBackOfficeuserTwo
  Scenario Outline: Create Back Office User 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Create UserTwo with Role <Role>
     Then "userTwo should be created" and Status Code should be <StatusCode>
    Examples: 
      | Role                   | StatusCode | Port | RequestMethod | 
      | Junior Customer Agent  | 200        | 7018 | PUT           | 
  
 @LoginToBackOfficeuserTwo
  Scenario Outline: Login with the Created User 
    Given RequestType "/login" and Port <Port> and Method is <RequestMethod> 
     When UserTwo Portal
     Then "userTwo should be able to Login" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | Portal      | RequestMethod | 
      | 200        | 7018 | back-office | POST          | 
      
      
  @ClaimsByEntity
  Scenario Outline: Claims the specified entity type
    Given RequestType "/claim" and Port <Port> and Method is <RequestMethod> 
     When userOne is logged in
     Then "User should be able to claim" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  |
      | 200        | 7049 | POST           |


  @AssignsByEntity
  Scenario Outline: Assigns the specified entity type
    Given RequestType "/assign" and Port <Port> and Method is <RequestMethod>
     When role is parent
     Then "User should be able to assign" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  | 
      | 200        | 7049 | POST           | 
      
      
 @DeterminesAssignedToMe
  Scenario Outline: Determines whether is assigned to me to the specified entity type
    Given RequestType "/assigned/me" and Port <Port> and Method is <RequestMethod>
     When userTwo is logged in
     Then "successfully checked" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7049 | GET           |
      
  @EscalatesByEntity
  Scenario Outline: Escalates the specified entity type
    Given RequestType "/escalate" and Port <Port> and Method is <RequestMethod> 
     When userTwo is logged in
     Then "User should be able to escalate" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7049 | POST          |
      
  @GetsEscalations
  Scenario Outline: Gets all escalations
    Given RequestType "/escalations" and Port <Port> and Method is <RequestMethod> 
     When userTwo is logged in
     Then "We should be able to get escalations details" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7049 | GET           |
  
  @AcceptsByEntity
  Scenario Outline: Accepts the specified entity type
    Given RequestType "/accept" and Port <Port> and Method is <RequestMethod> 
     When escalated by userTwo
     Then "user should be able to accept byentity" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  | 
      | 200        | 7049 | POST           |
  
  @CompletesByEntity
  Scenario Outline: Completes the specified entity type. 
    Given RequestType "/complete" and Port <Port> and Method is <RequestMethod> 
     When accepted by userOne
     Then "User should not be able to complete assignment" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7049 | POST          |
 
 
   @ClaimsByEntity
  Scenario Outline: Claims the specified entity type
    Given RequestType "/claim" and Port <Port> and Method is <RequestMethod> 
     When userOne is logged in
     Then "User should be able to claim" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  |
      | 200        | 7049 | POST           |


  @AssignsByEntity
  Scenario Outline: Assigns the specified entity type
    Given RequestType "/assign" and Port <Port> and Method is <RequestMethod>
     When role is parent
     Then "User should be able to assign" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  | 
      | 200        | 7049 | POST           | 
  
  @ReleasesByEntity
  Scenario Outline: Releases the specified entity type 
    Given RequestType "/release" and Port <Port> and Method is <RequestMethod> 
     When assigned application by userOne
     Then "User should be able to release" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7049 | POST          |
  

  @CancelsByEntity
  Scenario Outline: Cancels the specified entity type
    Given RequestType "/cancel" and Port <Port> and Method is <RequestMethod> 
     When escalated by userTwo
     Then "We should be able to cancel" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod    |
      | 200        | 7049 | DELETE           | 
 
   @ClaimsByEntity
  Scenario Outline: Claims the specified entity type
    Given RequestType "/claim" and Port <Port> and Method is <RequestMethod> 
     When userOne is logged in
     Then "User should be able to claim" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  |
      | 200        | 7049 | POST           |


  @AssignsByEntity
  Scenario Outline: Assigns the specified entity type
    Given RequestType "/assign" and Port <Port> and Method is <RequestMethod>
     When role is parent
     Then "User should be able to assign" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  | 
      | 200        | 7049 | POST           |  
  
  @UnassignsByEntity
  Scenario Outline: Unassigns the specified entity type
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When userOne has assigned the application 
     Then "We should be able to unassign" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod    |
      | 204        | 7049 | DELETE           |
      
      
  
  @GetsAssignments
  Scenario Outline: Gets all assignments 
    Given RequestType "/assignments" and Port <Port> and Method is <RequestMethod> 
     When userTwo is logged in
     Then "successfully getting assignments details" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7049 | GET           |
  
  @GetsByUser
  Scenario Outline: Gets all by user
    Given RequestType "/assignments/" and Port <Port> and Method is <RequestMethod>
     When User is given
     Then "User details should be available" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7049 | GET           |
  
