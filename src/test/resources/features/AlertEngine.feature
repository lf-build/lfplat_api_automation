Feature: Alert Engine Service 

@CreateAlert
Scenario Outline: Get Tags
	  Given RequestType "/create" and Port <Port> and Method is <RequestMethod>
     Then "Should create alert for the specified Id" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7043 | POST          |
       

@GetAlerts 
  Scenario Outline: Get Alerts
	  Given RequestType "/" and Port <Port> and Method is <RequestMethod>
	  When Alert is created
     Then "Should get the alerts for the specified entity" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7043 | GET           |
     
@GetTags 
  Scenario Outline: Get Tags
	  Given RequestType "/tags" and Port <Port> and Method is <RequestMethod>
	   When Created Alert
     Then "Should get the tags for the specified entity" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7043 | GET           |

@DismissAlerts 
  Scenario Outline: Get Tags
	  Given RequestType "/dismiss" and Port <Port> and Method is <RequestMethod>
     When Alert Id is available
     Then "Should dismiss the alert for the specified Id" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7043 | POST          | 

@UndoDismissAlerts 
  Scenario Outline: Get Tags
	  Given RequestType "/dismiss/undo" and Port <Port> and Method is <RequestMethod>
     When Alert Id is available
     Then "Should undo dismissed alert for the specified Id" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7043 | POST          |
      
