Feature: Action Center Service 

@RequestListByEntity 
  Scenario Outline: Gets the request list by entityType
	  Given RequestType "/application" and Port <Port> and Method is <RequestMethod>
     When ActionentityType is given
     Then "Should get the action details of specified entity" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7033 | GET           | 
      
      
@AddsEntityByRequestId
  Scenario Outline: Adds the specified entity type by requestid
	  Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When request id is <requestId>
     Then "action center should get added" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  | 
      | 204        | 7033 | POST           | 
      
@AddsEntity
  Scenario Outline: Adds the specified entity
	  Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When adding entity
     Then "entity  should be added" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  | 
      | 204        | 7033 | POST           | 
   
@GetRequestsByEntity
  Scenario Outline: Gets the requests by entity
	  Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When entity id is given
     Then "Should get the list of request" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7033 | GET           | 

@GetsRequestByStatusandId
  Scenario Outline: Gets the request list by status and Id
	  Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When status and id is given
     Then "Should get the list of request with the given status and Id" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7033 | GET           | 
      
@GetsRequestByStatus
  Scenario Outline: Gets the request list by status
	  Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When actionstatus is given
     Then "Should get the status of request type" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7033 | GET           | 

@GetsSummaryByRequetId
  Scenario Outline: Gets Summary By RequetId
	  Given RequestType "/count" and Port <Port> and Method is <RequestMethod>
     When summary with requestid
     Then "Should get the summary of request type" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7033 | GET           | 

@GetSummary
  Scenario Outline: Gets Summary
	  Given RequestType "/count" and Port <Port> and Method is <RequestMethod>
     When summary     with wntity is given
     Then "Should get the summmary" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7033 | GET           | 
      
      
@GetsRequestByRequestIdandId 
  Scenario Outline: Gets the request list by requestId and Id
	  Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When given requestId and Id
     Then "Should get the action details of specified requestId and Id" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7033 | GET           | 
      
@GetsGroupSummary 
  Scenario Outline: Gets the request list by entityType
	  Given RequestType "/groups/" and Port <Port> and Method is <RequestMethod>
     When group summary
     Then "Should get the group summary" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7033 | GET           | 
 
 
 @GetsRequestTypes
  Scenario Outline: Gets the request types.
	  Given RequestType "/request-types" and Port <Port> and Method is <RequestMethod>
     When request types is given
     Then "Should get details of all request types" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7033 | GET           |      
      
           
      

