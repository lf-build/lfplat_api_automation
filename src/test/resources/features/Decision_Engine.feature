Feature: Decision Engine Service 


  @AddRule
  Scenario Outline: Check the current User
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Rule Name is given
     Then "rule should return true" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7011 | POST          | 
 
 
  @UpdatRule
  Scenario Outline: update rule 
    Given RequestType "" and Port <Port> and Method is <RequestMethod> 
     When rule should be added
     Then "rule should get updated" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7011 | PUT           |  

  @GetByRuleName
  Scenario Outline: Get by rule name
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Rule Name is given
     Then "User should be created" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7011 | GET           | 
  

  

