Feature: Verification Engine Service 

@GetsConfiguration
  Scenario Outline: Gets the config details
    Given RequestType "/verification-engine" and Port <Port> and Method is <RequestMethod> 
     When config details 
     Then "configuration details" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7001 | GET           |

  @GetsVerificationDetail
  Scenario Outline: Gets the verification detail
    Given RequestType "/detail" and Port <Port> and Method is <RequestMethod>
     When fact is  <fact>
     Then "getting verification details" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7020 | GET           |
  
  

@GetsVerificationStatus
  Scenario Outline: Gets the verification status 
    Given RequestType "/status" and Port <Port> and Method is <RequestMethod> 
     When fact given 
     Then "getting verification status" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7020 | GET           |
      
      
      
@VerifiesFacts 
  Scenario Outline: Verifies the facts 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Fact <fact> and source <source> is given 
     Then "fact verified successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7020 | POST          |
      
  
  
  @GetsVerificationMethod
  Scenario Outline: Gets the verification method
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When productID is <productId> 
     Then "getting verification method" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7020 | GET           |
  
  @GetsVerification
  Scenario Outline: Gets the verification
    Given RequestType "/do-not-load-methods/" and Port <Port> and Method is <RequestMethod>
     When ProductId is given 
     Then "verification done successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7020 | GET           | 
  
  @GetsVerificationFacts
  Scenario Outline: Gets the verification facts
    Given RequestType "/config-detail" and Port <Port> and Method is <RequestMethod> 
     When productID <productId> and staus workflowid  <workFlowStatusId>  is given
     Then "getting verification facts" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7020 | GET           |
  
  @InitiatesManualVerification
  Scenario Outline: Initiates the manual verification
    Given RequestType "/initiate-manualverification" and Port <Port> and Method is <RequestMethod> 
     When fact  <fact>  method name  <method> is given
     Then "Manual verification is intiated" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7020 | POST          |
  
  @InitiatesSyndicationVerification
  Scenario Outline: Initiates the syndication verification
    Given RequestType "/initiate-syndication-verification" and Port <Port> and Method is <RequestMethod> 
     When fact  <fact>  method name  <method> is given
     Then "Syndication verification initiated" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7020 | POST          |
      
  @VerificationsDocumentReceived
  Scenario Outline: Verifications the document received
    Given RequestType "/<entityType>/<entityId>/<workFlowStatusId>/<fact>/<method>/upload-document" and Port <Port> and Method is <RequestMethod> 
     When fact  <fact>  method name  <method> is given
     Then "Verification document recieved" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7020 | POST          |
  
  @PendingsTheDocument
  Scenario Outline: Pendings the document
    Given RequestType "/pending-document" and Port <Port> and Method is <RequestMethod> 
     When workflowstatusId is <workFlowStatusId>
     Then "document is in pending state" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7020 | GET           |
  
  @GetsRequiredDocument
  Scenario Outline: Gets the required document 
    Given RequestType "/required-document" and Port <Port> and Method is <RequestMethod> 
     When productId <productId> and fact <fact> is given
     Then "getting the required documents" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7020 | GET           |
  
  @GetsDocuments
  Scenario Outline: Gets the documents
    Given RequestType "/documents" and Port <Port> and Method is <RequestMethod> 
     When workflowstatusId is <workFlowStatusId>
     Then "document is getting downloaded" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7020 | GET           |
  
  @GetsDocumentsByFact
  Scenario Outline: Gets the documents
    Given RequestType "/documentsDetails" and Port <Port> and Method is <RequestMethod> 
     When document is uploaded
     Then "getting documents by fact name" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7020 | GET           |
      
  @VerifiesDocument
  Scenario Outline: Gets the documents
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When documentId is <documentId>
     Then "document verified" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7020 | PUT           |
      
  @ResetsFacts
  Scenario Outline: Resets the facts 
    Given RequestType "/resetfacts" and Port <Port> and Method is <RequestMethod> 
     When verificationentityType is <entityType>
     Then "fact is reset succesfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7020 | PUT           | 
      
  @EventsRepublish
  Scenario Outline: Events the re publish
    Given RequestType "/events-republish" and Port <Port> and Method is <RequestMethod> 
     When workflowstatusId is <workFlowStatusId>
     Then "event republished successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7020 | POST          |
      
  @InitiatesDynamicFactVerification
  Scenario Outline: Initiates the dynamic fact verification.
    Given RequestType "/initiate-dynamicfact-verification" and Port <Port> and Method is <RequestMethod> 
     When To Initiate dynamic verification
     Then "intiated dynamiv fact verification" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 204        | 7020 | POST          |
  

  
 