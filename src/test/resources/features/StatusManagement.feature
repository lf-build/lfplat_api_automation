Feature: Status Management Service 

@ProcesStatusWorkFlow 
  Scenario Outline: Processes the status work flow
    Given RequestType "/workflow/" and Port <Port> and Method is <RequestMethod>
     When application is recieved
     Then "Workflow should be processed" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7021 | POST          | 
  
  
  @InitiateSubWorFlow
  Scenario Outline: Initiates the sub work flow
    Given RequestType "/subworkflow/" and Port <Port> and Method is <RequestMethod>
     When workflow is processed
     Then "Workflow is initiated" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7021 | POST          | 
  
  @MoveSubWorkFlow
  Scenario Outline: Moves the sub work flow
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When Intiated subworkflow
     Then "Moved subWorkflow successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 204        | 7021 | PUT           | 
  
  @GetsChecklist
  Scenario Outline: Gets the checklist
    Given RequestType "/checklist" and Port <Port> and Method is <RequestMethod>
     When statusWorkFlowId & status is given
     Then "getting details of checklist" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7021 | GET           | 
  
  @GetTransitions
  Scenario Outline: Gets the transitions.
    Given RequestType "/transitions" and Port <Port> and Method is <RequestMethod> 
     When statusWorkFlowId is given
     Then "getting details of transitions successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7021 | GET           | 
  
  @GetReasons
  Scenario Outline: Gets the reasons
    Given RequestType "/reasons" and Port <Port> and Method is <RequestMethod> 
     When statusWorkFlowId is given
     Then "getting details of reason" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 404        | 7021 | GET           | 
  
  @GetActivities
  Scenario Outline: Gets the activities 
    Given RequestType "/activities" and Port <Port> and Method is <RequestMethod> 
     When statusWorkFlowId is given
     Then "getting activities list" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 404        | 7021 | GET           | 
  
  @GetStatusByStatusCode
  Scenario Outline: Gets the status by status code
    Given RequestType "/detail" and Port <Port> and Method is <RequestMethod> 
     When statusWorkFlowId is given
     Then "getting details of status by status code" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7021 | GET           | 
  
  @GetsSubStatusDetails
  Scenario Outline: Gets the sub status details 
    Given RequestType "/detail" and Port <Port> and Method is <RequestMethod> 
     When substatus is given
     Then "getting details of substatus" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7021 | GET           | 
  
  @GetActiveStatusWorkFlow
  Scenario Outline: Gets the active status work flow
    Given RequestType "/statusworkflow" and Port <Port> and Method is <RequestMethod> 
     When entityType & entityId is given
     Then "details of active status workflow getting successfully" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7021 | GET           | 
  
  @GetEntityStatusHistory
  Scenario Outline: Gets the entity status history 
    Given RequestType "/history/" and Port <Port> and Method is <RequestMethod> 
     When productId is given to get history
     Then "getting details of entity status history" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7021 | GET           | 
  
  @GetsStatus
  Scenario Outline: Gets the status 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When statusWorkFlowId is given to get status
     Then "successfully getting details of status " and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7021 | GET           | 
  
  @GetWorkFlowNames
  Scenario Outline: Gets the work flow names
    Given RequestType "/statusworkflows" and Port <Port> and Method is <RequestMethod>
     When status management entitytype is given
     Then "names displayed" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7021 | GET           |
      
 @GetStatusByProduct
 Scenario Outline: Gets the status by product
   Given RequestType "/statusbyproduct" and Port <Port> and Method is <RequestMethod> 
    When productId is given to get status by product
    Then "successfully getting details of status by product" and Status Code should be <StatusCode>
   Examples: 
     | StatusCode | Port | RequestMethod | 
     | 200        | 7021 | GET           |
     
 @ChangeStatus
 Scenario Outline: Changes the status
   Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
    When application needs to change status
    Then "status should get changed" and Status Code should be <StatusCode>
   Examples: 
     | StatusCode | Port | RequestMethod | 
     | 204        | 7021 | PUT           | 
  
