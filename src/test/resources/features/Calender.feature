Feature: Calender Service 

@GetsDate 
  Scenario Outline: Gets the date
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When current date is given
     Then "User should be able to get details of that date" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod |
      | 200        | 7069 | GET           |

 
  
  @GetsPeriodicDates
  Scenario Outline: Gets the periodic dates
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When term and periodicity is given
     Then "User should get details betweem that term" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port  | RequestMethod |
      | 200        | 7069  | GET           |

  
  