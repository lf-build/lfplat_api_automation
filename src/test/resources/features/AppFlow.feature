Feature: AppFlow Service 

  @StartsSpecifiedWorkflow
  Scenario Outline: Starts the specified workflow
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Workflow is given
     Then "Workflow is started" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  | 
      | 200        | 7060 | POST           | 
  
  @GetsDefinition
  Scenario Outline: Gets the definition 
    Given RequestType "/definition" and Port <Port> and Method is <RequestMethod> 
     When workflow has started
     Then "Should get details of defination" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7060 | GET           | 
  
  @ExecutesStep
  Scenario Outline: Executes the step
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When started for executing steps
     Then "success" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7060 | POST          | 
      

  @ExecutesStep
  Scenario Outline: Executes the step
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When started for executing steps
     Then "success" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7060 | POST          | 
 
  @ExecutesStep
  Scenario Outline: Executes the step
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When started for executing steps
     Then "success" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7060 | POST          | 
      
      
  @GetsSpecifiedWorkflowById
  Scenario Outline: Gets the specified workflow by id 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When Workflow and id is given
     Then "User should be able to change password" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7060 | GET           | 

  
  @PausesSpecifiedWorkflow
  Scenario Outline: Pauses the specified workflow
    Given RequestType "/pause" and Port <Port> and Method is <RequestMethod> 
     When started the workflow
     Then "workflow is paused" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7060 | PUT           | 
  
  @ResumesExecuteStep
  Scenario Outline: Res the execute step
    Given RequestType "/exec" and Port <Port> and Method is <RequestMethod> 
     When executed the step
     Then "Resumed the executed step" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7060 | PUT           | 
      
  @StartsSpecifiedWorkflow
  Scenario Outline: Starts the specified workflow
    Given RequestType "/" and Port <Port> and Method is <RequestMethod>
     When Workflow is given
     Then "Workflow is started" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod  | 
      | 200        | 7060 | POST           |      
  
  @CancelsSpecifiedWorkflow
  Scenario Outline: Cancels the specified workflow 
    Given RequestType "/" and Port <Port> and Method is <RequestMethod> 
     When Workflow and id is given
     Then "User should be able to reset password" and Status Code should be <StatusCode>
    Examples: 
      | StatusCode | Port | RequestMethod | 
      | 200        | 7060 | DELETE        | 
  
 