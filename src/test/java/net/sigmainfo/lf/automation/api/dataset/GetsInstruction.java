package net.sigmainfo.lf.automation.api.dataset;
import java.util.List;
public class GetsInstruction {
	private List<Integer> internalReferenceNumbers = null;

	public List<Integer> getInternalReferenceNumbers() {
	return internalReferenceNumbers;
	}

	public void setInternalReferenceNumbers(List<Integer> internalReferenceNumbers) {
	this.internalReferenceNumbers = internalReferenceNumbers;
	}

}
