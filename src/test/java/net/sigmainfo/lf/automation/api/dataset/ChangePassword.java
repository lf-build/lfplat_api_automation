package net.sigmainfo.lf.automation.api.dataset;

public class ChangePassword {
	private String currentPassword;
	private String password;
	private String confirmPassword;

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String CurrentPassword) {
		currentPassword = CurrentPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String Password) {
		password = Password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String ConfirmPassword) {
		confirmPassword = ConfirmPassword;
	}

}
