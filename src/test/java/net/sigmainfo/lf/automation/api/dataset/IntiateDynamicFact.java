package net.sigmainfo.lf.automation.api.dataset;
import java.util.List;
public class IntiateDynamicFact {

	private String factName;
	private List<DocumentDetail> documentDetails = null;

	public String getFactName() {
	return factName;
	}

	public void setFactName(String factName) {
	this.factName = factName;
	}

	public List<DocumentDetail> getDocumentDetails() {
	return documentDetails;
	}

	public void setDocumentDetails(List<DocumentDetail> documentDetails) {
	this.documentDetails = documentDetails;
	}

	
	public class DocumentDetail {

		private String category;
		private String name;

		public String getCategory() {
		return category;
		}

		public void setCategory(String category) {
		this.category = category;
		}

		public String getName() {
		return name;
		}

		public void setName(String name) {
		this.name = name;
		}

		}
	}