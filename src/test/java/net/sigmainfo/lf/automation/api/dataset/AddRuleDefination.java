package net.sigmainfo.lf.automation.api.dataset;

import java.util.List;

public class AddRuleDefination {
	private String ruleName;
	private String dataAttributeStoreName;
	private String effectiveDate;
	private String expiryDate;
	private String ruleClass;
	private String ruleType;
	private String deRuleName;
	private String ruleVersion;
	private List<String> requiredSources = null;
	private List<String> optionalSources = null;
	private List<String> groupNames = null;
	private String createdBy;
	private String description;
	private String customDate;
	private String applicationType;

	public String getRuleName() {
	return ruleName;
	}

	public void setRuleName(String ruleName) {
	this.ruleName = ruleName;
	}

	public String getDataAttributeStoreName() {
	return dataAttributeStoreName;
	}

	public void setDataAttributeStoreName(String dataAttributeStoreName) {
	this.dataAttributeStoreName = dataAttributeStoreName;
	}

	public String getEffectiveDate() {
	return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
	this.effectiveDate = effectiveDate;
	}

	public String getExpiryDate() {
	return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
	this.expiryDate = expiryDate;
	}

	public String getRuleClass() {
	return ruleClass;
	}

	public void setRuleClass(String ruleClass) {
	this.ruleClass = ruleClass;
	}

	public String getRuleType() {
	return ruleType;
	}

	public void setRuleType(String ruleType) {
	this.ruleType = ruleType;
	}

	public String getDeRuleName() {
	return deRuleName;
	}

	public void setDeRuleName(String deRuleName) {
	this.deRuleName = deRuleName;
	}

	public String getRuleVersion() {
	return ruleVersion;
	}

	public void setRuleVersion(String ruleVersion) {
	this.ruleVersion = ruleVersion;
	}

	public List<String> getRequiredSources() {
	return requiredSources;
	}

	public void setRequiredSources(List<String> requiredSources) {
	this.requiredSources = requiredSources;
	}

	public List<String> getOptionalSources() {
	return optionalSources;
	}

	public void setOptionalSources(List<String> optionalSources) {
	this.optionalSources = optionalSources;
	}

	public List<String> getGroupNames() {
	return groupNames;
	}

	public void setGroupNames(List<String> groupNames) {
	this.groupNames = groupNames;
	}

	public String getCreatedBy() {
	return createdBy;
	}

	public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
	}

	public String getDescription() {
	return description;
	}

	public void setDescription(String description) {
	this.description = description;
	}

	public String getCustomDate() {
	return customDate;
	}

	public void setCustomDate(String customDate) {
	this.customDate = customDate;
	}

	public String getApplicationType() {
	return applicationType;
	}

	public void setApplicationType(String applicationType) {
	this.applicationType = applicationType;
	}

}
