package net.sigmainfo.lf.automation.api.dataset;

public class AddTemplate {
	private String name;
	private String title;
	private String format;
	private String body;
	private String version;
	private Properties properties;
	private String id;

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public String getTitle() {
	return title;
	}

	public void setTitle(String title) {
	this.title = title;
	}

	public String getFormat() {
	return format;
	}

	public void setFormat(String format) {
	this.format = format;
	}

	public String getBody() {
	return body;
	}

	public void setBody(String body) {
	this.body = body;
	}

	public String getVersion() {
	return version;
	}

	public void setVersion(String version) {
	this.version = version;
	}

	public Properties getProperties() {
	return properties;
	}

	public void setProperties(Properties properties) {
	this.properties = properties;
	}

	public String getId() {
	return id;
	}

	public void setId(String id) {
	this.id = id;
	}


	public class PdfSettings {

	private String pageSize;
	private String pageWidth;
	private String pageHeight;
	private String orientation;
	private Integer dpi;
	private Integer imageQuality;
	private String marginTop;
	private String marginBottom;
	private String marginLeft;
	private String marginRight;

	public String getPageSize() {
	return pageSize;
	}

	public void setPageSize(String pageSize) {
	this.pageSize = pageSize;
	}

	public String getPageWidth() {
	return pageWidth;
	}

	public void setPageWidth(String pageWidth) {
	this.pageWidth = pageWidth;
	}

	public String getPageHeight() {
	return pageHeight;
	}

	public void setPageHeight(String pageHeight) {
	this.pageHeight = pageHeight;
	}

	public String getOrientation() {
	return orientation;
	}

	public void setOrientation(String orientation) {
	this.orientation = orientation;
	}

	public Integer getDpi() {
	return dpi;
	}

	public void setDpi(Integer dpi) {
	this.dpi = dpi;
	}

	public Integer getImageQuality() {
	return imageQuality;
	}

	public void setImageQuality(Integer imageQuality) {
	this.imageQuality = imageQuality;
	}

	public String getMarginTop() {
	return marginTop;
	}

	public void setMarginTop(String marginTop) {
	this.marginTop = marginTop;
	}

	public String getMarginBottom() {
	return marginBottom;
	}

	public void setMarginBottom(String marginBottom) {
	this.marginBottom = marginBottom;
	}

	public String getMarginLeft() {
	return marginLeft;
	}

	public void setMarginLeft(String marginLeft) {
	this.marginLeft = marginLeft;
	}

	public String getMarginRight() {
	return marginRight;
	}

	public void setMarginRight(String marginRight) {
	this.marginRight = marginRight;
	}
	}



	public class Properties {

	private PdfSettings pdfSettings;

	public PdfSettings getPdfSettings() {
	return pdfSettings;
	}

	public void setPdfSettings(PdfSettings pdfSettings) {
	this.pdfSettings = pdfSettings;
	}
	
	}
}

