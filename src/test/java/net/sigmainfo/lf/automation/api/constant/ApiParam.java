package net.sigmainfo.lf.automation.api.constant;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

import org.json.JSONArray;
import org.springframework.stereotype.Component;

/**
 * ALL THE VARIABLES TO BE DECLARED IN THIS CLASS
 */
@Component
public class ApiParam {

	public static String getUserType() {
		return userType;
	}

	public static void setUserType(String userType) {
		ApiParam.userType = userType;
	}

	public static String userType;

	public static String currentFeatureFile;
	public static String baseresturl;
	public static String AuthToken;
	public static String requestType;
	public static String contentType;
	public static String environment;
	public static String ASPort;
	public static String ScenarioName;
	public static String currentScenarioName;
	public static String name;
	public static String email;
	public static String username;
	public static String password;
	public static String[] roles;
	public static boolean isActive;
	public static String passwordExpirationDate;
	public static String portal;
	public static String LoginToken;
	public static String resetToken;
	public static String id;
	public static String consumersID;
	public static String field;
	public static String entityId;
	public static String consumerEntityId;
	public static Set<String> entityid;
	public static String entityType;
	public static String referenceNumber;
	public static String verificationCode;
	public static String phone;
	public static String mobile;
	public static String subEntityIds;
	public static String status;
	public static String templateName;
	public static String version;
	public static String format;
	public static String actionEntityID;
	public static String actionEntityType;
	public static String requestId;
	public static String actionStatus;
	public static String actionId;
	public static LocalDate currentDate;
	public static String term;
	public static String periodicity;
	public static String achreferenceNumber;
	public static String fundingSourceId;
	public static String achstatus;
	public static int internalReferenceNumber;
	public static String reason;
	public static String InstructionId;
	public static LocalDateTime executionDate;
	public static String processStartDateTime;
	public static String processEndDateTime;
	public static String fileID;
	public static String emailEntityType;
	public static String mailId;
	public static String emailEntityId;
	public static String emailTemplateName;
	public static String emailTemplateVersion;
	public static String toName;
	public static String subject;
	public static String body;
	public static Object attachmentIds;
	public static JSONArray ccEmailAddress;
	public static JSONArray bccEmailAddress;
	public static JSONArray toEmailAddress;
	public static String productId;
	public static String fact;
	public static String method;
	public static String verificationStatus;
	public static String verificationReason;
	public static String workFlowStatusId;
	public static String verificationEntityType;
	public static String verificationEntityId;
	public static String documentId;
	public static String verificationSource;
	public static String alertEntityType;
	public static String alertEntityId;
	public static String alertStatus;
	public static String alertID;
	public static String workflow;
	public static String appFlowID;
	public static String step;
	public static String SMSentitytype;
	public static String SMSentityid;
	public static String SMStemplateName;
	public static String SMStemplateVersion;
	public static String SMSrecipientPhone;
	public static String SMScommonSMSText;
	public static String SMSmessage;
	public static String LoginToken1;
	public static String LoginToken2;
	public static String assignEntityType;
	public static String assignEntityId;
	public static String assignRole;
	public static String assignEsclationId;
	public static String assignFromRole;
	public static String assignToRole;
	public static String assignNotes;
	public static String assignName;
	public static String assignEmail;
	public static String assignUsername;
	public static String assignUsername1;
	public static String assignUsername2;
	public static String assignPassword;
	public static String assignPortal;
	public static String[] assignRoles;
	public static String dataEntityType;
	public static String dataEntityId;
	public static String dataName;
	public static String dataSecondaryName;
	public static String ruleName;
	public static String tempName;
	public static String tempVersion;
	public static String tempFormat;
	public static String tokenEntityType;
	public static String tokenEntityId;
	public static String token;
	public static String statusManagementEntitytype;
	public static String statusManagementEntityid;
	public static String statusManagementProductId;
	public static String statusManagementStatusWorkFlowId;
	public static String statusManagementParentStatus;
	public static String statusManagementSubWorkFlowId;
	public static String statusManagementSubWorkflowStatus;
	public static String statusManagementNewStatus;
	public static String eventStoreId;
	public static String productRuleEntityType;
	public static String productRuleEntityId;
	public static String productRuleProductId;
	public static String productRuleRuleId;
	public static String productRuleRuleType;
	public static String productRuleClassName;
	public static String productRuleRuleName;
	public static String productRuleScoreCardId;
	public static String productRuleScoreCardVariableId;
	public static String productRuleScoreCardName;
	public static String productRulePricingStrategyId;
	public static String productRulePricingStrategyName;
	public static String productRulePricingStrategyVersion;
	public static String productRulePricingStrategyStepId;
	public static String productRuleSegmentId;
	public static String productRuleSegmentName;
	public static String productRuleLookupId;
	public static String productRuleLookupName;
	public static String productRuleTemplateId;
	public static String productRuleTemplateName;
	public static String emailVeriEntityType;
	public static String emailVeriEntityId;
	public static String verificationEmailId;
	public static String emailName;
	public static String emailVerificationCode;
	public static String source;
	public static String activityLogEntityType;
	public static String activityLogEntityId;
	public static String activityLogProductId;
	public static String activityLogFacts;
	public static long StartTime;
	public static long EndTime;

	public static String getEmailVeriEntityType() {
		return emailVeriEntityType;
	}

	public static void setEmailVeriEntityType(String emailVeriEntityType) {
		ApiParam.emailVeriEntityType = emailVeriEntityType;
	}

	public static String getEmailVeriEntityId() {
		return emailVeriEntityId;
	}

	public static void setEmailVeriEntityId(String emailVeriEntityId) {
		ApiParam.emailVeriEntityId = emailVeriEntityId;
	}

	public static String getVerificationEmailId() {
		return verificationEmailId;
	}

	public static void setVerificationEmailId(String verificationEmailId) {
		ApiParam.verificationEmailId = verificationEmailId;
	}

	public static String getEmailName() {
		return emailName;
	}

	public static void setEmailName(String emailName) {
		ApiParam.emailName = emailName;
	}

	public static String getEmailVerificationCode() {
		return emailVerificationCode;
	}

	public static void setEmailVerificationCode(String emailVerificationCode) {
		ApiParam.emailVerificationCode = emailVerificationCode;
	}

	public static String getSource() {
		return source;
	}

	public static void setSource(String source) {
		ApiParam.source = source;
	}

	public static String getActivityLogEntityType() {
		return activityLogEntityType;
	}

	public static void setActivityLogEntityType(String activityLogEntityType) {
		ApiParam.activityLogEntityType = activityLogEntityType;
	}

	public static String getActivityLogEntityId() {
		return activityLogEntityId;
	}

	public static void setActivityLogEntityId(String activityLogEntityId) {
		ApiParam.activityLogEntityId = activityLogEntityId;
	}

	public static String getActivityLogProductId() {
		return activityLogProductId;
	}

	public static void setActivityLogProductId(String activityLogProductId) {
		ApiParam.activityLogProductId = activityLogProductId;
	}

	public static String getActivityLogFacts() {
		return activityLogFacts;
	}

	public static void setActivityLogFacts(String activityLogFacts) {
		ApiParam.activityLogFacts = activityLogFacts;
	}

	public static String getEventStoreId() {
		return eventStoreId;
	}

	public static void setEventStoreId(String eventStoreId) {
		ApiParam.eventStoreId = eventStoreId;
	}

	public static String getProductRuleEntityType() {
		return productRuleEntityType;
	}

	public static void setProductRuleEntityType(String productRuleEntityType) {
		ApiParam.productRuleEntityType = productRuleEntityType;
	}

	public static String getProductRuleEntityId() {
		return productRuleEntityId;
	}

	public static void setProductRuleEntityId(String productRuleEntityId) {
		ApiParam.productRuleEntityId = productRuleEntityId;
	}

	public static String getProductRuleProductId() {
		return productRuleProductId;
	}

	public static void setProductRuleProductId(String productRuleProductId) {
		ApiParam.productRuleProductId = productRuleProductId;
	}

	public static String getProductRuleRuleId() {
		return productRuleRuleId;
	}

	public static void setProductRuleRuleId(String productRuleRuleId) {
		ApiParam.productRuleRuleId = productRuleRuleId;
	}

	public static String getProductRuleRuleType() {
		return productRuleRuleType;
	}

	public static void setProductRuleRuleType(String productRuleRuleType) {
		ApiParam.productRuleRuleType = productRuleRuleType;
	}

	public static String getProductRuleClassName() {
		return productRuleClassName;
	}

	public static void setProductRuleClassName(String productRuleClassName) {
		ApiParam.productRuleClassName = productRuleClassName;
	}

	public static String getProductRuleRuleName() {
		return productRuleRuleName;
	}

	public static void setProductRuleRuleName(String productRuleRuleName) {
		ApiParam.productRuleRuleName = productRuleRuleName;
	}

	public static String getProductRuleScoreCardId() {
		return productRuleScoreCardId;
	}

	public static void setProductRuleScoreCardId(String productRuleScoreCardId) {
		ApiParam.productRuleScoreCardId = productRuleScoreCardId;
	}

	public static String getProductRuleScoreCardVariableId() {
		return productRuleScoreCardVariableId;
	}

	public static void setProductRuleScoreCardVariableId(String productRuleScoreCardVariableId) {
		ApiParam.productRuleScoreCardVariableId = productRuleScoreCardVariableId;
	}

	public static String getProductRuleScoreCardName() {
		return productRuleScoreCardName;
	}

	public static void setProductRuleScoreCardName(String productRuleScoreCardName) {
		ApiParam.productRuleScoreCardName = productRuleScoreCardName;
	}

	public static String getProductRulePricingStrategyId() {
		return productRulePricingStrategyId;
	}

	public static void setProductRulePricingStrategyId(String productRulePricingStrategyId) {
		ApiParam.productRulePricingStrategyId = productRulePricingStrategyId;
	}

	public static String getProductRulePricingStrategyName() {
		return productRulePricingStrategyName;
	}

	public static void setProductRulePricingStrategyName(String productRulePricingStrategyName) {
		ApiParam.productRulePricingStrategyName = productRulePricingStrategyName;
	}

	public static String getProductRulePricingStrategyVersion() {
		return productRulePricingStrategyVersion;
	}

	public static void setProductRulePricingStrategyVersion(String productRulePricingStrategyVersion) {
		ApiParam.productRulePricingStrategyVersion = productRulePricingStrategyVersion;
	}

	public static String getProductRulePricingStrategyStepId() {
		return productRulePricingStrategyStepId;
	}

	public static void setProductRulePricingStrategyStepId(String productRulePricingStrategyStepId) {
		ApiParam.productRulePricingStrategyStepId = productRulePricingStrategyStepId;
	}

	public static String getProductRuleSegmentId() {
		return productRuleSegmentId;
	}

	public static void setProductRuleSegmentId(String productRuleSegmentId) {
		ApiParam.productRuleSegmentId = productRuleSegmentId;
	}

	public static String getProductRuleSegmentName() {
		return productRuleSegmentName;
	}

	public static void setProductRuleSegmentName(String productRuleSegmentName) {
		ApiParam.productRuleSegmentName = productRuleSegmentName;
	}

	public static String getProductRuleLookupId() {
		return productRuleLookupId;
	}

	public static void setProductRuleLookupId(String productRuleLookupId) {
		ApiParam.productRuleLookupId = productRuleLookupId;
	}

	public static String getProductRuleLookupName() {
		return productRuleLookupName;
	}

	public static void setProductRuleLookupName(String productRuleLookupName) {
		ApiParam.productRuleLookupName = productRuleLookupName;
	}

	public static String getProductRuleTemplateId() {
		return productRuleTemplateId;
	}

	public static void setProductRuleTemplateId(String productRuleTemplateId) {
		ApiParam.productRuleTemplateId = productRuleTemplateId;
	}

	public static String getProductRuleTemplateName() {
		return productRuleTemplateName;
	}

	public static void setProductRuleTemplateName(String productRuleTemplateName) {
		ApiParam.productRuleTemplateName = productRuleTemplateName;
	}

	public static String getStatusManagementEntitytype() {
		return statusManagementEntitytype;
	}

	public static void setStatusManagementEntitytype(String statusManagementEntitytype) {
		ApiParam.statusManagementEntitytype = statusManagementEntitytype;
	}

	public static String getStatusManagementEntityid() {
		return statusManagementEntityid;
	}

	public static void setStatusManagementEntityid(String statusManagementEntityid) {
		ApiParam.statusManagementEntityid = statusManagementEntityid;
	}

	public static String getStatusManagementProductId() {
		return statusManagementProductId;
	}

	public static void setStatusManagementProductId(String statusManagementProductId) {
		ApiParam.statusManagementProductId = statusManagementProductId;
	}

	public static String getStatusManagementStatusWorkFlowId() {
		return statusManagementStatusWorkFlowId;
	}

	public static void setStatusManagementStatusWorkFlowId(String statusManagementStatusWorkFlowId) {
		ApiParam.statusManagementStatusWorkFlowId = statusManagementStatusWorkFlowId;
	}

	public static String getStatusManagementParentStatus() {
		return statusManagementParentStatus;
	}

	public static void setStatusManagementParentStatus(String statusManagementParentStatus) {
		ApiParam.statusManagementParentStatus = statusManagementParentStatus;
	}

	public static String getStatusManagementSubWorkFlowId() {
		return statusManagementSubWorkFlowId;
	}

	public static void setStatusManagementSubWorkFlowId(String statusManagementSubWorkFlowId) {
		ApiParam.statusManagementSubWorkFlowId = statusManagementSubWorkFlowId;
	}

	public static String getStatusManagementSubWorkflowStatus() {
		return statusManagementSubWorkflowStatus;
	}

	public static void setStatusManagementSubWorkflowStatus(String statusManagementSubWorkflowStatus) {
		ApiParam.statusManagementSubWorkflowStatus = statusManagementSubWorkflowStatus;
	}

	public static String getStatusManagementNewStatus() {
		return statusManagementNewStatus;
	}

	public static void setStatusManagementNewStatus(String statusManagementNewStatus) {
		ApiParam.statusManagementNewStatus = statusManagementNewStatus;
	}

	public static String getDataEntityType() {
		return dataEntityType;
	}

	public static void setDataEntityType(String dataEntityType) {
		ApiParam.dataEntityType = dataEntityType;
	}

	public static String getDataEntityId() {
		return dataEntityId;
	}

	public static void setDataEntityId(String dataEntityId) {
		ApiParam.dataEntityId = dataEntityId;
	}

	public static String getDataName() {
		return dataName;
	}

	public static void setDataName(String dataName) {
		ApiParam.dataName = dataName;
	}

	public static String getDataSecondaryName() {
		return dataSecondaryName;
	}

	public static void setDataSecondaryName(String dataSecondaryName) {
		ApiParam.dataSecondaryName = dataSecondaryName;
	}

	public static String getRuleName() {
		return ruleName;
	}

	public static void setRuleName(String ruleName) {
		ApiParam.ruleName = ruleName;
	}

	public static String getTempName() {
		return tempName;
	}

	public static void setTempName(String tempName) {
		ApiParam.tempName = tempName;
	}

	public static String getTempVersion() {
		return tempVersion;
	}

	public static void setTempVersion(String tempVersion) {
		ApiParam.tempVersion = tempVersion;
	}

	public static String getTempFormat() {
		return tempFormat;
	}

	public static void setTempFormat(String tempFormat) {
		ApiParam.tempFormat = tempFormat;
	}

	public static String getTokenEntityType() {
		return tokenEntityType;
	}

	public static void setTokenEntityType(String tokenEntityType) {
		ApiParam.tokenEntityType = tokenEntityType;
	}

	public static String getTokenEntityId() {
		return tokenEntityId;
	}

	public static void setTokenEntityId(String tokenEntityId) {
		ApiParam.tokenEntityId = tokenEntityId;
	}

	public static String getToken() {
		return token;
	}

	public static void setToken(String token) {
		ApiParam.token = token;
	}

	public static String getAssignUsername1() {
		return assignUsername1;
	}

	public static void setAssignUsername1(String assignUsername1) {
		ApiParam.assignUsername1 = assignUsername1;
	}

	public static String getAssignUsername2() {
		return assignUsername2;
	}

	public static void setAssignUsername2(String assignUsername2) {
		ApiParam.assignUsername2 = assignUsername2;
	}

	public static String getLoginToken1() {
		return LoginToken1;
	}

	public static void setLoginToken1(String loginToken1) {
		LoginToken1 = loginToken1;
	}

	public static String getLoginToken2() {
		return LoginToken2;
	}

	public static void setLoginToken2(String loginToken2) {
		LoginToken2 = loginToken2;
	}

	public static String getAssignEntityType() {
		return assignEntityType;
	}

	public static void setAssignEntityType(String assignEntityType) {
		ApiParam.assignEntityType = assignEntityType;
	}

	public static String getAssignEntityId() {
		return assignEntityId;
	}

	public static void setAssignEntityId(String assignEntityId) {
		ApiParam.assignEntityId = assignEntityId;
	}

	public static String getAssignRole() {
		return assignRole;
	}

	public static void setAssignRole(String assignRole) {
		ApiParam.assignRole = assignRole;
	}

	public static String getAssignEsclationId() {
		return assignEsclationId;
	}

	public static void setAssignEsclationId(String assignEsclationId) {
		ApiParam.assignEsclationId = assignEsclationId;
	}

	public static String getAssignFromRole() {
		return assignFromRole;
	}

	public static void setAssignFromRole(String assignFromRole) {
		ApiParam.assignFromRole = assignFromRole;
	}

	public static String getAssignToRole() {
		return assignToRole;
	}

	public static void setAssignToRole(String assignToRole) {
		ApiParam.assignToRole = assignToRole;
	}

	public static String getAssignNotes() {
		return assignNotes;
	}

	public static void setAssignNotes(String assignNotes) {
		ApiParam.assignNotes = assignNotes;
	}

	public static String getAssignName() {
		return assignName;
	}

	public static void setAssignName(String assignName) {
		ApiParam.assignName = assignName;
	}

	public static String getAssignEmail() {
		return assignEmail;
	}

	public static void setAssignEmail(String assignEmail) {
		ApiParam.assignEmail = assignEmail;
	}

	public static String getAssignUsername() {
		return assignUsername;
	}

	public static void setAssignUsername(String assignUsername) {
		ApiParam.assignUsername = assignUsername;
	}

	public static String getAssignPassword() {
		return assignPassword;
	}

	public static void setAssignPassword(String assignPassword) {
		ApiParam.assignPassword = assignPassword;
	}

	public static String getAssignPortal() {
		return assignPortal;
	}

	public static void setAssignPortal(String assignPortal) {
		ApiParam.assignPortal = assignPortal;
	}

	public static String[] getAssignRoles() {
		return assignRoles;
	}

	public static void setAssignRoles(String[] assignRoles) {
		ApiParam.assignRoles = assignRoles;
	}

	public static String getSMSentitytype() {
		return SMSentitytype;
	}

	public static void setSMSentitytype(String sMSentitytype) {
		SMSentitytype = sMSentitytype;
	}

	public static String getSMSentityid() {
		return SMSentityid;
	}

	public static void setSMSentityid(String sMSentityid) {
		SMSentityid = sMSentityid;
	}

	public static String getSMStemplateName() {
		return SMStemplateName;
	}

	public static void setSMStemplateName(String sMStemplateName) {
		SMStemplateName = sMStemplateName;
	}

	public static String getSMStemplateVersion() {
		return SMStemplateVersion;
	}

	public static void setSMStemplateVersion(String sMStemplateVersion) {
		SMStemplateVersion = sMStemplateVersion;
	}

	public static String getSMSrecipientPhone() {
		return SMSrecipientPhone;
	}

	public static void setSMSrecipientPhone(String sMSrecipientPhone) {
		SMSrecipientPhone = sMSrecipientPhone;
	}

	public static String getSMScommonSMSText() {
		return SMScommonSMSText;
	}

	public static void setSMScommonSMSText(String sMScommonSMSText) {
		SMScommonSMSText = sMScommonSMSText;
	}

	public static String getSMSmessage() {
		return SMSmessage;
	}

	public static void setSMSmessage(String sMSmessage) {
		SMSmessage = sMSmessage;
	}

	public static String getWorkflow() {
		return workflow;
	}

	public static void setWorkflow(String workflow) {
		ApiParam.workflow = workflow;
	}

	public static String getAppFlowID() {
		return appFlowID;
	}

	public static void setAppFlowID(String appFlowID) {
		ApiParam.appFlowID = appFlowID;
	}

	public static String getStep() {
		return step;
	}

	public static void setStep(String step) {
		ApiParam.step = step;
	}

	public static String getAlertEntityType() {
		return alertEntityType;
	}

	public static void setAlertEntityType(String alertEntityType) {
		ApiParam.alertEntityType = alertEntityType;
	}

	public static String getAlertEntityId() {
		return alertEntityId;
	}

	public static void setAlertEntityId(String alertEntityId) {
		ApiParam.alertEntityId = alertEntityId;
	}

	public static String getAlertStatus() {
		return alertStatus;
	}

	public static void setAlertStatus(String alertStatus) {
		ApiParam.alertStatus = alertStatus;
	}

	public static String getAlertID() {
		return alertID;
	}

	public static void setAlertID(String alertID) {
		ApiParam.alertID = alertID;
	}

	public static String getProductId() {
		return productId;
	}

	public static void setProductId(String productId) {
		ApiParam.productId = productId;
	}

	public static String getFact() {
		return fact;
	}

	public static void setFact(String fact) {
		ApiParam.fact = fact;
	}

	public static String getMethod() {
		return method;
	}

	public static void setMethod(String method) {
		ApiParam.method = method;
	}

	public static String getVerificationStatus() {
		return verificationStatus;
	}

	public static void setVerificationStatus(String verificationStatus) {
		ApiParam.verificationStatus = verificationStatus;
	}

	public static String getVerificationReason() {
		return verificationReason;
	}

	public static void setVerificationReason(String verificationReason) {
		ApiParam.verificationReason = verificationReason;
	}

	public static String getWorkFlowStatusId() {
		return workFlowStatusId;
	}

	public static void setWorkFlowStatusId(String workFlowStatusId) {
		ApiParam.workFlowStatusId = workFlowStatusId;
	}

	public static String getVerificationEntityType() {
		return verificationEntityType;
	}

	public static void setVerificationEntityType(String verificationEntityType) {
		ApiParam.verificationEntityType = verificationEntityType;
	}

	public static String getVerificationEntityId() {
		return verificationEntityId;
	}

	public static void setVerificationEntityId(String verificationEntityId) {
		ApiParam.verificationEntityId = verificationEntityId;
	}

	public static String getDocumentId() {
		return documentId;
	}

	public static void setDocumentId(String documentId) {
		ApiParam.documentId = documentId;
	}

	public static String getVerificationSource() {
		return verificationSource;
	}

	public static void setVerificationSource(String verificationSource) {
		ApiParam.verificationSource = verificationSource;
	}

	public static String getEmailEntityType() {
		return emailEntityType;
	}

	public static void setEmailEntityType(String emailEntityType) {
		ApiParam.emailEntityType = emailEntityType;
	}

	public static String getMailId() {
		return mailId;
	}

	public static void setMailId(String mailId) {
		ApiParam.mailId = mailId;
	}

	public static String getEmailEntityId() {
		return emailEntityId;
	}

	public static void setEmailEntityId(String emailEntityId) {
		ApiParam.emailEntityId = emailEntityId;
	}

	public static String getEmailTemplateName() {
		return emailTemplateName;
	}

	public static void setEmailTemplateName(String emailTemplateName) {
		ApiParam.emailTemplateName = emailTemplateName;
	}

	public static String getEmailTemplateVersion() {
		return emailTemplateVersion;
	}

	public static void setEmailTemplateVersion(String emailTemplateVersion) {
		ApiParam.emailTemplateVersion = emailTemplateVersion;
	}

	public static String getToName() {
		return toName;
	}

	public static void setToName(String toName) {
		ApiParam.toName = toName;
	}

	public static String getSubject() {
		return subject;
	}

	public static void setSubject(String subject) {
		ApiParam.subject = subject;
	}

	public static String getBody() {
		return body;
	}

	public static void setBody(String body) {
		ApiParam.body = body;
	}

	public static Object getAttachmentIds() {
		return attachmentIds;
	}

	public static void setAttachmentIds(Object attachmentIds) {
		ApiParam.attachmentIds = attachmentIds;
	}

	public static JSONArray getCcEmailAddress() {
		return ccEmailAddress;
	}

	public static void setCcEmailAddress(JSONArray ccEmailAddress) {
		ApiParam.ccEmailAddress = ccEmailAddress;
	}

	public static JSONArray getBccEmailAddress() {
		return bccEmailAddress;
	}

	public static void setBccEmailAddress(JSONArray bccEmailAddress) {
		ApiParam.bccEmailAddress = bccEmailAddress;
	}

	public static JSONArray getToEmailAddress() {
		return toEmailAddress;
	}

	public static void setToEmailAddress(JSONArray toEmailAddress) {
		ApiParam.toEmailAddress = toEmailAddress;
	}

	public static LocalDate getCurrentDate() {
		return currentDate;
	}

	public static void setCurrentDate(LocalDate currentDate) {
		ApiParam.currentDate = currentDate;
	}

	public static String getTerm() {
		return term;
	}

	public static void setTerm(String term) {
		ApiParam.term = term;
	}

	public static String getPeriodicity() {
		return periodicity;
	}

	public static void setPeriodicity(String periodicity) {
		ApiParam.periodicity = periodicity;
	}

	public static String getAchreferenceNumber() {
		return achreferenceNumber;
	}

	public static void setAchreferenceNumber(String achreferenceNumber) {
		ApiParam.achreferenceNumber = achreferenceNumber;
	}

	public static String getFundingSourceId() {
		return fundingSourceId;
	}

	public static void setFundingSourceId(String fundingSourceId) {
		ApiParam.fundingSourceId = fundingSourceId;
	}

	public static String getAchstatus() {
		return achstatus;
	}

	public static void setAchstatus(String achstatus) {
		ApiParam.achstatus = achstatus;
	}

	public static int getInternalReferenceNumber() {
		return internalReferenceNumber;
	}

	public static void setInternalReferenceNumber(int internalReferenceNumber) {
		ApiParam.internalReferenceNumber = internalReferenceNumber;
	}

	public static String getReason() {
		return reason;
	}

	public static void setReason(String reason) {
		ApiParam.reason = reason;
	}

	public static String getInstructionId() {
		return InstructionId;
	}

	public static void setInstructionId(String instructionId) {
		InstructionId = instructionId;
	}

	public static LocalDateTime getExecutionDate() {
		return executionDate;
	}

	public static void setExecutionDate(LocalDateTime executionDate) {
		ApiParam.executionDate = executionDate;
	}

	public static String getProcessStartDateTime() {
		return processStartDateTime;
	}

	public static void setProcessStartDateTime(String processStartDateTime) {
		ApiParam.processStartDateTime = processStartDateTime;
	}

	public static String getProcessEndDateTime() {
		return processEndDateTime;
	}

	public static void setProcessEndDateTime(String processEndDateTime) {
		ApiParam.processEndDateTime = processEndDateTime;
	}

	public static String getFileID() {
		return fileID;
	}

	public static void setFileID(String fileID) {
		ApiParam.fileID = fileID;
	}

	public static String getActionEntityID() {
		return actionEntityID;
	}

	public static void setActionEntityID(String actionEntityID) {
		ApiParam.actionEntityID = actionEntityID;
	}

	public static String getActionEntityType() {
		return actionEntityType;
	}

	public static void setActionEntityType(String actionEntityType) {
		ApiParam.actionEntityType = actionEntityType;
	}

	public static String getRequestId() {
		return requestId;
	}

	public static void setRequestId(String requestId) {
		ApiParam.requestId = requestId;
	}

	public static String getActionStatus() {
		return actionStatus;
	}

	public static void setActionStatus(String actionStatus) {
		ApiParam.actionStatus = actionStatus;
	}

	public static String getActionId() {
		return actionId;
	}

	public static void setActionId(String actionId) {
		ApiParam.actionId = actionId;
	}

	public static String getTemplateName() {
		return templateName;
	}

	public static void setTemplateName(String templateName) {
		ApiParam.templateName = templateName;
	}

	public static String getVersion() {
		return version;
	}

	public static void setVersion(String version) {
		ApiParam.version = version;
	}

	public static String getFormat() {
		return format;
	}

	public static void setFormat(String format) {
		ApiParam.format = format;
	}

	public static String getConsumerEntityId() {
		return consumerEntityId;
	}

	public static void setConsumerEntityId(String consumerEntityId) {
		ApiParam.consumerEntityId = consumerEntityId;
	}

	public static Set<String> getEntityid() {
		return entityid;
	}

	public static void setEntityid(Set<String> entityid) {
		ApiParam.entityid = entityid;
	}

	public static String getStatus() {
		return status;
	}

	public static void setStatus(String status) {
		ApiParam.status = status;
	}

	public static String getSubEntityIds() {
		return subEntityIds;
	}

	public static void setSubEntityIds(String subEntityIds) {
		ApiParam.subEntityIds = subEntityIds;
	}

	public static String getReferenceNumber() {
		return referenceNumber;
	}

	public static void setReferenceNumber(String referenceNumber) {
		ApiParam.referenceNumber = referenceNumber;
	}

	public static String getVerificationCode() {
		return verificationCode;
	}

	public static void setVerificationCode(String verificationCode) {
		ApiParam.verificationCode = verificationCode;
	}

	public static String getPhone() {
		return phone;
	}

	public static void setPhone(String phone) {
		ApiParam.phone = phone;
	}

	public static String getMobile() {
		return mobile;
	}

	public static void setMobile(String mobile) {
		ApiParam.mobile = mobile;
	}


	public static String getConsumersID() {
		return consumersID;
	}

	public static void setConsumersID(String consumersID) {
		ApiParam.consumersID = consumersID;
	}

	public static String getField() {
		return field;
	}

	public static void setField(String field) {
		ApiParam.field = field;
	}

	public static String getEntityId() {
		return entityId;
	}

	public static void setEntityId(String entityId) {
		ApiParam.entityId = entityId;
	}

	public static String getEntityType() {
		return entityType;
	}

	public static void setEntityType(String entityType) {
		ApiParam.entityType = entityType;
	}

	public static String getCurrentFeatureFile() {
		return currentFeatureFile;
	}

	public static void setCurrentFeatureFile(String currentFeatureFile) {
		ApiParam.currentFeatureFile = currentFeatureFile;
	}

	public static String getBaseresturl() {
		return baseresturl;
	}

	public static void setBaseresturl(String baseresturl) {
		ApiParam.baseresturl = baseresturl;
	}

	public static String getAuthToken() {
		return AuthToken;
	}

	public static void setAuthToken(String authToken) {
		AuthToken = authToken;
	}

	public static String getRequestType() {
		return requestType;
	}

	public static void setRequestType(String requestType) {
		ApiParam.requestType = requestType;
	}

	public static String getContentType() {
		return contentType;
	}

	public static void setContentType(String contentType) {
		ApiParam.contentType = contentType;
	}

	public static String getEnvironment() {
		return environment;
	}

	public static void setEnvironment(String environment) {
		ApiParam.environment = environment;
	}

	public static String getASPort() {
		return ASPort;
	}

	public static void setASPort(String aSPort) {
		ASPort = aSPort;
	}

	public static String getScenarioName() {
		return ScenarioName;
	}

	public static void setScenarioName(String scenarioName) {
		ScenarioName = scenarioName;
	}

	public static String getCurrentScenarioName() {
		return currentScenarioName;
	}

	public static void setCurrentScenarioName(String currentScenarioName) {
		ApiParam.currentScenarioName = currentScenarioName;
	}

	public static String getName() {
		return name;
	}

	public static void setName(String Name) {
		name = Name;
	}

	public static String getEmail() {
		return email;
	}

	public static void setEmail(String Email) {
		email = Email;
	}

	public static String getUsername() {
		return username;
	}

	public static void setUsername(String Username) {
		username = Username;
	}

	public static String[] getRoles() {
		return roles;
	}

	public static void setRoles(String[] Roles) {
		roles = Roles;
	}

	public static boolean isActive() {
		return isActive;
	}

	public static void setActive(boolean IsActive) {
		isActive = IsActive;
	}

	public static String getPasswordExpirationDate() {
		return passwordExpirationDate;
	}

	public static void setPasswordExpirationDate(String PasswordExpirationDate) {
		passwordExpirationDate = PasswordExpirationDate;
	}

	public static String getPortal() {
		return portal;
	}

	public static void setPortal(String Portal) {
		portal = Portal;
	}

	public static String getPassword() {
		return password;
	}

	public static void setPassword(String Password) {
		password = Password;
	}

	public static String getLoginToken() {
		return LoginToken;
	}

	public static void setLoginToken(String loginToken) {
		LoginToken = loginToken;
	}

	public static String getResetToken() {
		return resetToken;
	}

	public static void setResetToken(String ResetToken) {
		resetToken = ResetToken;
	}

	public static String getId() {
		return id;
	}

	public static void setId(String Id) {
		id = Id;
	}

	public static long getStartTime() {
		return StartTime;
	}

	public static void setStartTime(long startTime) {
		StartTime = startTime;
	}

	public static long getEndTime() {
		return EndTime;
	}

	public static void setEndTime(long endTime) {
		EndTime = endTime;
	}


}
