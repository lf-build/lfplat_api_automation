package net.sigmainfo.lf.automation.api.dataset;

public class AddTemplateData {
	private String emailTemplateName;
	private String emailTemplateVersion;
	private String mobileTemplateName;
	private String mobileTemplateVersion;
	private String description;
	private String effectiveDate;
	private String expiryDate;
	private Boolean isEnable;
	private String eventName;
	private String ruleDefinationName;
	private String deRuleVersion;
	private String createdDate;
	private String updatedDate;

	public String getEmailTemplateName() {
	return emailTemplateName;
	}

	public void setEmailTemplateName(String emailTemplateName) {
	this.emailTemplateName = emailTemplateName;
	}

	public String getEmailTemplateVersion() {
	return emailTemplateVersion;
	}

	public void setEmailTemplateVersion(String emailTemplateVersion) {
	this.emailTemplateVersion = emailTemplateVersion;
	}

	public String getMobileTemplateName() {
	return mobileTemplateName;
	}

	public void setMobileTemplateName(String mobileTemplateName) {
	this.mobileTemplateName = mobileTemplateName;
	}

	public String getMobileTemplateVersion() {
	return mobileTemplateVersion;
	}

	public void setMobileTemplateVersion(String mobileTemplateVersion) {
	this.mobileTemplateVersion = mobileTemplateVersion;
	}

	public String getDescription() {
	return description;
	}

	public void setDescription(String description) {
	this.description = description;
	}

	public String getEffectiveDate() {
	return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
	this.effectiveDate = effectiveDate;
	}

	public String getExpiryDate() {
	return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
	this.expiryDate = expiryDate;
	}

	public Boolean getIsEnable() {
	return isEnable;
	}

	public void setIsEnable(Boolean isEnable) {
	this.isEnable = isEnable;
	}

	public String getEventName() {
	return eventName;
	}

	public void setEventName(String eventName) {
	this.eventName = eventName;
	}

	public String getRuleDefinationName() {
	return ruleDefinationName;
	}

	public void setRuleDefinationName(String ruleDefinationName) {
	this.ruleDefinationName = ruleDefinationName;
	}

	public String getDeRuleVersion() {
	return deRuleVersion;
	}

	public void setDeRuleVersion(String deRuleVersion) {
	this.deRuleVersion = deRuleVersion;
	}

	public String getCreatedDate() {
	return createdDate;
	}

	public void setCreatedDate(String createdDate) {
	this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
	return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
	this.updatedDate = updatedDate;
	}
}
