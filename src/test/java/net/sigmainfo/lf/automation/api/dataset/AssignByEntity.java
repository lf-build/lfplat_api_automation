package net.sigmainfo.lf.automation.api.dataset;

public class AssignByEntity {
	private String user;
	private String role;

	public String getUser() {
	return user;
	}

	public void setUser(String user) {
	this.user = user;
	}

	public String getRole() {
	return role;
	}

	public void setRole(String role) {
	this.role = role;
	}

}
