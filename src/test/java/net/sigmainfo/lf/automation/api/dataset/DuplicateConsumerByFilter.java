package net.sigmainfo.lf.automation.api.dataset;

import com.fasterxml.jackson.databind.JsonNode;

public class DuplicateConsumerByFilter {

	public JsonNode additionalProp1;
	public JsonNode additionalProp2;
	public JsonNode additionalProp3;



	public JsonNode getAdditionalProp1() {
		return this.additionalProp1;
	}

	public void setAdditionalProp1(JsonNode additionalProp1) {
	this.additionalProp1 = additionalProp1;}

	public JsonNode getAdditionalProp2() {
		return this.additionalProp2;
	}

	public void setAdditionalProp2(JsonNode additionalProp2) {
	this.additionalProp2 = additionalProp2;}

	public JsonNode getAdditionalProp3() {
		return this.additionalProp3;
	}

	public void setAdditionalProp3(JsonNode additionalProp3) {
	this.additionalProp3 = additionalProp3;
	}

	public class AdditionalProp3 {


	}
	
	public class AdditionalProp2 {


	}

	public class AdditionalProp1 {


	}

}
