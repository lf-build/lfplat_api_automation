package net.sigmainfo.lf.automation.api.dataset;

import java.util.List;

public class UpdateLookUpData {

	private String effectiveDate;
	private String expiryDate;
	private List<String> lookupBy = null;
	private List<String> lookupValues = null;
	private String lookupStorageName;
	private String description;
	private String lastUpdatedDate;
	private String lastUpdatedBy;
	private String dataAttributeStoreName;
	private String ruleDefinitionId;
	private LookupData lookupData;
	private String customDate;
	private String applicationType;

	public String getEffectiveDate() {
	return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
	this.effectiveDate = effectiveDate;
	}

	public String getExpiryDate() {
	return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
	this.expiryDate = expiryDate;
	}

	public List<String> getLookupBy() {
	return lookupBy;
	}

	public void setLookupBy(List<String> lookupBy) {
	this.lookupBy = lookupBy;
	}

	public List<String> getLookupValues() {
	return lookupValues;
	}

	public void setLookupValues(List<String> lookupValues) {
	this.lookupValues = lookupValues;
	}

	public String getLookupStorageName() {
	return lookupStorageName;
	}

	public void setLookupStorageName(String lookupStorageName) {
	this.lookupStorageName = lookupStorageName;
	}

	public String getDescription() {
	return description;
	}

	public void setDescription(String description) {
	this.description = description;
	}

	public String getLastUpdatedDate() {
	return lastUpdatedDate;
	}

	public void setLastUpdatedDate(String lastUpdatedDate) {
	this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLastUpdatedBy() {
	return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
	this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getDataAttributeStoreName() {
	return dataAttributeStoreName;
	}

	public void setDataAttributeStoreName(String dataAttributeStoreName) {
	this.dataAttributeStoreName = dataAttributeStoreName;
	}

	public String getRuleDefinitionId() {
	return ruleDefinitionId;
	}

	public void setRuleDefinitionId(String ruleDefinitionId) {
	this.ruleDefinitionId = ruleDefinitionId;
	}

	public LookupData getLookupData() {
	return lookupData;
	}

	public void setLookupData(LookupData lookupData) {
	this.lookupData = lookupData;
	}

	public String getCustomDate() {
	return customDate;
	}

	public void setCustomDate(String customDate) {
	this.customDate = customDate;
	}

	public String getApplicationType() {
	return applicationType;
	}

	public void setApplicationType(String applicationType) {
	this.applicationType = applicationType;
	}


	public class LookupData {


	}
}
