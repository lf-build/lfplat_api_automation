package net.sigmainfo.lf.automation.api.dataset;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SendSmsEntityType {
	private String RecipientPhone;
	private String CommonSMSText;
	
	 @JsonProperty("RecipientPhone")
	public String getRecipientPhone() {
		return this.RecipientPhone;
	}
	public void setRecipientPhone(String RecipientPhone) {
		this.RecipientPhone = RecipientPhone;
	}
	
	@JsonProperty("CommonSMSText")
	public String getCommonSMSText() {
		return this.CommonSMSText;
	}

	public void setCommonSMSText(String CommonSMSText) {
		this.CommonSMSText = CommonSMSText;
	}



}
