package net.sigmainfo.lf.automation.api.dataset;

public class AddPricingStrategy {
	private String name;
	private String description;
	private String version;
	private String effectiveDate;
	private String expiryDate;

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public String getDescription() {
	return description;
	}

	public void setDescription(String description) {
	this.description = description;
	}

	public String getVersion() {
	return version;
	}

	public void setVersion(String version) {
	this.version = version;
	}

	public String getEffectiveDate() {
	return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
	this.effectiveDate = effectiveDate;
	}

	public String getExpiryDate() {
	return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
	this.expiryDate = expiryDate;
	}
}
