package net.sigmainfo.lf.automation.api.dataset;

public class RejectInstruction {
	private String reason;

	public String getReason() {
	return reason;
	}

	public void setReason(String reason) {
	this.reason = reason;
	}
}
