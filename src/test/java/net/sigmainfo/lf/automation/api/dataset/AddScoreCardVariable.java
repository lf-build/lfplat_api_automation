package net.sigmainfo.lf.automation.api.dataset;

public class AddScoreCardVariable {
	private String variableName;
	private String variableType;
	private String definitionId;
	private String dataAttributeStoreName;
	private ScoreCardVariableSource scoreCardVariableSource;
	private Integer weightage;
	private String description;

	public String getVariableName() {
	return variableName;
	}

	public void setVariableName(String variableName) {
	this.variableName = variableName;
	}

	public String getVariableType() {
	return variableType;
	}

	public void setVariableType(String variableType) {
	this.variableType = variableType;
	}

	public String getDefinitionId() {
	return definitionId;
	}

	public void setDefinitionId(String definitionId) {
	this.definitionId = definitionId;
	}

	public String getDataAttributeStoreName() {
	return dataAttributeStoreName;
	}

	public void setDataAttributeStoreName(String dataAttributeStoreName) {
	this.dataAttributeStoreName = dataAttributeStoreName;
	}

	public ScoreCardVariableSource getScoreCardVariableSource() {
	return scoreCardVariableSource;
	}

	public void setScoreCardVariableSource(ScoreCardVariableSource scoreCardVariableSource) {
	this.scoreCardVariableSource = scoreCardVariableSource;
	}

	public Integer getWeightage() {
	return weightage;
	}

	public void setWeightage(Integer weightage) {
	this.weightage = weightage;
	}

	public String getDescription() {
	return description;
	}

	public void setDescription(String description) {
	this.description = description;
	}

	public class ScoreCardVariableSource {

	private String inputSource;
	private String inputVariable;

	public String getInputSource() {
	return inputSource;
	}

	public void setInputSource(String inputSource) {
	this.inputSource = inputSource;
	}

	public String getInputVariable() {
	return inputVariable;
	}

	public void setInputVariable(String inputVariable) {
	this.inputVariable = inputVariable;
	}
	}
}
