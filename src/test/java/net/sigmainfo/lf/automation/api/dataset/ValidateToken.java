package net.sigmainfo.lf.automation.api.dataset;

public class ValidateToken {
	private String entityId;
	private String entityType;
	private String token;

	public String getEntityId() {
	return entityId;
	}

	public void setEntityId(String entityId) {
	this.entityId = entityId;
	}

	public String getEntityType() {
	return entityType;
	}

	public void setEntityType(String entityType) {
	this.entityType = entityType;
	}

	public String getToken() {
	return token;
	}

	public void setToken(String token) {
	this.token = token;
	}

}
