package net.sigmainfo.lf.automation.api.dataset;

public class ResetPassword {
	private String username;
	private String password;
	private String portal;

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String Password) {
		this.password = Password;
	}

	public String getPortal() {
		return this.portal;
	}

	public void setPortal(String Portal) {
		this.portal = Portal;
	}

	public String getusername() {
	return this.username;
	}

	public void setusername(String Username) {
		 this.username = Username;
	}
}
