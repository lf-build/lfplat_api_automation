package net.sigmainfo.lf.automation.api.dataset;

public class DuplicateConsumerByField {
	private String entityType;
	private String consumerentityId;

	public String getEntityType() {
	return entityType;
	}

	public void setEntityType(String entityType) {
	this.entityType = entityType;
	}

	public String getConsumerentityId() {
		return this.consumerentityId;
	}

	public void setConsumerentityId(String consumerentityId) {
	this.consumerentityId = consumerentityId;}


}
