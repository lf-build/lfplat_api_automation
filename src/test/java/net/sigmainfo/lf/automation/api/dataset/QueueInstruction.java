package net.sigmainfo.lf.automation.api.dataset;

import java.time.LocalDateTime;

public class QueueInstruction {

			private String referenceNumber;
			private String type;
			private String amount;
			private String standardEntryClassCode;
			private String executionDate;
			private String fundingSourceId;
			private Receiving receiving;
			private String frequency;
			private String entryDescription;

			public String getReferenceNumber() {
			return referenceNumber;
			}

			public void setReferenceNumber(String referenceNumber) {
			this.referenceNumber = referenceNumber;
			}

			public String getType() {
			return type;
			}

			public void setType(String type) {
			this.type = type;
			}

			public String getAmount() {
			return amount;
			}

			public void setAmount(String amount) {
			this.amount = amount;
			}

			public String getStandardEntryClassCode() {
			return standardEntryClassCode;
			}

			public void setStandardEntryClassCode(String standardEntryClassCode) {
			this.standardEntryClassCode = standardEntryClassCode;
			}

			public String getExecutionDate() {
				return executionDate;
			}

			public void setExecutionDate(String executionDate) {
				this.executionDate = executionDate;
			}

			public String getFundingSourceId() {
			return fundingSourceId;
			}

			public void setFundingSourceId(String fundingSourceId) {
			this.fundingSourceId = fundingSourceId;
			}

			public Receiving getReceiving() {
			return receiving;
			}

			public void setReceiving(Receiving receiving) {
			this.receiving = receiving;
			}

			public String getFrequency() {
			return frequency;
			}

			public void setFrequency(String frequency) {
			this.frequency = frequency;
			}

			public String getEntryDescription() {
			return entryDescription;
			}

			public void setEntryDescription(String entryDescription) {
			this.entryDescription = entryDescription;
			}


			public class Receiving {

			private String accountType;
			private String routingNumber;
			private String accountNumber;
			private String name;

			public String getAccountType() {
			return accountType;
			}

			public void setAccountType(String accountType) {
			this.accountType = accountType;
			}

			public String getRoutingNumber() {
			return routingNumber;
			}

			public void setRoutingNumber(String routingNumber) {
			this.routingNumber = routingNumber;
			}

			public String getAccountNumber() {
			return accountNumber;
			}

			public void setAccountNumber(String accountNumber) {
			this.accountNumber = accountNumber;
			}

			public String getName() {
			return name;
			}

			public void setName(String name) {
			this.name = name;
			}

			}




}
