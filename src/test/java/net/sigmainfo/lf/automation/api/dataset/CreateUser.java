package net.sigmainfo.lf.automation.api.dataset;

public class CreateUser {
	private String name;
	private String email;
	private String username;
	private String password;
	private String[] roles;
	private boolean isActive;
	private String passwordExpirationDate;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String[] getRoles() {
		return this.roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}

	public boolean isActive() {
		return this.isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getPasswordExpirationDate() {
		return this.passwordExpirationDate;
	}

	public void setPasswordExpirationDate(String passwordExpirationDate) {
		this.passwordExpirationDate = passwordExpirationDate;
	}

}
