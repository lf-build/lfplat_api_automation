package net.sigmainfo.lf.automation.api.dataset;

public class AddSegment {

	private String segmentName;
	private String dataAttributeStoreName;
	private SegmentVariableSource segmentVariableSource;
	private String description;
	private String segmentType;
	private String segmentStorageName;
	private String ruleDefinitionId;
	private String effectiveDate;
	private String expiryDate;
	private Data data;
	private String customDate;
	private String applicationType;

	public String getSegmentName() {
	return segmentName;
	}

	public void setSegmentName(String segmentName) {
	this.segmentName = segmentName;
	}

	public String getDataAttributeStoreName() {
	return dataAttributeStoreName;
	}

	public void setDataAttributeStoreName(String dataAttributeStoreName) {
	this.dataAttributeStoreName = dataAttributeStoreName;
	}

	public SegmentVariableSource getSegmentVariableSource() {
	return segmentVariableSource;
	}

	public void setSegmentVariableSource(SegmentVariableSource segmentVariableSource) {
	this.segmentVariableSource = segmentVariableSource;
	}

	public String getDescription() {
	return description;
	}

	public void setDescription(String description) {
	this.description = description;
	}

	public String getSegmentType() {
	return segmentType;
	}

	public void setSegmentType(String segmentType) {
	this.segmentType = segmentType;
	}

	public String getSegmentStorageName() {
	return segmentStorageName;
	}

	public void setSegmentStorageName(String segmentStorageName) {
	this.segmentStorageName = segmentStorageName;
	}

	public String getRuleDefinitionId() {
	return ruleDefinitionId;
	}

	public void setRuleDefinitionId(String ruleDefinitionId) {
	this.ruleDefinitionId = ruleDefinitionId;
	}

	public String getEffectiveDate() {
	return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
	this.effectiveDate = effectiveDate;
	}

	public String getExpiryDate() {
	return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
	this.expiryDate = expiryDate;
	}

	public Data getData() {
	return data;
	}

	public void setData(Data data) {
	this.data = data;
	}

	public String getCustomDate() {
	return customDate;
	}

	public void setCustomDate(String customDate) {
	this.customDate = customDate;
	}

	public String getApplicationType() {
	return applicationType;
	}

	public void setApplicationType(String applicationType) {
	this.applicationType = applicationType;
	}

	public class Data {


	}



	public class SegmentVariableSource {

	private String inputSource;
	private String inputVariable;

	public String getInputSource() {
	return inputSource;
	}

	public void setInputSource(String inputSource) {
	this.inputSource = inputSource;
	}

	public String getInputVariable() {
	return inputVariable;
	}

	public void setInputVariable(String inputVariable) {
	this.inputVariable = inputVariable;
	}

	}
	
	
}
