package net.sigmainfo.lf.automation.api.dataset;

public class EscalateByEntity {
	private String fromRole;
	private String toRole;
	private String notes;

	public String getFromRole() {
	return fromRole;
	}

	public void setFromRole(String fromRole) {
	this.fromRole = fromRole;
	}

	public String getToRole() {
	return toRole;
	}

	public void setToRole(String toRole) {
	this.toRole = toRole;
	}

	public String getNotes() {
	return notes;
	}

	public void setNotes(String notes) {
	this.notes = notes;
	}

}
