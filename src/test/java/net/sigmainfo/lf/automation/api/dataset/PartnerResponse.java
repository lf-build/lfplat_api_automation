package net.sigmainfo.lf.automation.api.dataset;

import java.util.ArrayList;

public class PartnerResponse {
	private ArrayList<PartnerArray> partnerArray;

	public ArrayList<PartnerArray> getPartnerArray() {
		return this.partnerArray;
	}

	public void setPartnerArray(ArrayList<PartnerArray> partnerArray) {
	this.partnerArray = partnerArray;}

}
