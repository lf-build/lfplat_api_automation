package net.sigmainfo.lf.automation.api.dataset;
import java.util.List;
public class ChangeStatus {

	private String note;
	private List<Object> reasons = null;

	public String getNote() {
	return note;
	}

	public void setNote(String note) {
	this.note = note;
	}

	public List<Object> getReasons() {
	return reasons;
	}

	public void setReasons(List<Object> reasons) {
	this.reasons = reasons;
	}


}
