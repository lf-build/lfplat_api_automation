package net.sigmainfo.lf.automation.api.dataset;

public class AddScoreCard {
	private String ruleDefinitionId;
	private String scoreCardName;
	private String description;
	private String customDate;
	private String applicationType;
	private String effectiveDate;
	private String expiryDate;
	private Boolean isValidated;
	private String validationRuleId;

	public String getRuleDefinitionId() {
	return ruleDefinitionId;
	}

	public void setRuleDefinitionId(String ruleDefinitionId) {
	this.ruleDefinitionId = ruleDefinitionId;
	}

	public String getScoreCardName() {
	return scoreCardName;
	}

	public void setScoreCardName(String scoreCardName) {
	this.scoreCardName = scoreCardName;
	}

	public String getDescription() {
	return description;
	}

	public void setDescription(String description) {
	this.description = description;
	}

	public String getCustomDate() {
	return customDate;
	}

	public void setCustomDate(String customDate) {
	this.customDate = customDate;
	}

	public String getApplicationType() {
	return applicationType;
	}

	public void setApplicationType(String applicationType) {
	this.applicationType = applicationType;
	}

	public String getEffectiveDate() {
	return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
	this.effectiveDate = effectiveDate;
	}

	public String getExpiryDate() {
	return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
	this.expiryDate = expiryDate;
	}

	public Boolean getIsValidated() {
	return isValidated;
	}

	public void setIsValidated(Boolean isValidated) {
	this.isValidated = isValidated;
	}

	public String getValidationRuleId() {
	return validationRuleId;
	}

	public void setValidationRuleId(String validationRuleId) {
	this.validationRuleId = validationRuleId;
	}
}
