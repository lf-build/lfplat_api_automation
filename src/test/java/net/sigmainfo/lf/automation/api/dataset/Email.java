package net.sigmainfo.lf.automation.api.dataset;
import java.util.List;

import org.json.JSONArray;

import com.fasterxml.jackson.databind.JsonNode;
public class Email {


	private List<ToEmailAddress> toEmailAddress = null;
	private List<CcEmailAddress> ccEmailAddress = null;
	private List<BccEmailAddress> bccEmailAddress = null;
		private String toName;
		private String subject;
		private String body;
		private Object attachmentIds;
		
		public List<ToEmailAddress> getToEmailAddress() {
			return toEmailAddress;
		}

		public void setToEmailAddress(List<ToEmailAddress> toEmailAddress) {
			this.toEmailAddress = toEmailAddress;
		}

		public List<CcEmailAddress> getCcEmailAddress() {
			return ccEmailAddress;
		}

		public void setCcEmailAddress(List<CcEmailAddress> ccEmailAddress) {
			this.ccEmailAddress = ccEmailAddress;
		}

		public List<BccEmailAddress> getBccEmailAddress() {
			return bccEmailAddress;
		}

		public void setBccEmailAddress(List<BccEmailAddress> bccEmailAddress) {
			this.bccEmailAddress = bccEmailAddress;
		}

		

		public String getToName() {
		return toName;
		}

		public void setToName(String toName) {
		this.toName = toName;
		}

		public String getSubject() {
		return subject;
		}

		public void setSubject(String subject) {
		this.subject = subject;
		}

		public String getBody() {
		return body;
		}

		public void setBody(String body) {
		this.body = body;
		}

		public Object getAttachmentIds() {
		return attachmentIds;
		}

		public void setAttachmentIds(Object attachmentIds) {
		this.attachmentIds = attachmentIds;
		}

		public class ToEmailAddress {

		private String name;
		private String email;

		public String getName() {
		return name;
		}

		public void setName(String name) {
		this.name = name;
		}

		public String getEmail() {
		return email;
		}

		public void setEmail(String email) {
		this.email = email;
		}

		}
		
		
		public class BccEmailAddress {

			private String name;
			private String email;

			public String getName() {
			return name;
			}

			public void setName(String name) {
			this.name = name;
			}

			public String getEmail() {
			return email;
			}

			public void setEmail(String email) {
			this.email = email;
			}

			}


			public class CcEmailAddress {

			private String name;
			private String email;

			public String getName() {
			return name;
			}

			public void setName(String name) {
			this.name = name;
			}

			public String getEmail() {
			return email;
			}

			public void setEmail(String email) {
			this.email = email;
			}

			}


}






