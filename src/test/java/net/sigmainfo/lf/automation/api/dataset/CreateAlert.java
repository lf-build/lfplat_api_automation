package net.sigmainfo.lf.automation.api.dataset;
import java.util.List;

import org.json.JSONObject;

public class CreateAlert {


	private String entityType;
	private String entityId;
	private String name;
	private List<String> tags = null;
	private Timestamp timestamp;
	private String description;
	private String status;
	private String eventName;
	private JSONObject eventData;
	private List<String> categories = null;
	private String id;

	public String getEntityType() {
	return entityType;
	}

	public void setEntityType(String entityType) {
	this.entityType = entityType;
	}

	public String getEntityId() {
	return entityId;
	}

	public void setEntityId(String entityId) {
	this.entityId = entityId;
	}

	public String getName() {
	return name;
	}

	public void setName(String name) {
	this.name = name;
	}

	public List<String> getTags() {
	return tags;
	}

	public void setTags(List<String> tags) {
	this.tags = tags;
	}

	public Timestamp getTimestamp() {
	return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
	this.timestamp = timestamp;
	}

	public String getDescription() {
	return description;
	}

	public void setDescription(String description) {
	this.description = description;
	}

	public String getStatus() {
	return status;
	}

	public void setStatus(String status) {
	this.status = status;
	}

	public String getEventName() {
	return eventName;
	}

	public void setEventName(String eventName) {
	this.eventName = eventName;
	}

	public JSONObject getEventData() {
	return eventData;
	}

	public void setEventData(JSONObject jsonObject) {
	this.eventData = jsonObject;
	}

	public List<String> getCategories() {
	return categories;
	}

	public void setCategories(List<String> categories) {
	this.categories = categories;
	}

	public String getId() {
	return id;
	}

	public void setId(String id) {
	this.id = id;
	}
	

	public class EventData {

	}



	public class Timestamp {

	private String time;

	public String getTime() {
	return time;
	}

	public void setTime(String time) {
	this.time = time;
	}

	}
	


}
