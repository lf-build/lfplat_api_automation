package net.sigmainfo.lf.automation.api.dataset;

public class SendSMS {
	private String recipientPhone;
	private String message;

	public String getRecipientPhone() {
	return recipientPhone;
	}

	public void setRecipientPhone(String recipientPhone) {
	this.recipientPhone = recipientPhone;
	}

	public String getMessage() {
	return message;
	}

	public void setMessage(String message) {
	this.message = message;
	}

	
}
