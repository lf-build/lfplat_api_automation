package net.sigmainfo.lf.automation.api.dataset;

public class PauseWorkflow {
	private Boolean terminated;
	private Output output;

	public Boolean getTerminated() {
	return terminated;
	}

	public void setTerminated(Boolean terminated) {
	this.terminated = terminated;
	}

	public Output getOutput() {
	return output;
	}

	public void setOutput(Output output) {
	this.output = output;
	}

	public class Output {


	}
}
