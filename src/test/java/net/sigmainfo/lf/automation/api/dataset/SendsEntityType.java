package net.sigmainfo.lf.automation.api.dataset;

public class SendsEntityType {
	private String entityType;
	private String entityId;
	private String templateName;
	private String templateVersion;
	private String email;

	public String getEntityType() {
	return entityType;
	}

	public void setEntityType(String entityType) {
	this.entityType = entityType;
	}

	public String getEntityId() {
	return entityId;
	}

	public void setEntityId(String entityId) {
	this.entityId = entityId;
	}

	public String getTemplateName() {
	return templateName;
	}

	public void setTemplateName(String templateName) {
	this.templateName = templateName;
	}

	public String getTemplateVersion() {
	return templateVersion;
	}

	public void setTemplateVersion(String templateVersion) {
	this.templateVersion = templateVersion;
	}

	public String getEmail() {
	return email;
	}

	public void setEmail(String email) {
	this.email = email;
	}
}
