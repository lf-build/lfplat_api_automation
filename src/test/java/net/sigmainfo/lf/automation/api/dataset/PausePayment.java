package net.sigmainfo.lf.automation.api.dataset;

public class PausePayment {
	private String startDate;
	private String endDate;
	private String notes;

	public String getStartDate() {
	return startDate;
	}

	public void setStartDate(String startDate) {
	this.startDate = startDate;
	}

	public String getEndDate() {
	return endDate;
	}

	public void setEndDate(String endDate) {
	this.endDate = endDate;
	}

	public String getNotes() {
	return notes;
	}

	public void setNotes(String notes) {
	this.notes = notes;
	}
}
