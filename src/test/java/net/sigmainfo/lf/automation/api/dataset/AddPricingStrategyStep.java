package net.sigmainfo.lf.automation.api.dataset;

import java.util.List;

public class AddPricingStrategyStep {
	private String stepName;
	private String parameterName;
	private List<String> parameterTypes = null;
	private String description;
	private String stepType;
	private Integer sequenceNumber;
	private Boolean continueOnException;
	private Boolean continueOnNonEligibility;

	public String getStepName() {
	return stepName;
	}

	public void setStepName(String stepName) {
	this.stepName = stepName;
	}

	public String getParameterName() {
	return parameterName;
	}

	public void setParameterName(String parameterName) {
	this.parameterName = parameterName;
	}

	public List<String> getParameterTypes() {
	return parameterTypes;
	}

	public void setParameterTypes(List<String> parameterTypes) {
	this.parameterTypes = parameterTypes;
	}

	public String getDescription() {
	return description;
	}

	public void setDescription(String description) {
	this.description = description;
	}

	public String getStepType() {
	return stepType;
	}

	public void setStepType(String stepType) {
	this.stepType = stepType;
	}

	public Integer getSequenceNumber() {
	return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
	this.sequenceNumber = sequenceNumber;
	}

	public Boolean getContinueOnException() {
	return continueOnException;
	}

	public void setContinueOnException(Boolean continueOnException) {
	this.continueOnException = continueOnException;
	}

	public Boolean getContinueOnNonEligibility() {
	return continueOnNonEligibility;
	}

	public void setContinueOnNonEligibility(Boolean continueOnNonEligibility) {
	this.continueOnNonEligibility = continueOnNonEligibility;
	}

}
