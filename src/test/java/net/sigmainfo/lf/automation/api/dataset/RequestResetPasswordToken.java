package net.sigmainfo.lf.automation.api.dataset;

public class RequestResetPasswordToken {
	private String username;
	private String portal;

	public String getPortal() {
		return portal;
	}

	public void setPortal(String portal) {
		this.portal = portal;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
