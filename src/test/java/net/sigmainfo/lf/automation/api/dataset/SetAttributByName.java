package net.sigmainfo.lf.automation.api.dataset;

import java.util.List;

public class SetAttributByName {
	
	private List<String> names = null;
	private String secondaryName;

	public List<String> getNames() {
	return names;
	}

	public void setNames(List<String> names) {
	this.names = names;
	}

	public String getSecondaryName() {
	return secondaryName;
	}

	public void setSecondaryName(String secondaryName) {
	this.secondaryName = secondaryName;
	}

}
