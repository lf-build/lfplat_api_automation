package net.sigmainfo.lf.automation.api.dataset;

public class GenerateByTemplate {

		private String name;
		private String format;
		private String version;
		private Data data;

		public String getName() {
		return name;
		}

		public void setName(String name) {
		this.name = name;
		}

		public String getFormat() {
		return format;
		}

		public void setFormat(String format) {
		this.format = format;
		}

		public String getVersion() {
		return version;
		}

		public void setVersion(String version) {
		this.version = version;
		}

		public Data getData() {
		return data;
		}

		public void setData(Data data) {
		this.data = data;
		}

public class Data {

	private String reasons;

	public String getReasons() {
	return reasons;
	}

	public void setReasons(String reasons) {
	this.reasons = reasons;
	}

	}


}
