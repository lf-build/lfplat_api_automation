package net.sigmainfo.lf.automation.api.function;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.Gson;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.internal.mapping.Jackson2Mapper;
import com.jayway.restassured.mapper.ObjectMapper;
import com.jayway.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.dataset.*;
import net.sigmainfo.lf.automation.api.dataset.AddScoreCardVariable.ScoreCardVariableSource;
import net.sigmainfo.lf.automation.api.dataset.AddSegment.SegmentVariableSource;
import net.sigmainfo.lf.automation.api.dataset.AddTemplate.PdfSettings;
import net.sigmainfo.lf.automation.api.dataset.CreateAlert.EventData;
import net.sigmainfo.lf.automation.api.dataset.CreateAlert.Timestamp;
import net.sigmainfo.lf.automation.api.dataset.Email.BccEmailAddress;
import net.sigmainfo.lf.automation.api.dataset.Email.CcEmailAddress;
import net.sigmainfo.lf.automation.api.dataset.Email.ToEmailAddress;
import net.sigmainfo.lf.automation.api.dataset.GenerateByFormat.Properties;
import net.sigmainfo.lf.automation.api.dataset.GenerateByTemplate.Data;
import net.sigmainfo.lf.automation.api.dataset.PauseWorkflow.Output;
import net.sigmainfo.lf.automation.api.dataset.QueueInstruction.Receiving;
import net.sigmainfo.lf.automation.common.AbstractTests;
import org.apache.commons.lang.RandomStringUtils;
import org.json.JSONException;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


@Component
public class ApiFuncUtils extends AbstractTests {

	private static Logger logger = LoggerFactory.getLogger(ApiFuncUtils.class);

	/**
	 * THIS CLASS DEFINES ALL REUSABLE UTILITIES I.E. REUSABLE FUNCTIONS
	 */

	/**
	 * BELOW LINE IS VERY MUCH SIMILAR TO FOLLOWING STATEMENT ApiParam apiParam
	 * = new ApiParam();
	 *
	 * =======================================
	 *
	 * @Autowired ApiParam apiParam;
	 */

	ApiParam apiParam = new ApiParam();
	UserLogin objUsrLogin = new UserLogin();
	CreateUser objCreateUser = new CreateUser();
	AbstractTests objAbstractTests = new AbstractTests();
	RequestResetPasswordToken objRequestResetPasswordToken = new RequestResetPasswordToken();
	ResetPassword objResetPassword = new ResetPassword();
	ChangePassword objChangePassword = new ChangePassword();
	DuplicateConsumerByField objByField = new DuplicateConsumerByField();
	DuplicateConsumerByFilter objByFilter = new DuplicateConsumerByFilter();
	IntiateVerification objIntiateVerification = new IntiateVerification();
	AddActionEntity objAdd = new AddActionEntity();
	GetsInstruction objInst = new GetsInstruction();
	RejectInstruction objReject = new RejectInstruction();
	PausePayment objPause = new PausePayment();
	QueueInstruction objQueue = new QueueInstruction();
	Receiving objreceive = new QueueInstruction().new Receiving();
	SendTemplateName objTemplateName = new SendTemplateName();
	Email objSendMail = new Email();
	CcEmailAddress objCC = new Email().new CcEmailAddress();
	BccEmailAddress objBCC =new Email().new BccEmailAddress();
	ToEmailAddress objTO = new Email().new ToEmailAddress();
	SendsEntityType objEntityType = new SendsEntityType();
	ResetFacts objResetFacts= new ResetFacts();
	IntiateDynamicFact objInitiateDynamic = new IntiateDynamicFact();
	IntiateManualVerification objIntiateManual = new  IntiateManualVerification();
	VerifiesFacts objVerify =new VerifiesFacts();
	CreateAlert objCreatAlert = new CreateAlert();
	Timestamp objTime = new CreateAlert().new Timestamp();
	EventData objEvent = new CreateAlert().new EventData();
	PauseWorkflow objPauseFlow = new PauseWorkflow();
	Output objOutput =  new PauseWorkflow().new Output();
	SendSMS objSendSMS = new  SendSMS();
	SendSmsEntityType objSendEntityType = new SendSmsEntityType();
	AssignByEntity objAssign = new AssignByEntity();
	EscalateByEntity objEscalate = new EscalateByEntity();
	CompleteAssignment objComplete =new CompleteAssignment();
	GenerateByFormat objGenFormat = new GenerateByFormat();
	Properties objProperty = new GenerateByFormat().new Properties();
	GenerateByTemplate objGenTemplate = new GenerateByTemplate();
	Data objTemplateData = new GenerateByTemplate().new Data();
	SetSchema objSchema = new SetSchema();
	SetAttribute objAttribute = new SetAttribute();
	SetAttributByName objName = new SetAttributByName();
	ProcessTemplate objProcessTemplate = new ProcessTemplate();
	AddTemplate objAddTemplate= new AddTemplate();
	net.sigmainfo.lf.automation.api.dataset.AddTemplate.Properties objProperties =new AddTemplate().new Properties();
	PdfSettings objPdfSettings = new AddTemplate().new PdfSettings();
	ValidateToken objValidateToken= new ValidateToken();
	IssueToken objIssueToken = new IssueToken();
	ChangeStatus objChangeStatus = new ChangeStatus();
	AddPricingStrategyStep objPricingStrategyStep = new AddPricingStrategyStep();
	AddScoreCard objScoreCard = new AddScoreCard();
	AddScoreCardVariable objScoreCardVariable = new AddScoreCardVariable();
	ScoreCardVariableSource objScoreCardVariableSource = new AddScoreCardVariable().new ScoreCardVariableSource();
	AddPricingStrategy objPricingStrategy = new AddPricingStrategy();
	AddRuleDefination objRuleDefination = new AddRuleDefination();
	AddLookUpData objLookUpData =new AddLookUpData();
	AddSegment objSegment =new AddSegment();
	SegmentVariableSource objSegmentVariableSource = new AddSegment().new SegmentVariableSource();
	AddTemplateData objTemplate =new AddTemplateData();
	UpdateLookUpData objUpdateLookUp =new UpdateLookUpData();


	public Response triggerPostRequest(String serviceUrl, Boolean isCorrectAuthorization, String authToken,
			String Body) {

		logger.info("=======================================================================================");
		logger.info("Triggering a Post request to :" + serviceUrl);
		logger.info("=======================================================================================");
		Header header;
		if (!isCorrectAuthorization) {
			header = new Header("Authorization", "Bearer 12" + authToken + "12");
		} else {
			header = new Header("Authorization", "Bearer " + authToken);
		}

		logger.info("============== Auth Token : " + authToken + "=============================");
		logger.info("============== Request payload : " + Body + "=============================");
		Response response = given().header(header).contentType(apiParam.contentType).body(Body).when().post(serviceUrl);

		logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
		return response;
	}
	
	
	public Response triggerDeleteRequest(String serviceUrl, Boolean isCorrectAuthorization, String authToken,
			String Body) {

		logger.info("=======================================================================================");
		logger.info("Triggering a Delete request to :" + serviceUrl);
		logger.info("=======================================================================================");
		Header header;
		if (!isCorrectAuthorization) {
			header = new Header("Authorization", "Bearer 12" + authToken + "12");
		} else {
			header = new Header("Authorization", "Bearer " + authToken);
		}

		Response response = given().header(header).contentType(apiParam.contentType).when().delete(serviceUrl);

		logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
		return response;
	}

	public Response triggerPutRequest(String serviceUrl, Boolean isCorrectAuthorization, String authToken,
			String Body) {

		logger.info("=======================================================================================");
		logger.info("Triggering a PUT request to :" + serviceUrl);
		logger.info("=======================================================================================");
		Header header;
		if (!isCorrectAuthorization) {
			header = new Header("Authorization", "Bearer 12" + authToken + "12");
		} else {
			header = new Header("Authorization", "Bearer " + authToken);
		}

		Response response = given().header(header).contentType(apiParam.contentType).body(Body).when().put(serviceUrl);

		logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
		return response;
	}

	private void setContentType(RequestSpecBuilder builder, ObjectMapper objectMapper, String contentType) {

		if (contentType.contains("application/json")) {
			objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

				public com.fasterxml.jackson.databind.ObjectMapper create(Class arg0, String arg1) {
					com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
					objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
					// objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
					return (objectMapper);
				}

			});
		}
		builder.setContentType(contentType);
	}

	private RequestSpecification constructRequestPayload(String requestType, String contentType, String serviceUrl) {

		RequestSpecBuilder builder = new RequestSpecBuilder();
		ObjectMapper objectMapper = null;

		if (contentType.contains("application/json")) {
			objectMapper = new Jackson2Mapper(new Jackson2ObjectMapperFactory() {

				public com.fasterxml.jackson.databind.ObjectMapper create(Class arg0, String arg1) {
					com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
					objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
					return objectMapper;
				}

			});

		}
		builder.setContentType(contentType);

		if (serviceUrl.equalsIgnoreCase("http://foundation.dev.lendfoundry.com:7066/")) {
			objIssueToken.setIssuer("me");
			objIssueToken.setIssuedAt("2018-07-26T09:04:44.3497546Z");
			objIssueToken.setExpiration("2018-07-26T09:04:44.3497546Z");
			objIssueToken.setSubject("can be anything");
			objIssueToken.setTenant("My-tenant");
			objIssueToken.setExpirationDays(1);
			objIssueToken.setEntityType("application");
			objIssueToken.setEntityId("0000932");
			objIssueToken.setValue("12");
			setBody(builder, objectMapper, objIssueToken, apiParam.contentType);
		} else if (requestType.equalsIgnoreCase("/validate")) {
			objValidateToken.setEntityType(apiParam.tokenEntityType);
			objValidateToken.setEntityId(apiParam.tokenEntityId);
			objValidateToken.setToken(apiParam.token);
			setBody(builder, objectMapper, objValidateToken, apiParam.contentType);
		} else if (serviceUrl.equalsIgnoreCase("http://foundation.dev.lendfoundry.com:7014/")) {
			objAddTemplate.setName(apiParam.tempName);
			objAddTemplate.setTitle("Document Upload Email - {{applicationNumber}}");
			objAddTemplate.setFormat(apiParam.tempFormat);
			objAddTemplate.setBody("<!doctype html><html><head><meta charset='utf-8'><title>Capital Alliance</title></head><body>File #{{applicationNumber}} has uploaded a document,Document Category {{documentCategory}}. {{tenantConfiguration.website}}</body></html>");
			objAddTemplate.setVersion(apiParam.tempVersion);
			objAddTemplate.setId("5a1ff814b2badc00075781e9");
			objPdfSettings.setPageSize("A4");
			objPdfSettings.setPageWidth("21cm");
			objPdfSettings.setPageHeight("29.7cm");
			objPdfSettings.setOrientation("portrait");
			objPdfSettings.setDpi(300);
			objPdfSettings.setImageQuality(90);
			objPdfSettings.setMarginTop("10");
			objPdfSettings.setMarginBottom("10");
			objPdfSettings.setMarginLeft("10");
			objPdfSettings.setMarginRight("10");
			objProperties.setPdfSettings(objPdfSettings);
			objAddTemplate.setProperties(objProperties);
			setBody(builder, objectMapper, objAddTemplate, apiParam.contentType);
		}else if (requestType.equalsIgnoreCase("/")) {
			objCreateUser.setName(apiParam.username);
			objCreateUser.setEmail(apiParam.email);
			objCreateUser.setUsername(apiParam.username);
			objCreateUser.setPassword(apiParam.password);
			objCreateUser.setRoles(apiParam.roles);
			objCreateUser.setActive(true);
			objCreateUser.setPasswordExpirationDate("2018-07-31T06:35:10.647Z");
			setBody(builder, objectMapper, objCreateUser, apiParam.contentType);
		} else if (requestType.equalsIgnoreCase("/login")) {
			objUsrLogin.setusername(apiParam.username);
			objUsrLogin.setPassword(apiParam.password);
			objUsrLogin.setPortal(apiParam.portal);
			setBody(builder, objectMapper, objUsrLogin, apiParam.contentType);
		} else if (requestType.equalsIgnoreCase("/request-token")) {
			objRequestResetPasswordToken.setPortal(apiParam.portal);
			objRequestResetPasswordToken.setUsername(apiParam.username);
			setBody(builder, objectMapper, objRequestResetPasswordToken, apiParam.contentType);
		} else if (requestType.equalsIgnoreCase("/reset-password/" + apiParam.resetToken)) {
			objResetPassword.setusername(apiParam.username);
			objResetPassword.setPassword(apiParam.password);
			objResetPassword.setPortal(apiParam.portal);
			setBody(builder, objectMapper, objResetPassword, apiParam.contentType);
		} else if (requestType.equalsIgnoreCase("/change-password")) {
			objChangePassword.setCurrentPassword("Sigma@123");
			objChangePassword.setPassword("Sigma@1234");
			objChangePassword.setConfirmPassword("Sigma@1234");
			setBody(builder, objectMapper, objChangePassword, apiParam.contentType);
		} else if (requestType.equalsIgnoreCase("/consumer/duplicate/null")) {
			objByField.setConsumerentityId(apiParam.consumerEntityId);
			objByField.setEntityType(apiParam.entityType);
			setBody(builder, objectMapper, objByField, apiParam.contentType);
		} else if (requestType.equalsIgnoreCase("/consumer/duplicate")) {
			String json = "{\"additionalProp1\":{},"
		            + "\"additionalProp2\":{} " + "\"additionalProp3\":{}}";
			objByFilter.setAdditionalProp1(null);
			objByFilter.setAdditionalProp2(null);
			objByFilter.setAdditionalProp3(null);
			setBody(builder, objectMapper, objByFilter, apiParam.contentType);
		}else if(requestType.equalsIgnoreCase("/reference-entity/by_consumerid")) {
			List<String> ConsumerId = new ArrayList<String>();
			ConsumerId.add(apiParam.consumersID);
			setBody(builder, objectMapper, ConsumerId,  apiParam.contentType);
		} else if (requestType.equalsIgnoreCase("/application/0000120")) {
			objIntiateVerification.setName("pooja");
			objIntiateVerification.setMobile("+919752867887");
			objIntiateVerification.setPhone("+919752867887");
			setBody(builder, objectMapper, objIntiateVerification, apiParam.contentType);
		}else if (requestType.equalsIgnoreCase("/"  + apiParam.getTemplateName() + "/" +apiParam.getVersion()  + "/" + apiParam.getFormat() + "/generate")) {
			objGenTemplate.setName("DocumentUploadEmail");
			objGenTemplate.setFormat("pdf");
			objGenTemplate.setVersion("1.0");
			objTemplateData.setReasons("Test");
			objGenTemplate.setData(objTemplateData);
			setBody(builder, objectMapper, objGenTemplate, apiParam.contentType);
			
		} else if (requestType.equalsIgnoreCase("/Pdf/generate")) {
			objGenFormat.setUrl("https://www.le.ac.uk/oerresources/bdra/html/page_09.htm");
			objProperty.setPageSize("A4");
			objProperty.setPageWidth("21cm");
			objProperty.setPageHeight("29.7cm");
			objProperty.setOrientation("portrait");
			objProperty.setDpi(300);
			objProperty.setImageQuality(90);
			objProperty.setMarginTop("10");
			objProperty.setMarginBottom("10");
			objProperty.setMarginLeft("10");
			objProperty.setMarginRight("10");
			objGenFormat.setProperties(objProperty);
			setBody(builder, objectMapper, objGenFormat, apiParam.contentType);
		
		} else if (requestType.equalsIgnoreCase("/Html/generate")) {
			objGenFormat.setUrl("https://www.le.ac.uk/oerresources/bdra/html/page_09.htm");
			objProperty.setPageSize("A4");
			objProperty.setPageWidth("21cm");
			objProperty.setPageHeight("29.7cm");
			objProperty.setOrientation("portrait");
			objProperty.setDpi(300);
			objProperty.setImageQuality(90);
			objProperty.setMarginTop("10");
			objProperty.setMarginBottom("10");
			objProperty.setMarginLeft("10");
			objProperty.setMarginRight("10");
			objGenFormat.setProperties(objProperty);
			setBody(builder, objectMapper, objGenFormat, apiParam.contentType);
			
		}else if (requestType.equalsIgnoreCase("/" + apiParam.actionEntityType + "/" + apiParam.actionEntityID + "/" + apiParam.requestId)) {
				objAdd.setPriority("1");
				objAdd.setDueDate("2018-05-14T10:40:07.927Z");
				objAdd.setEntityId(apiParam.actionEntityID);
				objAdd.setNotes("anything");
				setBody(builder, objectMapper, objAdd, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.actionEntityType  + "/" + apiParam.requestId)) {
				objAdd.setPriority("1");
				objAdd.setDueDate("2018-05-14T10:40:07.927Z");
				objAdd.setEntityId(apiParam.actionEntityID);
				objAdd.setNotes("anything");
				setBody(builder, objectMapper, objAdd, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/queue")) {
				String time = java.time.LocalDateTime.now().toString();
				objQueue.setExecutionDate(time);
				objQueue.setAmount("1000.00");
				objQueue.setEntryDescription("IDFLoan");
				objQueue.setFrequency("Single");
				objQueue.setType("creditToChecking");
				objQueue.setReferenceNumber("0000554");
				objQueue.setStandardEntryClassCode("WEB");
				objQueue.setFundingSourceId("1004");
				objreceive.setAccountType("individual");
				objreceive.setAccountNumber("123");
				objreceive.setRoutingNumber("072000098");
				objreceive.setName("Test");
				objQueue.setReceiving(objreceive);
				
				setBody(builder, objectMapper, objQueue, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/instruction/internalReferenceNumber")) {
				List<Integer> internalReferenceNumbers = new ArrayList();
				internalReferenceNumbers.add(apiParam.internalReferenceNumber);
				objInst.setInternalReferenceNumbers(internalReferenceNumbers);
				setBody(builder, objectMapper, objInst, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/instruction/by/internal-reference-number/" + apiParam.internalReferenceNumber +"/reject")) {
				objReject.setReason(apiParam.reason);
				setBody(builder, objectMapper, objReject, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/ach/" + apiParam.achreferenceNumber+ "/pause")) {
				objPause.setStartDate(apiParam.processStartDateTime);
				objPause.setEndDate(apiParam.processEndDateTime);
				objPause.setNotes("notes");
				setBody(builder, objectMapper, objPause, apiParam.contentType);
			}  else if (requestType.equalsIgnoreCase("/"+ apiParam.emailTemplateName +"/" + apiParam.emailTemplateVersion)) {
				objTemplateName.setEntityId(apiParam.emailEntityId);
				objTemplateName.setEntityType(apiParam.emailEntityType);
				objTemplateName.setEmail(apiParam.mailId);
				setBody(builder, objectMapper, objTemplateName, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.emailEntityType + "/" + apiParam.emailEntityId + "/mail")) {
				List<ToEmailAddress> toEmailAddress = new ArrayList();
				objTO.setEmail(apiParam.mailId);
				objTO.setName("KAREN BOHEN");
				toEmailAddress.add(objTO);
				objSendMail.setToEmailAddress(toEmailAddress);
				
				
				List<CcEmailAddress> ccEmailAddress = new ArrayList();
				objCC.setEmail("string2@yopmail.com");
				objCC.setName("string2");
				ccEmailAddress.add(objCC);
				objSendMail.setCcEmailAddress(ccEmailAddress);
				
				List<BccEmailAddress> bccEmailAddress = new ArrayList();
				objBCC.setEmail("string1@yopmail.com");
				objBCC.setName("string1");
				bccEmailAddress.add(objBCC);
				objSendMail.setBccEmailAddress(bccEmailAddress);
				objSendMail.setToName("KAREN BOHEN");
				objSendMail.setSubject(apiParam.subject);
				objSendMail.setAttachmentIds(null);
				objSendMail.setBody(apiParam.body);
				setBody(builder, objectMapper, objSendMail, apiParam.contentType);
		
			} else if(requestType.equalsIgnoreCase("/"+ apiParam.emailEntityType +"/" +apiParam.emailEntityId +"/" + apiParam.emailTemplateName +"/" +  apiParam.emailTemplateVersion)) {
				objEntityType.setEntityType(apiParam.emailEntityType);
				objEntityType.setEntityId(apiParam.emailEntityId);
				objEntityType.setTemplateName(apiParam.emailTemplateName);
				objEntityType.setTemplateVersion("1.0");
				objEntityType.setEmail(apiParam.mailId);
				setBody(builder, objectMapper, objEntityType, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase(("/" + apiParam.verificationEntityType + "/" + apiParam.verificationEntityId + "/resetfacts"))) {
				List<String> factNames = new ArrayList();
				factNames.add("BankVerification");
				objResetFacts.setFactNames(factNames);
				setBody(builder, objectMapper, objResetFacts, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.verificationEntityType + "/" + apiParam.verificationEntityId +"/" +apiParam.workFlowStatusId + "/initiate-dynamicfact-verification")) {
				JSONObject jo = new JSONObject();

		        jo.put("factName", "string");
		        JSONArray ja = new JSONArray();
		         
		        Map m = new LinkedHashMap(2);
		        m.put("category","string");
		        m.put("name","string");
		        ja.add(m);
		        jo.put("documentDetails", ja);
				setBody(builder, objectMapper, jo, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.verificationEntityType + "/" + apiParam.verificationEntityId +"/" +apiParam.workFlowStatusId +"/"+ apiParam.fact + "/initiate-manualverification")) {
				
				setBody(builder, objectMapper, objIntiateManual, apiParam.contentType);
			} else if((requestType.equalsIgnoreCase("/"+ apiParam.verificationEntityType + "/" + apiParam.verificationEntityId + "/" + apiParam.workFlowStatusId + "/" + apiParam.fact + "/" + apiParam.method+ "/" + apiParam.verificationSource))) {
				setBody(builder, objectMapper, objVerify, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase("/create")) {
				objCreatAlert.setEntityType("application");
				objCreatAlert.setEntityId("ABC12344");
				objCreatAlert.setName("Sample Name");
				List<String> tags = new ArrayList();
				tags.add("Own");
				objCreatAlert.setTags(tags);
				objTime.setTime("2018-04-16T07:00:19.508Z");
				objCreatAlert.setTimestamp(objTime);
				objCreatAlert.setDescription("string");
				objCreatAlert.setStatus("Actve");
				objCreatAlert.setEventName("Sample Event");
				//objCreatAlert.setEventData(new JSONObject());
				List<String> categories = new ArrayList();
				categories.add("string");
				objCreatAlert.setCategories(categories);
				objCreatAlert.setId("string");			
				setBody(builder, objectMapper, objCreatAlert, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" +apiParam.workflow +"/" + apiParam.appFlowID + "/pause")) {
				JSONObject json =new JSONObject();
				json.put("terminated",true);
				json.put("output",new JSONObject());
				setBody(builder, objectMapper, json, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.workflow +"/" + apiParam.appFlowID)) {
				JSONObject json =new JSONObject();
				json.put("terminated",false);
				json.put("output",new JSONObject());
				setBody(builder, objectMapper, json, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.SMSentitytype + "/" + apiParam.SMSentityid)) {
				objSendSMS.setRecipientPhone("+919752867887");
				objSendSMS.setMessage("hello");
				setBody(builder, objectMapper, objSendSMS, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.SMSentitytype + "/" + apiParam.SMSentityid +"/" + apiParam.SMStemplateName +"/" + apiParam.SMStemplateVersion)) {
				objSendEntityType.setRecipientPhone("+919752867887");
				objSendEntityType.setCommonSMSText("hii");
				setBody(builder, objectMapper, objSendEntityType, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" +apiParam.assignEntityType + "/" + apiParam.assignEntityId  +"/assign")) {
				objAssign.setUser(apiParam.assignUsername2);
				objAssign.setRole(apiParam.assignRole);
				setBody(builder, objectMapper, objAssign, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" +apiParam.assignEntityType + "/" + apiParam.assignEntityId  + "/escalate")) {
				objEscalate.setFromRole(apiParam.assignFromRole);
				objEscalate.setToRole(apiParam.assignToRole);
				objEscalate.setNotes("String");
				setBody(builder, objectMapper, objEscalate, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/login")) {
				objUsrLogin.setusername(apiParam.assignUsername);
				objUsrLogin.setPassword(apiParam.assignPassword);
				objUsrLogin.setPortal(apiParam.assignPortal);
				setBody(builder, objectMapper, objUsrLogin, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" +apiParam.assignEntityType + "/" + apiParam.assignEntityId  +"/escalations/"  + apiParam.assignEsclationId  + "/complete")) {
				objComplete.setNotes("string");
				setBody(builder, objectMapper, objComplete, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/"+ apiParam.dataEntityType + "/" + apiParam.dataEntityId + "/" + apiParam.dataName + "/" + apiParam.dataSecondaryName)) {
				List<String> names = new ArrayList();
				names.add(apiParam.dataName);
				objName.setNames(names);
				objName.setSecondaryName(apiParam.dataSecondaryName);
				setBody(builder, objectMapper, objName, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.dataEntityType + "/" + apiParam.dataName + "/schema")) {
				objSchema.setIsSecondryEnable(true);
				setBody(builder, objectMapper, objSchema, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.dataEntityType + "/" + apiParam.dataEntityId)) {
				JSONObject json =new JSONObject();
				json.put("additionalProp1",new JSONObject());
				json.put("additionalProp2",new JSONObject());
				json.put("additionalProp3",new JSONObject());
				System.out.println("============================" + json);
				setBody(builder, objectMapper, json, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.dataEntityType + "/" + apiParam.dataEntityId + "/names")) {
				List<String> names = new ArrayList();
				names.add(apiParam.dataName);
				objName.setNames(names);
				objName.setSecondaryName(apiParam.dataSecondaryName);
				setBody(builder, objectMapper, objName, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.dataEntityType + "/" + apiParam.dataEntityId + "/" + apiParam.dataName)) {
				objSchema.setIsSecondryEnable(true);
				setBody(builder, objectMapper, objSchema, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("")) {
				JSONObject updateRule =new JSONObject();
				updateRule.put("source","function alert_dummy_rule(payload) { return 'true';}");
				setBody(builder, objectMapper, updateRule, apiParam.contentType);
				
			} else if (requestType.equalsIgnoreCase("/"+ apiParam.ruleName)) {
				JSONObject addRule =new JSONObject();
				addRule.put("source","function alert_dummy_rule(payload) { return 'true';}");
				setBody(builder, objectMapper, addRule, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.tempName +"/" + apiParam.tempVersion +"/" + apiParam.tempFormat)) {
				objAddTemplate.setName(apiParam.tempName);
				objAddTemplate.setTitle("Document Upload Email - {{applicationNumber}}");
				objAddTemplate.setFormat(apiParam.tempFormat);
				objAddTemplate.setBody("<!doctype html><html><head><meta charset='utf-8'><title>Capital Alliance</title></head><body>File #{{applicationNumber}} has uploaded a document,Document Category {{documentCategory}}. {{tenantConfiguration.website}}</body></html>");
				objAddTemplate.setVersion(apiParam.tempVersion);
				objAddTemplate.setId("5a1ff814b2badc00075781e9");
				objPdfSettings.setPageSize("A4");
				objPdfSettings.setPageWidth("21cm");
				objPdfSettings.setPageHeight("29.7cm");
				objPdfSettings.setOrientation("portrait");
				objPdfSettings.setDpi(300);
				objPdfSettings.setImageQuality(90);
				objPdfSettings.setMarginTop("10");
				objPdfSettings.setMarginBottom("10");
				objPdfSettings.setMarginLeft("10");
				objPdfSettings.setMarginRight("10");
				objProperties.setPdfSettings(objPdfSettings);
				objAddTemplate.setProperties(objProperties);
				setBody(builder, objectMapper, objAddTemplate, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.tempName +"/" + apiParam.tempVersion +"/" + apiParam.tempFormat + "/process")) {
				objProcessTemplate.setApplicationNumber("CA0000001");
				objProcessTemplate.setDocumentCategory("BankStatement");
				setBody(builder, objectMapper, objProcessTemplate, apiParam.contentType);
			} else 		if (requestType.equalsIgnoreCase("/" + apiParam.statusManagementEntitytype +"/" + apiParam.statusManagementEntityid +"/" +apiParam.statusManagementStatusWorkFlowId + "/" +apiParam.statusManagementNewStatus)) {
				objChangeStatus.setNote("string");
				objChangeStatus.setReasons(new ArrayList());
				setBody(builder, objectMapper, objChangeStatus, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.statusManagementEntitytype + "/" +apiParam.statusManagementEntityid +"/" +apiParam.statusManagementStatusWorkFlowId +"/" + apiParam.statusManagementSubWorkFlowId + "/" +apiParam.statusManagementSubWorkflowStatus)) {
				objChangeStatus.setNote("string");
				objChangeStatus.setReasons(new ArrayList());
				setBody(builder, objectMapper, objChangeStatus, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/ruledefinition")) {
				objRuleDefination.setRuleName(objAbstractTests.GenerateName());
				objRuleDefination.setDataAttributeStoreName("DrawDownEligibility_Attribute");
				objRuleDefination.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objRuleDefination.setExpiryDate("2018-08-23T07:10:43.423Z");
				objRuleDefination.setRuleClass("eligibility");
				objRuleDefination.setRuleType("Rule");
				objRuleDefination.setDeRuleName("DrawDownEligibilityCheck");
				objRuleDefination.setRuleVersion("1.0");
				List<String> requiredSources = new ArrayList();
				requiredSources.add("DrawDown");
				objRuleDefination.setRequiredSources(requiredSources);
				List<String> optionalSources = new ArrayList();
				optionalSources.add("string");
				objRuleDefination.setOptionalSources(optionalSources);
				List<String> groupNames = new ArrayList();
				groupNames.add("string");
				objRuleDefination.setGroupNames(groupNames);
				
				objRuleDefination.setCreatedBy("string");
				objRuleDefination.setDescription("DrawDownEligibilityRule");
				objRuleDefination.setCustomDate("2018-08-23T07:10:43.423Z");
				objRuleDefination.setApplicationType("systemDate");
				setBody(builder, objectMapper, objRuleDefination, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleRuleId + "/ruledefinition")) {
				objRuleDefination.setRuleName(objAbstractTests.GenerateName());
				objRuleDefination.setDataAttributeStoreName("DrawDownEligibility_Attribute");
				objRuleDefination.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objRuleDefination.setExpiryDate("2018-08-23T07:10:43.423Z");
				objRuleDefination.setRuleClass("eligibility");
				objRuleDefination.setRuleType("Rule");
				objRuleDefination.setDeRuleName("DrawDownEligibilityCheck");
				objRuleDefination.setRuleVersion("1.0");
				List<String> requiredSources = new ArrayList();
				requiredSources.add("DrawDown");
				objRuleDefination.setRequiredSources(requiredSources);
				List<String> optionalSources = new ArrayList();
				optionalSources.add("string");
				objRuleDefination.setOptionalSources(optionalSources);
				List<String> groupNames = new ArrayList();
				groupNames.add("string");
				objRuleDefination.setGroupNames(groupNames);
				
				objRuleDefination.setCreatedBy("string");
				objRuleDefination.setDescription("DrawDownEligibilityRule");
				objRuleDefination.setCustomDate("2018-08-23T07:10:43.423Z");
				objRuleDefination.setApplicationType("systemDate");
				setBody(builder, objectMapper, objRuleDefination, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/scorecard")) {
				objScoreCard.setRuleDefinitionId(apiParam.productRuleRuleId);
				objScoreCard.setScoreCardName(objAbstractTests.GenerateName());
				objScoreCard.setDescription("add score card");
				objScoreCard.setCustomDate("2018-08-23T07:10:43.423Z");
				objScoreCard.setApplicationType("systemDate");
				objScoreCard.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objScoreCard.setExpiryDate("2018-08-23T07:10:43.423Z");
				objScoreCard.setIsValidated(true);
				objScoreCard.setValidationRuleId("string");
				setBody(builder, objectMapper, objScoreCard, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleScoreCardId +"/scorecard")) {
				objScoreCard.setRuleDefinitionId(apiParam.productRuleRuleId);
				objScoreCard.setScoreCardName(objAbstractTests.GenerateName());
				objScoreCard.setDescription("add score card");
				objScoreCard.setCustomDate("2018-08-23T07:10:43.423Z");
				objScoreCard.setApplicationType("systemDate");
				objScoreCard.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objScoreCard.setExpiryDate("2018-08-23T07:10:43.423Z");
				objScoreCard.setIsValidated(true);
				objScoreCard.setValidationRuleId("string");
				setBody(builder, objectMapper, objScoreCard, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleScoreCardId + "/scorecardvariable")) {
				objScoreCardVariable.setVariableName(objAbstractTests.GenerateName());
				objScoreCardVariable.setVariableType("rule");
				objScoreCardVariable.setDefinitionId(apiParam.productRuleRuleId);
				objScoreCardVariable.setDataAttributeStoreName("CompanyDbCategoryRequested");
				objScoreCardVariableSource.setInputSource("string");
				objScoreCardVariableSource.setInputVariable("string");
				objScoreCardVariable.setScoreCardVariableSource(objScoreCardVariableSource);
				objScoreCardVariable.setWeightage(0);
				objScoreCardVariable.setDescription("CompanyDbCategoryRequested");
				setBody(builder, objectMapper, objScoreCardVariable, apiParam.contentType);
			} else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleScoreCardId +"/"+ apiParam.productRuleScoreCardVariableId + "/scorecardvariable")) {
				objScoreCardVariable.setVariableName(objAbstractTests.GenerateName());
				objScoreCardVariable.setVariableType("rule");
				objScoreCardVariable.setDefinitionId(apiParam.productRuleRuleId);
				objScoreCardVariable.setDataAttributeStoreName("CompanyDbCategoryRequested");
				objScoreCardVariableSource.setInputSource("string");
				objScoreCardVariableSource.setInputVariable("string");
				objScoreCardVariable.setScoreCardVariableSource(objScoreCardVariableSource);
				objScoreCardVariable.setWeightage(0);
				objScoreCardVariable.setDescription("CompanyDbCategoryRequested");
				setBody(builder, objectMapper, objScoreCardVariable, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/pricingstrategy")) {
				objPricingStrategy.setName(objAbstractTests.GenerateName());
				objPricingStrategy.setDescription("Eligibility check while submitting application");
				objPricingStrategy.setVersion("1.0");
				objPricingStrategy.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objPricingStrategy.setExpiryDate("2018-08-23T07:10:43.423Z");
				setBody(builder, objectMapper, objPricingStrategy, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase("/" + apiParam.productRulePricingStrategyId + "/pricingstrategy")) {
				objPricingStrategy.setName(objAbstractTests.GenerateName());
				objPricingStrategy.setDescription("Eligibility check while submitting application");
				objPricingStrategy.setVersion("1.0");
				objPricingStrategy.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objPricingStrategy.setExpiryDate("2018-08-23T07:10:43.423Z");
				setBody(builder, objectMapper, objPricingStrategy, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase("/" + apiParam.productRulePricingStrategyId +"/pricingstrategystep")) {
				objPricingStrategyStep.setStepName(objAbstractTests.GenerateName());
				objPricingStrategyStep.setParameterName("DrawDownEligibilityRule");
				List<String> parameterTypes = new ArrayList();
				parameterTypes.add("string");
				objPricingStrategyStep.setDescription("To Calculate P Score");
				objPricingStrategyStep.setStepType("scoreCard");
				objPricingStrategyStep.setSequenceNumber(1);
				objPricingStrategyStep.setContinueOnException(false);
				objPricingStrategyStep.setContinueOnNonEligibility(false);

				setBody(builder, objectMapper, objPricingStrategyStep, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase("/" + apiParam.productRulePricingStrategyId +"/"+ apiParam.productRulePricingStrategyStepId + "/pricingstrategystep")) {
				objPricingStrategyStep.setStepName(objAbstractTests.GenerateName());
				objPricingStrategyStep.setParameterName("DrawDownEligibilityRule");
				List<String> parameterTypes = new ArrayList();
				parameterTypes.add("string");
				objPricingStrategyStep.setDescription("To Calculate P Score");
				objPricingStrategyStep.setStepType("scoreCard");
				objPricingStrategyStep.setSequenceNumber(1);
				objPricingStrategyStep.setContinueOnException(false);
				objPricingStrategyStep.setContinueOnNonEligibility(false);

				setBody(builder, objectMapper, objPricingStrategyStep, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/segment")) {
				objSegment.setSegmentName(objAbstractTests.GenerateName());
				objSegment.setDataAttributeStoreName("CompanyDbCategoryRequested");
				objSegmentVariableSource.setInputSource("string");
				objSegmentVariableSource.setInputVariable("string");
				objSegment.setSegmentVariableSource(objSegmentVariableSource);
				objSegment.setDescription("SEGMENT");
				objSegment.setSegmentType("range");
				objSegment.setSegmentStorageName("string");
				objSegment.setRuleDefinitionId(apiParam.productRuleRuleId);
				objSegment.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objSegment.setExpiryDate("2018-08-23T07:10:43.423Z");
				objSegment.setCustomDate("2018-08-23T07:10:43.423Z");
				objSegment.setApplicationType("systemDate");
				setBody(builder, objectMapper, objSegment, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleSegmentId + "/segment")) {
				objSegment.setSegmentName(objAbstractTests.GenerateName());
				objSegment.setDataAttributeStoreName("CompanyDbCategoryRequested");
				objSegmentVariableSource.setInputSource("string");
				objSegmentVariableSource.setInputVariable("string");
				objSegment.setSegmentVariableSource(objSegmentVariableSource);
				objSegment.setDescription("SEGMENT");
				objSegment.setSegmentType("range");
				objSegment.setSegmentStorageName("string");
				objSegment.setRuleDefinitionId(apiParam.productRuleRuleId);
				objSegment.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objSegment.setExpiryDate("2018-08-23T07:10:43.423Z");
				objSegment.setCustomDate("2018-08-23T07:10:43.423Z");
				objSegment.setApplicationType("systemDate");
				setBody(builder, objectMapper, objSegment, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/lookup")) {
				objLookUpData.setLookupName(objAbstractTests.GenerateName());
				objLookUpData.setDataAttributeStoreName("DrawDownEligibility_Attribute");
				objLookUpData.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objLookUpData.setExpiryDate("2018-08-23T07:10:43.423Z");
				List<String> lookupBy = new ArrayList();
				lookupBy.add("string");
				objLookUpData.setLookupBy(lookupBy);
				List<String> lookupValues = new ArrayList();
				lookupValues.add("string");
				objLookUpData.setLookupValues(lookupValues);
				objLookUpData.setLookupStorageName("string");
				objLookUpData.setDescription("lookup");
				objLookUpData.setRuleDefinitionId(apiParam.productRuleRuleId);
				objLookUpData.setCustomDate("2018-08-23T07:10:43.423Z");
				objLookUpData.setApplicationType("systemDate");	
				setBody(builder, objectMapper, objLookUpData, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase( "/" + apiParam.productRuleLookupId+ "/lookup")) {
				objUpdateLookUp.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objUpdateLookUp.setExpiryDate("2018-08-23T07:10:43.423Z");
				List<String> lookupBy = new ArrayList();
				lookupBy.add("string");
				objUpdateLookUp.setLookupBy(lookupBy);
				List<String> lookupValues = new ArrayList();
				lookupValues.add("string");
				objUpdateLookUp.setLookupValues(lookupValues);
				objUpdateLookUp.setLookupStorageName("string");
				objUpdateLookUp.setDescription("lookup");
				objUpdateLookUp.setLastUpdatedDate("2018-08-23T07:10:43.423Z");
				objUpdateLookUp.setLastUpdatedBy("string");
				objUpdateLookUp.setDataAttributeStoreName("DrawDownEligibility_Attribute");
				objUpdateLookUp.setRuleDefinitionId(apiParam.productRuleRuleId);
				objUpdateLookUp.setCustomDate("2018-08-23T07:10:43.423Z");
				objUpdateLookUp.setApplicationType("systemDate");	
				setBody(builder, objectMapper, objUpdateLookUp, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/template")) {
				objTemplate.setEmailTemplateName(objAbstractTests.GenerateName());
				objTemplate.setEmailTemplateVersion("1.0");
				objTemplate.setMobileTemplateName(objAbstractTests.GenerateName());
				objTemplate.setMobileTemplateVersion("1.0");
				objTemplate.setDescription("Template");
				objTemplate.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objTemplate.setExpiryDate("2018-08-23T07:10:43.423Z");
				objTemplate.setIsEnable(true);
				objTemplate.setEventName("EventName");
				objTemplate.setRuleDefinationName("CompanyDbCategoryRequested");
				objTemplate.setDeRuleVersion("1.0");
				objTemplate.setCreatedDate("2018-08-23T07:10:43.423Z");
				objTemplate.setUpdatedDate("2018-08-23T07:10:43.423Z");	
				setBody(builder, objectMapper, objTemplate, apiParam.contentType);
			}
			else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleTemplateId + "/template")) {
				objTemplate.setEmailTemplateName(objAbstractTests.GenerateName());
				objTemplate.setEmailTemplateVersion("1.0");
				objTemplate.setMobileTemplateName(objAbstractTests.GenerateName());
				objTemplate.setMobileTemplateVersion("1.0");
				objTemplate.setDescription("Template");
				objTemplate.setEffectiveDate("2018-08-23T07:10:43.423Z");
				objTemplate.setExpiryDate("2018-08-23T07:10:43.423Z");
				objTemplate.setIsEnable(true);
				objTemplate.setEventName("EventName");
				objTemplate.setRuleDefinationName("CompanyDbCategoryRequested");
				objTemplate.setDeRuleVersion("1.0");
				objTemplate.setCreatedDate("2018-08-23T07:10:43.423Z");
				objTemplate.setUpdatedDate("2018-08-23T07:10:43.423Z");	
				setBody(builder, objectMapper, objTemplate, apiParam.contentType);
			}
			else if (requestType.equalsIgnoreCase("/" + apiParam.productRuleEntityType +"/" +apiParam.productRuleEntityId +"/"+ apiParam.productRuleProductId + "/" + apiParam.productRuleRuleName +"/ruledetails")) {
				JSONObject objExecute = new JSONObject();
				setBody(builder, objectMapper, objExecute, apiParam.contentType);
			}else if (requestType.equalsIgnoreCase("/" + apiParam.emailVeriEntityType + "/" + apiParam.emailVeriEntityId)) {
				JSONObject emailVerification =new JSONObject();
				emailVerification.put("Source","borrower");
				emailVerification.put("Name",apiParam.emailName);
				emailVerification.put("Email",apiParam.verificationEmailId);
				emailVerification.put("Data",new JSONObject());
				setBody(builder, objectMapper, emailVerification, apiParam.contentType);
			}
			

		return builder.build();
	}

	private static void setBody(RequestSpecBuilder builder, ObjectMapper objectMapper, Object body, String conType) {

		if (conType.contains("json")) {
			builder.setBody(body, objectMapper);
		} else if (conType.contains("xml")) {
			builder.setBody(body);
		}
	}

	public Response triggerPostRequest(String serviceUrl, Boolean isCorrectAuthorization, String authToken) {

		logger.info("=======================================================================================");
		logger.info("Triggering a POST request to :" + serviceUrl);
		logger.info("=======================================================================================");
		Header header;
		if (!isCorrectAuthorization) {
			header = new Header("Authorization", "Bearer 12" + authToken + "12");
		} else {
			header = new Header("Authorization", "Bearer " + authToken);
		}
		RequestSpecification requestSpec = constructRequestPayload(apiParam.requestType, apiParam.contentType,
				serviceUrl);
		apiParam.StartTime = System.currentTimeMillis();
		Response response = given().header(header).contentType(apiParam.contentType).spec(requestSpec).log().all()
				.when().post(serviceUrl);
		apiParam.EndTime = System.currentTimeMillis();

		logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
		return response;
	}

	public Response triggerPutRequest(String serviceUrl, Boolean isCorrectAuthorization, String authToken) {

		logger.info("=======================================================================================");
		logger.info("Triggering a PUT request to :" + serviceUrl);
		logger.info("=======================================================================================");
		Header header;
		if (!isCorrectAuthorization) {
			header = new Header("Authorization", "Bearer 12" + authToken + "12");
		} else {
			header = new Header("Authorization", "Bearer " + authToken);
		}
		RequestSpecification requestSpec = constructRequestPayload(apiParam.requestType, apiParam.contentType,
				serviceUrl);
		apiParam.StartTime = System.currentTimeMillis();
		Response response = given().header(header).contentType(apiParam.contentType).spec(requestSpec).log().all()
				.when().put(serviceUrl);
		apiParam.EndTime = System.currentTimeMillis();
		logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
		return response;
	}
	
	
	public Response triggerDeleteRequest(String serviceUrl, Boolean isCorrectAuthorization, String authToken) {

		logger.info("=======================================================================================");
		logger.info("Triggering a Delete request to :" + serviceUrl);
		logger.info("=======================================================================================");
		Header header;
		if (!isCorrectAuthorization) {
			header = new Header("Authorization", "Bearer 12" + authToken + "12");
		} else {
			header = new Header("Authorization", "Bearer " + authToken);
		}
		RequestSpecification requestSpec = constructRequestPayload(apiParam.requestType, apiParam.contentType,
				serviceUrl);
		apiParam.StartTime = System.currentTimeMillis();
		Response response = given().header(header).contentType(apiParam.contentType).log().all()
				.when().delete(serviceUrl);
		apiParam.EndTime = System.currentTimeMillis();
		logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
		return response;
	}

	public Response triggerGetRequest(String serviceUrl, Boolean isCorrectAuthorization, String authToken) {

		logger.info("=======================================================================================");
		logger.info("Triggering a GET request to :" + serviceUrl);
		logger.info("=======================================================================================");
		Header header;
		if (!isCorrectAuthorization) {
			header = new Header("Authorization", "Bearer 12" + authToken + "12");
		} else {
			header = new Header("Authorization", "Bearer " + authToken);
		}
		apiParam.StartTime = System.currentTimeMillis();
		Response response = given().header(header).contentType(apiParam.contentType).log().all().when().get(serviceUrl);
		apiParam.EndTime = System.currentTimeMillis();

		logger.info("============== Response Code: " + response.getStatusCode() + "=============================");
		return response;
	}

}
