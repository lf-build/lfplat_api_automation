package net.sigmainfo.lf.automation.api.dataset;

public class AddActionEntity {
	private String priority;
	private String dueDate;
	private String entityId;
	private String notes;

	public String getPriority() {
	return priority;
	}

	public void setPriority(String priority) {
	this.priority = priority;
	}

	public String getDueDate() {
	return dueDate;
	}

	public void setDueDate(String dueDate) {
	this.dueDate = dueDate;
	}

	public String getEntityId() {
	return entityId;
	}

	public void setEntityId(String entityId) {
	this.entityId = entityId;
	}

	public String getNotes() {
	return notes;
	}

	public void setNotes(String notes) {
	this.notes = notes;
	}

}
