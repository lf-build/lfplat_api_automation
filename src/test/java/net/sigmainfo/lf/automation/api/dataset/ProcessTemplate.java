package net.sigmainfo.lf.automation.api.dataset;

public class ProcessTemplate {
	private String applicationNumber;
	private String documentCategory;

	public String getApplicationNumber() {
	return applicationNumber;
	}

	public void setApplicationNumber(String applicationNumber) {
	this.applicationNumber = applicationNumber;
	}

	public String getDocumentCategory() {
	return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
	this.documentCategory = documentCategory;
	}
}
