package net.sigmainfo.lf.automation.api.tests;

import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.listener.ExtentProperties;
import net.sigmainfo.lf.automation.listener.Reporter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(strict = true, monochrome = true, features = "features", glue = "net.sigmainfo.lf.automation.stepdefinition", plugin = {
		"net.sigmainfo.lf.automation.listener.ExtentCucumberFormatter:output/LF_API_automation.html",
		"rerun:output/FailedScenario.txt" }, tags = {
				"@CreateBackOfficeUser,@LoginToBackOffice,@CheckCurrentUser,@ChangePassword,"
						+ "@LogoutCurrentUser,@RequestResetPasswordToken,@GetUserDetailsByUsername,"
						+ "@ResetPassword,@DeactivateUser,@LoginforDeactivatedUser,@ActivateUser,"
						+ "@BackOfficeLoginforActivatedUser,@GetUserDetailsById,"
						+ "@GetReferenceEntityByIdAndType,@GetReferenceEntityByConsumerIDParams,@GetConsumerByID,@GetDuplicateConsumerByField,"
						+ "@GetDuplicateConsumerByFilter,@GetReferenceEntityByID,@GetReferenceEntityByConsumerID,"
						+ "@InitiatesVerification,@VerifiesMobile,@GetsVerificationDetails,"
						+ "@Configuration,@GeneratesNumber,@GenerateNestedNumber,"
						+ "@GetsAll,@GetsByStatus,@GetAllByStatus,"
						+ "@GenerateSpecifiedTemplate,@GenerateSpecifiedFormat,@GenerateHTmlFormat,"
						+ "@RequestListByEntity,@AddsEntityByRequestId,@AddsEntity,@GetRequestsByEntity,@GetsRequestByStatusandId,@GetsRequestByStatus,"
						+ "@GetsSummaryByRequetId,@GetSummary,@GetsRequestByRequestIdandId,@GetsGroupSummary,@GetsRequestTypes,"
						+ "@GetsDate,@GetsPeriodicDates,"
						+ "@Queuesinstruction,@DequeuesinstructionById,@DequeuesByInternalRefNum,@CreatesFile,@SendsFile,@Cancelsfile,"
						+ "@Processesfile,@GetsAchNocReturnData,@GetsInstructionByRefNum,@GetsInstructionByInternalrefNum,"
						+ "@GetsInstructionByFundingSource,@RejectsInstructionByInternalRefNum,@GetsPauseBylRefNum,@PausespaymentByReferenceNumber,@ResumesPayment,@GetsPauses,"
						+ "@GetsHistory,@SendsSpecifiedTemplateName,@SendsSpecificEntityId,@SendsMail,@GetsHistoryByTemplate,"
						+ "@GetsVerificationDetail,@ResetsFacts,@GetsVerificationStatus,@GetsVerificationMethod,@GetsVerification,"
						+ "@InitiatesManualVerification,@InitiatesSyndicationVerification,@PendingsTheDocument,@GetsDocuments,@EventsRepublish,@InitiatesDynamicFactVerification,"
						+ "@GetsRequiredDocument,@GetsDocumentsByFact,@GetsVerificationFacts,@VerifiesDocument,"
						+ "@CreateAlert,@GetAlerts,@GetTags,@DismissAlerts,@UndoDismissAlerts,"
						+ "@GetsDefinition,@StartsSpecifiedWorkflow,@GetsSpecifiedWorkflowById,"
						+ "@ExecutesStep,@PausesSpecifiedWorkflow,@CancelsSpecifiedWorkflow,"
						+ "@SendsSMS,@SendspecifiedEntitytype,"
						+ "@CreateBackOfficeuserOne,@LoginToBackOfficeuserOne,@CreateBackOfficeuserTwo,@LoginToBackOfficeuserTwo,"
						+ "@ClaimsByEntity,@AssignsByEntity,@DeterminesAssignedToMe,@EscalatesByEntity,"
						+ "@GetsEscalations,@AcceptsByEntity,@CompletesByEntity,@ReleasesByEntity,"
						+ "@UnassignsByEntity,@GetsAssignments,@GetsByUser,"
						+ "@AddRule,@UpdatRule,@GetByRuleName,"
						+ "@SetsAttributeByName,@SetsSchema,@SetsAttribute,@GetsAllAttributes,"
						+ "@GetsAttributeByNames,@GetsAttributeBySpecificNames,@GetsAttribute,@RemovesAttribute,"
						+ "@IssueToken,@ValidateToken,"
						+ "@AddTemplate,@ProcessesTemplate,@GetAllTemplate,@GetTemplateProperties,@GetSpecifiedTemplate,@UpdateTemplate,@DeleteTemplate,"
						+ "@ProcesStatusWorkFlow,@ChangeStatus,@InitiateSubWorFlow,@MoveSubWorkFlow,"
						+ "@GetsChecklist,@GetTransitions,@GetReasons,@GetActivities,@GetStatusByStatusCode,"
						+ "@GetsSubStatusDetails,@GetActiveStatusWorkFlow,@GetEntityStatusHistory,@GetsStatus,@GetWorkFlowNames,@GetStatusByProduct,"
						+ "@GetsDataByIdentifier,"
						+ "@AddRuleDefinition,@UpdateRuleDefinition,@DeleteRuleDefinition,@GetRuleDefinitionById,@GetProductRuleDetail,"
						+ "@GetAllRuleDefinitionsByRuleType,@GetRuleDefinitionByName,@GetRuleDefinitionDetails,"
						+ "@ExecuteWithRuleName,@ExecuteWithRuleNameAndEventDataAsBodyParams,"
						+ "@AddScoreCard,@UpdateScoreCardById,@DeleteScoreCard,@GetScoreCardDetailById,@RunScoreCard,"
						+ "@AddScoreCardVariable,@UpdateScoreCardVariableByVariableId,@DeleteScoreCardVariable,"
						+ "@GetScoreCardVariableByVariableId,@GetScoreCardByName,@GetAllScoreCardByEntityType,@ValidateScoreCard,"
						+ "@AddPricingStrategy,@UpdatePricingStrategy,@DeletePricingStrategy,@GetPricingStrategyById,@RunPricingStrategy,@GetAllPricingStrategy,@GetStrategyHistory,"
						+ "@AddPricingStrategySteps,@UpdatePricingStrategyStep,@DeletePricingStrategyStep,@GetPricingStrategyStep,"
						+ "@AddSegment,@UpdateSegmentById,@DeleteSegment,@GetSegmentById,@GetAllSegments,@GetSegmentByName,@RunSegment,"
						+ "@AddLookup,@UpdateLookup,@DeleteLookup,@GetLookupByLookupId,@GetLookupByName,@GetAllLookupDataByEntityType,@RunLookup,"
						+ "@AddTemplate,@UpdateTemplate,@GetAllTemplateByEntityType,@GetTemplateByEventName,"
						+ "@InitiateEmailVerification,@VerifiesEmail,@GetsVerificationStatus,"
						+ "@GetActivitiesWithProductId,@GetAllActivitiesWithProductId,@GetsActivitiesByTypeAndId,@GetsAllActivitiesWithoutProductId,"
						+ "@GetsActivityAllCleanEntries,@GetsActivitiesByTagsWithProductid,@GetsActivitiesByNotagsWithProductId,@GetsActivitiesByTagsWithoutProductid,"
						+ "@GetsActivitiesByNotagsWithoutProductId,@GetsTagsWithProductId,@GetsTagsWithoutProductId" })
						
public class RunAPITest extends AbstractTestNGCucumberTests {

	public static AbstractTests ABtests = new AbstractTests();
	public static String env = null;

	@BeforeClass
	@Parameters({ "Environment" })
	public void setupalways(String environment) throws Exception {
		env = environment;
		ABtests.postConstruct(environment);
	}

	@AfterClass
	public void teardown() throws Exception {
		Reporter.loadXMLConfig(new File("extent-config.xml"));
		Reporter.setSystemInfo("Tester name", System.getProperty("user.name"));
		Reporter.setSystemInfo("Tester Role", "admin");
		Reporter.setSystemInfo("os", "window 8");
		Reporter.setSystemInfo("Server", env);
		Reporter.setSystemInfo("Owner", "Balaji.G");
	}
}