package net.sigmainfo.lf.automation.api.dataset;

public class SetAttribute {

	private AdditionalProp1 additionalProp1;
	private AdditionalProp2 additionalProp2;
	private AdditionalProp3 additionalProp3;

	public AdditionalProp1 getAdditionalProp1() {
	return additionalProp1;
	}

	public void setAdditionalProp1(AdditionalProp1 additionalProp1) {
	this.additionalProp1 = additionalProp1;
	}

	public AdditionalProp2 getAdditionalProp2() {
	return additionalProp2;
	}

	public void setAdditionalProp2(AdditionalProp2 additionalProp2) {
	this.additionalProp2 = additionalProp2;
	}

	public AdditionalProp3 getAdditionalProp3() {
	return additionalProp3;
	}

	public void setAdditionalProp3(AdditionalProp3 additionalProp3) {
	this.additionalProp3 = additionalProp3;
	}

	public class AdditionalProp3 {


	}

	public class AdditionalProp2 {


	}
	
	public class AdditionalProp1 {


	}


}
