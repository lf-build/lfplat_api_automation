package net.sigmainfo.lf.automation.api.dataset;

public class IssueToken {
	private String issuer;
	private String issuedAt;
	private String expiration;
	private String subject;
	private String tenant;
	private Integer expirationDays;
	private String entityType;
	private String entityId;
	private String value;

	public String getIssuer() {
	return issuer;
	}

	public void setIssuer(String issuer) {
	this.issuer = issuer;
	}

	public String getIssuedAt() {
	return issuedAt;
	}

	public void setIssuedAt(String issuedAt) {
	this.issuedAt = issuedAt;
	}

	public String getExpiration() {
	return expiration;
	}

	public void setExpiration(String expiration) {
	this.expiration = expiration;
	}

	public String getSubject() {
	return subject;
	}

	public void setSubject(String subject) {
	this.subject = subject;
	}

	public String getTenant() {
	return tenant;
	}

	public void setTenant(String tenant) {
	this.tenant = tenant;
	}

	public Integer getExpirationDays() {
	return expirationDays;
	}

	public void setExpirationDays(Integer expirationDays) {
	this.expirationDays = expirationDays;
	}

	public String getEntityType() {
	return entityType;
	}

	public void setEntityType(String entityType) {
	this.entityType = entityType;
	}

	public String getEntityId() {
	return entityId;
	}

	public void setEntityId(String entityId) {
	this.entityId = entityId;
	}

	public String getValue() {
	return value;
	}

	public void setValue(String value) {
	this.value = value;
	}
}
