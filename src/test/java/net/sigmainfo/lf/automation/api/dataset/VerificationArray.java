package net.sigmainfo.lf.automation.api.dataset;

import java.util.List;

public class VerificationArray {

private String entityType;
private String entityId;
private String factName;
private String methodName;
private String productId;
private String documentStatus;
private String statusWorkFlowId;
private List<VerificationSource> verificationSources = null;



public String getDocumentStatus() {
	return documentStatus;
}

public void setDocumentStatus(String documentStatus) {
this.documentStatus = documentStatus;}

public String getEntityType() {
return entityType;
}

public void setEntityType(String entityType) {
this.entityType = entityType;
}

public String getEntityId() {
return entityId;
}

public void setEntityId(String entityId) {
this.entityId = entityId;
}

public String getFactName() {
return factName;
}

public void setFactName(String factName) {
this.factName = factName;
}

public String getMethodName() {
return methodName;
}

public void setMethodName(String methodName) {
this.methodName = methodName;
}

public String getProductId() {
return productId;
}

public void setProductId(String productId) {
this.productId = productId;
}

public String getStatusWorkFlowId() {
return statusWorkFlowId;
}

public void setStatusWorkFlowId(String statusWorkFlowId) {
this.statusWorkFlowId = statusWorkFlowId;
}

public List<VerificationSource> getVerificationSources() {
return verificationSources;
}

public void setVerificationSources(List<VerificationSource> verificationSources) {
this.verificationSources = verificationSources;
}

public class VerificationSource {

private String sourceName;

public String getSourceName() {
return sourceName;
}

public void setSourceName(String sourceName) {
this.sourceName = sourceName;
}

}

}
