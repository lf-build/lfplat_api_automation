package net.sigmainfo.lf.automation.api.dataset;

public class SetSchema {
	private Boolean isSecondryEnable;

	public Boolean getIsSecondryEnable() {
	return isSecondryEnable;
	}

	public void setIsSecondryEnable(Boolean isSecondryEnable) {
	this.isSecondryEnable = isSecondryEnable;
	}
}
