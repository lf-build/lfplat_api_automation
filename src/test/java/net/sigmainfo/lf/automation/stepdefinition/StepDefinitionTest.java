package net.sigmainfo.lf.automation.stepdefinition;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.jayway.restassured.response.Response;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.formatter.model.Feature;
import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.api.dataset.PartnerArray;
import net.sigmainfo.lf.automation.api.dataset.VerificationArray;
import net.sigmainfo.lf.automation.api.dataset.VerificationArray.VerificationSource;
import net.sigmainfo.lf.automation.api.function.ApiFuncUtils;
import net.sigmainfo.lf.automation.common.AbstractTests;
import net.sigmainfo.lf.automation.listener.Reporter;

public class StepDefinitionTest {

	private org.slf4j.Logger logger = LoggerFactory.getLogger(StepDefinitionTest.class);
	public static AbstractTests ABtests = new AbstractTests();
	public static ApiFuncUtils apiFuncUtils = new ApiFuncUtils();
	public static String testResults = null;
	public static String RequestMethod = "";
	public static String AuthorizationToken = "";
	public static Response responsecode = null;
	String GenerateId = ABtests.GenerateEntityId();

	@Autowired
	ApiParam apiParam;
	boolean isCorrectAuthorization = true;
	String serviceUrl = apiParam.baseresturl + ":" + apiParam.ASPort + apiParam.requestType;

	@Before
	public void beforeHook(Scenario scenario) {
		logger.info("------------------------------------------------------------------------------------------------");
		apiParam.currentFeatureFile = scenario.getId().split(";")[0];
		apiParam.currentScenarioName = scenario.getName();
		AuthorizationToken = apiParam.AuthToken;
		logger.info("Current Running Feature File : " + apiParam.currentFeatureFile);
		logger.info(" Current Running Scenario Name : " + apiParam.currentScenarioName);
		logger.info("------------------------------------------------------------------------------------------------");
	}

	@Given("^RequestType \"([^\"]*)\" and Port (.*) and Method is (.*)$")
	public void requesttype_something_and_port_and_method_is(String RequestType, String port, String requestMethod)
			throws Throwable {
		try {
			RequestMethod = requestMethod;
			apiParam.requestType = RequestType;
			apiParam.ASPort = port;
		} finally {

		}
	}

	@When("^Create User with Role (.*)$")
	public void create_user_with_role(String role) throws Throwable {
		try {
			apiParam.username = ABtests.GenerateUsername();
			apiParam.password = "Sigma@123";
			apiParam.email = ABtests.GenerateEmail();
			apiParam.roles = role.split(" ");
		} finally {
			logger.info(
					"----------------------------------------- Prerequisite ------------------------------------------");
			logger.info(" UserName : " + apiParam.username);
			logger.info(" Email : " + apiParam.email);
			logger.info(" Request Type : " + apiParam.requestType);
			logger.info(" Port : " + apiParam.ASPort);
			logger.info(" roles = " + role);
			logger.info(
					"-------------------------------------------------------------------------------------------------");
		}
	}

	@Then("^\"([^\"]*)\" and Status Code should be (.*)$")
	public void user_something_and_status(String PrintText, String StatusCode) throws Throwable {
		testResults = "Failed";
		try {
			boolean isCorrectAuthorization = true;
			String serviceUrl = null;
			serviceUrl = apiParam.baseresturl + ":" + apiParam.ASPort + apiParam.requestType;
			logger.info("Service URL: " + serviceUrl);
			if (RequestMethod.equalsIgnoreCase("PUT")) {
				responsecode = apiFuncUtils.triggerPutRequest(serviceUrl, isCorrectAuthorization, AuthorizationToken);
			} else if (RequestMethod.equalsIgnoreCase("POST")) {
				responsecode = apiFuncUtils.triggerPostRequest(serviceUrl, isCorrectAuthorization, AuthorizationToken);
			} else if (RequestMethod.equalsIgnoreCase("GET")) {
				responsecode = apiFuncUtils.triggerGetRequest(serviceUrl, isCorrectAuthorization, AuthorizationToken);
			}else if (RequestMethod.equalsIgnoreCase("DELETE")) {
				responsecode = apiFuncUtils.triggerDeleteRequest(serviceUrl, isCorrectAuthorization, AuthorizationToken);
			}
			logger.info("----------------------------------------- PostRun ------------------------------------------");
			long Difference = apiParam.EndTime - apiParam.StartTime;
			long seconds = Difference / 1000;
			long minutes = seconds / 60;
			long hours = minutes / 60;
			if (PrintText.equals("User should be able to Login")) {
				apiParam.LoginToken = new JSONObject(responsecode.prettyPrint().toString()).getString("token");
				logger.info("Login Token : " + apiParam.LoginToken);
			} else if (PrintText.equals("We should be able to get the User details")) {
				apiParam.resetToken = new JSONObject(responsecode.prettyPrint().toString()).getString("resetToken");
				apiParam.id = new JSONObject(responsecode.prettyPrint().toString()).getString("id");
				logger.info("Reset Token : " + apiParam.resetToken);
				logger.info("id : " + apiParam.id);
			} else if (PrintText.equals("getting details if refernece entity")) {
				apiParam.consumerEntityId= new JSONObject(responsecode.prettyPrint().toString()).getString("entityId");
				apiParam.entityType = new JSONObject(responsecode.prettyPrint().toString()).getString("entityType");
				apiParam.id = new JSONObject(responsecode.prettyPrint().toString()).getString("id");
				JSONArray consumers = new JSONObject(responsecode.prettyPrint().toString()).getJSONArray("consumers");
				apiParam.consumersID= (String) consumers.get(0);

				
				logger.info("EntityId : " + apiParam.consumerEntityId);
				logger.info("EntityType : " + apiParam.entityType);
				logger.info("Id : " + apiParam.id);
				logger.info("consumers : " + apiParam.consumersID);
			} else if (PrintText.equals("should get all the details of partner")) {
				String responsecodeS= responsecode.prettyPrint().toString();
				apiParam.status = new Gson().fromJson(responsecodeS.substring(1,(responsecodeS.length()-1)), PartnerArray.class).getStatusCode();
			} else if (PrintText.equals("initiated")) {
				apiParam.referenceNumber = new JSONObject(responsecode.prettyPrint().toString()).getString("referenceNumber");
				logger.info("referenceNumber : " + apiParam.referenceNumber);
			} else if (PrintText.equals("should get configuration of number genarator")) {
				apiParam.entityid = ((JSONObject) new JSONObject(responsecode.prettyPrint().toString()).get("configurations")).keySet();

			} else if (PrintText.equals("all details of template should be displayed")) {
				apiParam.templateName = new JSONObject(responsecode.prettyPrint().toString()).getString("name");
				apiParam.version = new JSONObject(responsecode.prettyPrint().toString()).getString("version");
				apiParam.format = new JSONObject(responsecode.prettyPrint().toString()).getString("format");
				logger.info("templateName : " + apiParam.templateName);
				logger.info("version : " + apiParam.version);
				logger.info("format : " + apiParam.format);
				
			} else if (PrintText.equals("Should get the action details of specified entity")) {
				String responsecodeS= responsecode.prettyPrint().toString();
				JSONObject entity = new JSONObject(responsecodeS.substring(1,(responsecodeS.length()-1)));
				apiParam.actionEntityID = entity.getString("entityId");
				apiParam.actionEntityType= entity.getString("entityType");
				apiParam.actionId= entity.getString("id");
				apiParam.actionStatus= entity.getString("status");
				JSONObject reqType = (entity).getJSONObject("requestType");
				apiParam.requestId= (String) reqType.get("requestId");	
			} else if (PrintText.equals("instruction is queued")) {
				apiParam.achreferenceNumber= new JSONObject(responsecode.prettyPrint().toString()).getString("referenceNumber");
				apiParam.fundingSourceId= new JSONObject(responsecode.prettyPrint().toString()).getString("fundingSourceId");
				apiParam.achstatus= new JSONObject(responsecode.prettyPrint().toString()).getString("status");
				apiParam.internalReferenceNumber= (int) new JSONObject(responsecode.prettyPrint().toString()).get("internalReferenceNumber");
				apiParam.InstructionId= new JSONObject(responsecode.prettyPrint().toString()).getString("id");
				//apiParam.executionDate= new JSONObject(responsecode.prettyPrint().toString()).getString("executionDate");
			}else if (PrintText.equals("Creates the file")) {
					String responsecodeS= responsecode.prettyPrint().toString();
					JSONObject achEntity = new JSONObject(responsecodeS.substring(1,(responsecodeS.length()-1)));
					apiParam.processStartDateTime = achEntity.getString("processStartDateTime");
					apiParam.processEndDateTime = achEntity.getString("processEndDateTime");
					apiParam.fileID = achEntity.getString("id");
				} else if (PrintText.equals("getting history of send mail")) {
					
					String responsecodeS= responsecode.prettyPrint().toString();
					JSONObject entity = new JSONObject(responsecodeS.substring(1,(responsecodeS.length()-1)));
					apiParam.emailTemplateName= entity.getString("templateName");
					apiParam.emailEntityId = entity.getString("entityId");
					apiParam.emailEntityType= entity.getString("entityType");
					apiParam.body= entity.getString("body");
					apiParam.toEmailAddress = (entity).getJSONArray("toEmailAddress");
					apiParam.mailId= apiParam.toEmailAddress.getJSONObject(0).getString("email");
					apiParam.ccEmailAddress = (entity).getJSONArray("ccEmailAddress");
					apiParam.bccEmailAddress = (entity).getJSONArray("bccEmailAddress");
					apiParam.subject = entity.getString("subject");
				} else if (PrintText.equals("getting verification details")) {
				    System.out.println("CONDITION MATCHED"); 
				    String responsecodeS= responsecode.prettyPrint().toString();
				    apiParam.verificationEntityId = new Gson().fromJson(responsecodeS.substring(1,(responsecodeS.length()-1)), VerificationArray.class).getEntityId();
				    apiParam.verificationEntityType = new Gson().fromJson(responsecodeS.substring(1,(responsecodeS.length()-1)), VerificationArray.class).getEntityType();
				    apiParam.fact= new Gson().fromJson(responsecodeS.substring(1,(responsecodeS.length()-1)), VerificationArray.class).getFactName();
				    apiParam.method = new Gson().fromJson(responsecodeS.substring(1,(responsecodeS.length()-1)), VerificationArray.class).getMethodName();
				    apiParam.workFlowStatusId = new Gson().fromJson(responsecodeS.substring(1,(responsecodeS.length()-1)), VerificationArray.class).getStatusWorkFlowId();
				    VerificationSource source = new Gson().fromJson(responsecodeS.substring(1,(responsecodeS.length()-1)), VerificationArray.class).getVerificationSources().get(0);
				    apiParam.verificationSource = source.getSourceName();
				    apiParam.productId = new Gson().fromJson(responsecodeS.substring(1,(responsecodeS.length()-1)), VerificationArray.class).getProductId();
				    apiParam.verificationStatus = new Gson().fromJson(responsecodeS.substring(1,(responsecodeS.length()-1)), VerificationArray.class).getDocumentStatus();

				} else if (PrintText.equals("Should get the alerts for the specified entity")) {
						String responsecodeS= responsecode.prettyPrint().toString();
						JSONObject entity = new JSONObject(responsecodeS.substring(1,(responsecodeS.length()-1)));
						apiParam.alertID = entity.getString("id");
				} else if (PrintText.equals("Workflow is started")) {
					apiParam.appFlowID = new JSONObject(responsecode.prettyPrint().toString()).getString("id");
					logger.info("appFlowID : " + apiParam.appFlowID);

			   }else if (PrintText.equals("userOne should be able to Login")) {
					apiParam.LoginToken1 = new JSONObject(responsecode.prettyPrint().toString()).getString("token");
					logger.info("Login Token : " + apiParam.LoginToken1);
			   } else if (PrintText.equals("userTwo should be able to Login")) {
					apiParam.LoginToken2 = new JSONObject(responsecode.prettyPrint().toString()).getString("token");
					logger.info("Login Token : " + apiParam.LoginToken2);
			   } else if (PrintText.equals("We should be able to get escalations details")) {
					String responsecodeS= responsecode.prettyPrint().toString();
					JSONObject esccaltion = new JSONObject(responsecodeS.substring(1,(responsecodeS.length()-1)));
					apiParam.assignEsclationId = esccaltion.getString("id");
			   }else if (PrintText.equals("userOne should be created")) {
					apiParam.assignUsername1 = new JSONObject(responsecode.prettyPrint().toString()).getString("username");
				}
				else if (PrintText.equals("userTwo should be created")) {
					apiParam.assignUsername2 = new JSONObject(responsecode.prettyPrint().toString()).getString("username");
				} else if (PrintText.equals("Token is isssued")) {
					apiParam.token = new JSONObject(responsecode.prettyPrint().toString()).getString("value");
					logger.info(" Token : " + apiParam.token);
				} else if (PrintText.equals("rule defination is added")) {
					apiParam.productRuleRuleName = new JSONObject(responsecode.prettyPrint().toString()).getString("ruleName");
					apiParam.productRuleClassName = new JSONObject(responsecode.prettyPrint().toString()).getString("ruleClass");
					apiParam.productRuleRuleType= new JSONObject(responsecode.prettyPrint().toString()).getString("ruleType");
					apiParam.productRuleRuleId = new JSONObject(responsecode.prettyPrint().toString()).getString("id");
				} else if (PrintText.equals("addded score card successfully")) {
					apiParam.productRuleScoreCardName = new JSONObject(responsecode.prettyPrint().toString()).getString("scoreCardName");
					apiParam.productRuleScoreCardId= new JSONObject(responsecode.prettyPrint().toString()).getString("id");
				} else if (PrintText.equals("Added scorecard variable successfully")) {
					JSONArray responsecodeS=  new JSONObject(responsecode.prettyPrint()).getJSONArray("scoreCardVariables");
					JSONObject jsonobject = responsecodeS.getJSONObject(0);
					apiParam.productRuleScoreCardVariableId = jsonobject.getString("scoreCardVariableId");
				} else if (PrintText.equals("added pricig strategy successfully")) {
					apiParam.productRulePricingStrategyName = new JSONObject(responsecode.prettyPrint().toString()).getString("name");
					apiParam.productRulePricingStrategyVersion = new JSONObject(responsecode.prettyPrint().toString()).getString("version");
					apiParam.productRulePricingStrategyId = new JSONObject(responsecode.prettyPrint().toString()).getString("id");
				} else if (PrintText.equals("added pricing strategy steps successfully")) {
					JSONArray responsecodeS=  new JSONObject(responsecode.prettyPrint()).getJSONArray("pricingStrategyStep");
					JSONObject jsonobject = responsecodeS.getJSONObject(0);
					apiParam.productRulePricingStrategyStepId = jsonobject.getString("strategyStepId");
				} else if (PrintText.equals("added  segment successfully")) {
					apiParam.productRuleSegmentName = new JSONObject(responsecode.prettyPrint().toString()).getString("segmentName");
					apiParam.productRuleSegmentId = new JSONObject(responsecode.prettyPrint().toString()).getString("id");
				} else if (PrintText.equals("Added lookup successfully")) {
					apiParam.productRuleLookupName = new JSONObject(responsecode.prettyPrint().toString()).getString("lookupName");
					apiParam.productRuleLookupId = new JSONObject(responsecode.prettyPrint().toString()).getString("id");
				}else if (PrintText.equals("Added template successfully")) {
					apiParam.productRuleTemplateName = new JSONObject(responsecode.prettyPrint().toString()).getString("eventName");
					apiParam.productRuleTemplateId = new JSONObject(responsecode.prettyPrint().toString()).getString("id");
				} 
			Reporter.addStepLog("Response Time : " + hours +"h " + minutes + "m " + seconds +"s " + (Difference-(seconds*1000))+"ms");
			assertEquals(responsecode.getStatusCode(), Integer.parseInt(StatusCode));
			testResults = "Passed";
		} finally {
			logger.info("*****Test Result: " + testResults + "*****");
			logger.info(
					"-------------------------------------------------------------------------------------------------");
		}
	}

	@When("^Portal is (.+)$")
	public void portal_is(String portal) throws Throwable {
		try {
			apiParam.portal = portal;
		} finally {
			logger.info(" portal : " + portal);
		}
	}

	@When("^User is LoggedIn$")
	public void user_is_loggedin() throws Throwable {
		try {
			AuthorizationToken = apiParam.LoginToken;
		} finally {
			logger.info(" AuthorizationToken : " + apiParam.LoginToken);
		}
	}

	@When("^Get active User$")
	public void get_active_user() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.username;
	}

	@When("^The token is requested$")
	public void the_token_is_requested() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.resetToken;
		apiParam.password = "Sigma@123";
	}

	@When("^Get active User Id$")
	public void get_active_user_id() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.id;
	}
	
	@When("^Run Validation for :$")
	public void run_validation_for_(Map<String, String> tableData) throws Throwable {
	 tableData.get("Name");
	  
	}
	
	
	@When("^ConsumerID is (.*)$")
	public void consumerid_is(String consumerID) throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.getConsumersID();
		try {
			apiParam.consumersID = consumerID;
		}
		finally {
			logger.info(" consumers : " + consumerID);
		}
		
	}

	@When("^field is (.*)$")
	public void field_is(String field) throws Throwable {
		try {
			apiParam.field = null;
		} finally {
			logger.info(" field : " + field);
		}
	}

	@When("^duplicate is done by filters$")
	public void duplicate_is_done_by_filters() throws Throwable {

	}

	@When("^Id is (.*)$")
	public void id_is(String id) throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.id;
		try {
			apiParam.id = id;
		} finally {
			logger.info(" id : " + id);
		}
	}

	@When("^EntityId and EntityType is given$")
	public void entityid_is_and_EntityType_is() throws Throwable {
		apiParam.setEntityType("application");
		apiParam.setConsumerEntityId("0002062");
		apiParam.requestType = apiParam.requestType + apiParam.consumerEntityId + "/" + apiParam.entityType ;
	}
	
	@When("^ConsumerID id usimg paramas$")
	public void consumerid_id_usimg_paramas() throws Throwable {
		
	    
	}

	
	
	
	@When("^entityType is (.*)$")
	public void entitytype_is(String entityType) throws Throwable {
		
		try {
			apiParam.entityType = "application";
			apiParam.entityId ="0000120";
			apiParam.requestType = apiParam.requestType + apiParam.entityType + "/" +apiParam.entityId ;
		} finally {
			logger.info(" entityType : " + entityType);
		}
	    
	}

	@When("^reference number is (.*)$")
	public void reference_number_is(String referenceNumber ) throws Throwable {
		try {
			apiParam.verificationCode ="304721";
			apiParam.requestType = apiParam.requestType + apiParam.referenceNumber  + "/" +apiParam.verificationCode;
		} finally {
			logger.info(" referenceNumber : " + referenceNumber);
		}

	}
	
	@When("^using refNumber$")
	public void using_refNumber() throws Throwable {
			apiParam.requestType = apiParam.requestType + apiParam.getReferenceNumber();

	
	}
	
	@When("^Generate Number with entityId and subEntityIds$")
	public  void generate_Number_with_entityId_and_subEntityIds() throws Throwable {

		for(int i=0; i< apiParam.entityid.size();i++) {
			Object[] res =apiParam.entityid.toArray();
			apiParam.subEntityIds = ABtests.GenerateNumbers();
			apiParam.requestType = "/" + res[i] + "/" +apiParam.subEntityIds ;
			String serviceUrl = apiParam.baseresturl + ":" + apiParam.ASPort + apiParam.requestType;
			responsecode = apiFuncUtils.triggerPostRequest(serviceUrl, isCorrectAuthorization, AuthorizationToken);
		}
	}
	
	
	
	@When("^configuration is given$")
	public void configuration_is_given() throws Throwable {
		for(int i=0; i< apiParam.entityid.size();i++) {
			Object[] res =apiParam.entityid.toArray();
			apiParam.requestType = "/" + res[i] ;
			String serviceUrl = apiParam.baseresturl + ":" + apiParam.ASPort + apiParam.requestType;
			responsecode = apiFuncUtils.triggerPostRequest(serviceUrl, isCorrectAuthorization, AuthorizationToken);
		}
	}

	
	
	
	@When("^CONFIG$")
	public void config() throws Throwable {

	}
	
	@When("^gets all$")
	public void gets_all() throws Throwable {
			
	}
	
	
	@When("^Status is  given$")
	public void status_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.getStatus();

	}

	
	@When("^templatename is (.*)$")
	public void templatename_is(String templateName) throws Throwable {
		try {
			apiParam.templateName = "SendLocAgreementToAllOwners";
			apiParam.version ="1.0";
			apiParam.format ="Html";
			apiParam.requestType = "/" + apiParam.templateName + "/" +apiParam.version +  "/" + apiParam.format + apiParam.requestType;
		} finally {
			logger.info(" templateName : " + templateName);
		}
	}

	@When("^format is (.*)$")
	public void format_is(String format) throws Throwable {
		try {
			//apiParam.format = "Pdf";
			
		} finally {
			logger.info(" format : " + format);
		}
	}
	
	@When("^Template is added$")
	public void template_is_added() throws Throwable {

	}
	
	
	
	
	@When("^ActionentityType is given$")
	public void entitytype_is_given() throws Throwable {

	}
	
	
	@When("^request id is <requestId>$")
	public void request_id_is_requestId() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.actionEntityType + "/" + apiParam.actionEntityID + "/" + apiParam.requestId;
	}

	@When("^adding entity$")
	public void adding_entity() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.actionEntityType  + "/" + apiParam.requestId;
	}

	@When("^entity id is given$")
	public void entity_id_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.actionEntityType + "/" + apiParam.actionEntityID;
	}

	@When("^status and id is given$")
	public void status_and_id_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.actionEntityType + "/" + apiParam.actionEntityID + "/status/" + apiParam.actionStatus;
	}
	
	@When("^actionstatus is given$")
	public void actionstatus_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.actionEntityType + "/status/" + apiParam.actionStatus;
	}

	@When("^summary with requestid$")
	public void summary_with_requestid() throws Throwable {
		apiParam.requestType = "/" + apiParam.actionEntityType  + "/" + apiParam.requestId  + apiParam.requestType ;
	}

	@When("^summary     with wntity is given$")
	public void summary_with_wntity_is_given() throws Throwable {
		apiParam.requestType = "/" + apiParam.actionEntityType + apiParam.requestType;
	}

	@When("^given requestId and Id$")
	public void given_requestId_and_Id() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.actionEntityType  + "/" + apiParam.requestId + "/" + apiParam.actionId;
	}

	@When("^group summary$")
	public void group_summary() throws Throwable {
		apiParam.requestType = "/"  + apiParam.actionEntityType  + apiParam.requestType + apiParam.actionEntityID;
	}

	@When("^cancel with requestId$")
	public void cancel_with_requestId() throws Throwable {
		apiParam.requestType = "/" +apiParam.actionEntityType +"/"+ apiParam.actionEntityID  + "/" + apiParam.requestId + apiParam.requestType;
	}

	@When("^verify with requestId$")
	public void verify_with_requestId() throws Throwable {
		apiParam.requestType = "/" +apiParam.actionEntityType +"/"+ apiParam.actionEntityID  + "/" + apiParam.requestId + apiParam.requestType;
	}

	@When("^request types is given$")
	public void request_types_is_given() throws Throwable {
		apiParam.requestType =  "/" + apiParam.actionEntityType + apiParam.requestType;
	}



	

	@When("^current date is given$")
	public void current_date_is_given()  throws Throwable {
		apiParam.currentDate =java.time.LocalDate.now(); 
		apiParam.requestType =apiParam.requestType + apiParam.currentDate;
	}

	@When("^term and periodicity is given$")
	public void term_and_periodicity_is_given() throws Throwable {
		apiParam.term ="8";
		apiParam.periodicity ="days";
		apiParam.requestType =apiParam.requestType + apiParam.currentDate +"/"+ apiParam.term +"/" + apiParam.periodicity;
	}
	
	
	
	
	
	@When("^INSTRUCTION ID IS GIVEN$")
	public void instruction_ID_IS_GIVEN() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.InstructionId;
	}

	@When("^Internal reference number is given$")
	public void internal_reference_number_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.internalReferenceNumber;
	}

	@When("^instruction is queued$")
	public void instruction_is_queued() throws Throwable {
		apiParam.setExecutionDate(java.time.LocalDateTime.now());
		apiParam.requestType = apiParam.requestType + apiParam.fundingSourceId +"/" + apiParam.executionDate;
	}

	@When("^file is created$")
	public void file_is_created() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.fileID +"/send";
	}

	@When("^file is sent$")
	public void file_is_sent() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.fileID +"/cancel";
	}

	@When("^File is rejected$")
	public void file_is_rejected() throws Throwable {

	}

	@When("^file is noc return type$")
	public void file_is_noc_return_type() throws Throwable {
		
	}

	@When("^refNum and status is given$")
	public void refnum_and_status_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.achreferenceNumber +"/" + apiParam.achstatus;
	}

	@When("^sourceId is given$")
	public void sourceid_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.fundingSourceId +"/" + apiParam.executionDate +"/" + apiParam.achstatus;
	}

	@When("^Internal ref num is given$")
	public void internal_ref_num_is_given() throws Throwable {
		apiParam.reason = "Account closed";
		apiParam.requestType = apiParam.requestType + apiParam.internalReferenceNumber+ "/reject";
	}

	@When("^paused payment ref num is given$")
	public void paused_payment_ref_num_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.achreferenceNumber+ "/pause";
	}

	@When("^Ref number is given for paused payment$")
	public void ref_number_is_given_for_paused_payment() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.achreferenceNumber+ "/pause";
	}

	@When("^payment is paused$")
	public void payment_is_paused() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.achreferenceNumber+ "/pause";
	}

	
	
	
	@When("^Get history entityType (.+)$")
	public void get_history(String entityType) throws Throwable {
		apiParam.emailEntityType = "application";
		apiParam.emailEntityId = "DV0007084";
		apiParam.requestType = apiParam.requestType + apiParam.emailEntityType +"/" +apiParam.emailEntityId;
	}

	@When("^History by templatename(.+)$")
	public void history_by_templateName(String templateName) throws Throwable {
		apiParam.requestType = "/"+ apiParam.emailEntityType + "/" + apiParam.emailEntityId+ apiParam.requestType + apiParam.emailTemplateName;
	}
	
	@When("^Sends template name <templateName> and <templateVersion>$")
	public void sends_template_name_templateName_and_templateVersion()throws Throwable {
		apiParam.emailTemplateVersion ="1.0";
		apiParam.requestType = apiParam.requestType + apiParam.emailTemplateName +"/" + apiParam.emailTemplateVersion;
	}

	@When("^Sends mail$")
	public void sends_mail() throws Throwable {
		apiParam.emailEntityId ="DV0007077";
		apiParam.requestType = "/" + apiParam.emailEntityType + "/" + apiParam.emailEntityId + apiParam.requestType;
	}

	@When("^Sends entityType (.+)$")
	public void send_by_entityType(String entityType) throws Throwable {
		apiParam.emailTemplateVersion ="1.0";
		apiParam.requestType = apiParam.requestType + apiParam.emailEntityType +"/" +apiParam.emailEntityId +"/" + apiParam.emailTemplateName +"/" +  apiParam.emailTemplateVersion;
		
	}
	
	
	

	
	
	@When("^Fact (.+) and source (.+) is given$")
	public void fact_and_source_is_given(String fact, String source) throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.verificationEntityType + "/" + apiParam.verificationEntityId + "/" + apiParam.workFlowStatusId + "/" + apiParam.fact + "/" + apiParam.method+ "/" + apiParam.verificationSource;
		
	}

	@When("^fact is  (.+)$")
	public void fact_is(String fact) throws Throwable {
		apiParam.verificationEntityType= "application";
		apiParam.verificationEntityId ="DV0007092";
		apiParam.workFlowStatusId = "LocApprovalFlow";
		apiParam.fact ="LexisNexisForeclosures";
		apiParam.requestType= "/" + apiParam.verificationEntityType + "/" + apiParam.verificationEntityId +"/" +apiParam.workFlowStatusId +"/"+ apiParam.fact + apiParam.requestType;
	}

	@When("^document is uploaded$")
	public void document_is_uploaded() throws Throwable {
		apiParam.entityId ="0002326";
		apiParam.fact="BankVerification";
		apiParam.requestType= "/" + apiParam.entityType + "/" + apiParam.entityId +"/" +apiParam.workFlowStatusId +"/"+ apiParam.fact + apiParam.requestType;
	}
	
	@When("^productID is (.+)$")
	public void productid_is(String productId) throws Throwable {
		apiParam.requestType= "/" + apiParam.verificationEntityType + "/" + apiParam.verificationEntityId + "/" +apiParam.productId;
	}
	
	
	@When("^ProductId is given$")
	public void productid_is_given() throws Throwable {
		apiParam.requestType = "/" + apiParam.verificationEntityType + "/" + apiParam.verificationEntityId  + apiParam.requestType + apiParam.productId;

	}

	
	@When("^productID (.+) and staus workflowid  (.+)  is given$")
	public void productid_and_stausworkflowid_is_given(String productId ,String workFlowStatusId) throws Throwable {
		apiParam.requestType ="/" + apiParam.productId + "/" +apiParam.workFlowStatusId + apiParam.requestType;

	}


	@When("^fact  (.+)  method name  (.+) is given$")
	public void fact_and_methodName_is_given(String fact ,String methodName) throws Throwable {
		apiParam.requestType ="/" + apiParam.verificationEntityType + "/" + apiParam.verificationEntityId +"/" +apiParam.workFlowStatusId +"/"+ apiParam.fact +'/' +apiParam.method + apiParam.requestType;

	}

	@When("^workflowstatusId is (.+)$")
	public void workflowstatusid_is(String workflowstatusId) throws Throwable {
		apiParam.requestType ="/" + apiParam.verificationEntityType + "/" + apiParam.verificationEntityId +"/" +apiParam.workFlowStatusId + apiParam.requestType;

	}

	
	@When("^To Initiate dynamic verification$")
	public void workflowstatusId_is() throws Throwable {
		apiParam.requestType = "/" + apiParam.verificationEntityType + "/" + apiParam.verificationEntityId +"/" +apiParam.workFlowStatusId + apiParam.requestType;
	}
	@When("^productId (.+) and fact (.+) is given$")
	public void productid_and_fact_is_given(String productId ,String fact) throws Throwable {
		apiParam.requestType = "/" + apiParam.productId+ "/" + apiParam.workFlowStatusId +"/" +apiParam.fact + apiParam.requestType;

	}

	@When("^documentId is (.+)$")
	public void documentid_is(String documentId) throws Throwable {
		apiParam.documentId =null;
		apiParam.verificationReason =null;
		apiParam.requestType = apiParam.requestType + apiParam.verificationEntityType+ "/" + apiParam.verificationEntityId +"/" +apiParam.workFlowStatusId+"/" +apiParam.fact +"/" + apiParam.documentId+"/" +apiParam.verificationStatus+"/" +apiParam.verificationReason;

	}

	@When("^verificationentityType is (.+)$")
	public void verificationEntitytype_is(String entityType) throws Throwable {
		apiParam.requestType ="/" + apiParam.verificationEntityType + "/" + apiParam.verificationEntityId  +apiParam.requestType;

	}
	
	@When("^fact given$")
	public void fact_given() throws Throwable {
		apiParam.requestType = "/" + apiParam.getVerificationEntityType() + "/" + apiParam.getVerificationEntityId() + "/" + apiParam.getWorkFlowStatusId() + "/" + apiParam.getFact() + apiParam.requestType;
	}

	@When("^config details$")
	public void config_details() throws Throwable {
	    
	}
	
	
	
	
	
	
	@When("^Alert is created$")
	public void alert_is_created() throws Throwable {
		apiParam.alertEntityType = "application";
		apiParam.alertEntityId ="ABC12344";
		apiParam.alertStatus ="Active";
		apiParam.requestType = apiParam.requestType + apiParam.alertEntityType + "/" + apiParam.alertEntityId +"/" + apiParam.alertStatus;
	}

	@When("^Alert Id is available$")
	public void alert_Id_is_available() throws Throwable {
		apiParam.requestType = "/" + apiParam.alertEntityType + "/" + apiParam.alertEntityId  + "/"  + apiParam .alertID + apiParam.requestType;
	}
	
	
	@When("^Created Alert$")
	public void created_Alert() throws Throwable {
		apiParam.requestType = "/" + apiParam.alertEntityType + "/" + apiParam.alertEntityId  + apiParam.requestType;
	}
	
	
	
	
	
	
	
	@When("^Workflow is given$")
	public void workflow_is_given() throws Throwable {
		apiParam.workflow ="account-creation";
		apiParam.requestType = apiParam.requestType + apiParam.workflow;
	}
	@When("^workflow has started$")
	public void workflow_has_started() throws Throwable {
		apiParam.requestType = "/"+ apiParam.workflow + apiParam.requestType ;
	}
	
	@When("^started for executing steps$")
	public void started_for_executing_steps() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.workflow +"/" + apiParam.appFlowID;
	}


	@When("^Workflow and id is given$")
	public void workflow_and_id_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.workflow +"/" + apiParam.appFlowID;
	}

	@When("^started the workflow$")
	public void started_the_workflow() throws Throwable {
		apiParam.requestType = "/" +apiParam.workflow +"/" + apiParam.appFlowID +apiParam.requestType;
	}

	@When("^executed the step$")
	public void executed_the_step() throws Throwable {
		apiParam.requestType = "/" +apiParam.workflow +"/" + apiParam.appFlowID + "/" + apiParam.step + apiParam.requestType;
	}
	
	
	
	
	
	
	
	@When("^SMS Template and entity is given$")
	public void sms_Template_and_entity_is_given() throws Throwable {
		apiParam.SMSentitytype= "application";
		apiParam.SMSentityid ="My0001";
		apiParam.SMStemplateName ="CommonSMS";
		apiParam.SMStemplateVersion ="1.0";
		apiParam.requestType = apiParam.requestType + apiParam.SMSentitytype + "/" + apiParam.SMSentityid +"/" + apiParam.SMStemplateName +"/" + apiParam.SMStemplateVersion;
	}

	@When("^SMS entity is given$")
	public void sms_entity_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.SMSentitytype + "/" + apiParam.SMSentityid;
	}
	
	
	
	
	
	

	@When("^Create UserOne with Role (.*)$")
	public void create_UserOne_with_Role(String role) throws Throwable {
		try {
			apiParam.username = ABtests.GenerateUsername();
			apiParam.password = "Sigma@123";
			apiParam.email = ABtests.GenerateEmail();
			apiParam.roles = role.split(" ");
			apiParam.requestType = apiParam.requestType;
			
		} finally {
			logger.info(
					"----------------------------------------- Prerequisite ------------------------------------------");
			logger.info(" UserName : " + apiParam.username);
			logger.info(" Email : " + apiParam.email);
			logger.info(" Request Type : " + apiParam.requestType);
			logger.info(" Port : " + apiParam.ASPort);
			logger.info(" roles = " + role);
			logger.info(
					"-------------------------------------------------------------------------------------------------");
		}
	}
	@When("^Create UserTwo with Role (.*)$")
	public void create_UserTwo_with_Role(String role) throws Throwable {
		try {
			apiParam.username = ABtests.GenerateUsername();
			apiParam.password = "Sigma@123";
			apiParam.email = ABtests.GenerateEmail();
			apiParam.roles = role.split("   ");
			apiParam.requestType = apiParam.requestType;
			
		} finally {
			logger.info(
					"----------------------------------------- Prerequisite ------------------------------------------");
			logger.info(" UserName : " + apiParam.username);
			logger.info(" Email : " + apiParam.email);
			logger.info(" Request Type : " + apiParam.requestType);
			logger.info(" Port : " + apiParam.ASPort);
			logger.info(" roles = " + role);
			logger.info(
					"-------------------------------------------------------------------------------------------------");
		}
	}
	@When("^UserOne Portal$")
	public void userone_Portal() throws Throwable {
		apiParam.portal ="back-office";
		apiParam.password = "Sigma@123";
	}


	@When("^UserTwo Portal$")
	public void usertwo_Portal() throws Throwable {
		apiParam.portal ="back-office";
		apiParam.password = "Sigma@123";
	}
	

	@When("^userOne is logged in$")
	public void userone_is_logged_in() throws Throwable {
		AuthorizationToken = apiParam.LoginToken1;
		apiParam.assignEntityType ="application";
		apiParam.assignEntityId = GenerateId;
		apiParam.requestType = "/" +apiParam.assignEntityType + "/" + apiParam.assignEntityId  + apiParam.requestType;
	}
	@When("^role is parent$")
	public void role_is_parent() throws Throwable {
		AuthorizationToken = apiParam.LoginToken1;
		apiParam.assignRole = "Junior Customer Agent";
		apiParam.requestType = "/" +apiParam.assignEntityType + "/" + apiParam.assignEntityId  + apiParam.requestType;  
	}

	@When("^userTwo is logged in$")
	public void usertwo_is_logged_in() throws Throwable {
		apiParam.assignFromRole = "Junior Customer Agent";
		apiParam.assignToRole ="CEO";
		AuthorizationToken = apiParam.LoginToken2;
		apiParam.requestType = "/" +apiParam.assignEntityType + "/" + apiParam.assignEntityId  + apiParam.requestType;
	}

	@When("^escalated by userTwo$")
	public void escalated_by_userTwo() throws Throwable {
		AuthorizationToken = apiParam.LoginToken1;
		apiParam.requestType = "/" +apiParam.assignEntityType + "/" + apiParam.assignEntityId  +"/escalations/"  + apiParam.assignEsclationId  + apiParam.requestType;
	
	}

	@When("^accepted by userOne$")
	public void accepted_by_userOne() throws Throwable {
		AuthorizationToken = apiParam.LoginToken1;
		apiParam.requestType = "/" +apiParam.assignEntityType + "/" + apiParam.assignEntityId  +"/escalations/"  + apiParam.assignEsclationId  + apiParam.requestType;
	}

	@When("^userOne has assigned the application$")
	public void userone_has_assigned_the_application() throws Throwable {
		apiParam.assignRole = "Junior Customer Agent";
		AuthorizationToken = apiParam.LoginToken1;
		apiParam.requestType = apiParam.requestType +apiParam.assignEntityType + "/" + apiParam.assignEntityId  + "/" +apiParam.assignRole;
	}

	@When("^User is given$")
	public void user_is_given() throws Throwable {
		AuthorizationToken = apiParam.LoginToken2;
		apiParam.requestType = "/" +apiParam.assignEntityType + apiParam.requestType + apiParam.assignRole;
	}
	@When("^assigned application by userOne$")
	public void assigned_application_by_userOne() throws Throwable {
		AuthorizationToken = apiParam.LoginToken1;
		apiParam.requestType = "/" +apiParam.assignEntityType + "/" + apiParam.assignEntityId  + apiParam.requestType;
	}


	
	
	
	
	
	@When("^Name and secondary name is given$")
	public void name_and_secondary_name_is_given() throws Throwable {
		apiParam.dataEntityType ="application";
		apiParam.dataEntityId ="12345678";
		apiParam.dataName = "TestData";
		apiParam.dataSecondaryName= "TestDataName";
		apiParam.requestType = apiParam.requestType + apiParam.dataEntityType + "/" + apiParam.dataEntityId + "/" + apiParam.dataName + "/" + apiParam.dataSecondaryName;

	}

	@When("^attribute is set$")
	public void attribute_is_set() throws Throwable {
		apiParam.requestType = "/" + apiParam.dataEntityType + "/" + apiParam.dataName + apiParam.requestType; 

	}

	@When("^attributes are given$")
	public void attributes_are_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.dataEntityType + "/" + apiParam.dataEntityId;
	}

	@When("^properties and attributes are set$")
	public void properties_and_attributes_are_set() throws Throwable {
		apiParam.requestType = "/" + apiParam.dataEntityType + "/" + apiParam.dataEntityId + apiParam.requestType; 
	}

	@When("^aatributes by name is set$")
	public void aatributes_by_name_is_set() throws Throwable {
		apiParam.requestType = "/" + apiParam.dataEntityType + "/" + apiParam.dataEntityId + apiParam.requestType; 
	}

	@When("^name is given$")
	public void name_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.dataEntityType + "/" + apiParam.dataEntityId + "/names/" + apiParam.dataName;
	}

	@When("^attribute is already present$")
	public void attribute_is_already_present() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.dataEntityType + "/" + apiParam.dataEntityId + "/" + apiParam.dataName;
	}

	
	
	
	
	
	
	@When("^Rule Name is given$")
	public void rule_Name_is_given() throws Throwable {
		apiParam.ruleName="alert_dummy_rule";
		apiParam.requestType = apiParam.requestType + apiParam.ruleName;
	}

	@When("^rule should be added$")
	public void rule_should_be_added() throws Throwable {
	  
	}
	
	
	
	
	
	
	@When("^template name is given$")
	public void template_name_is_given() throws Throwable {
		apiParam.tempName= "DocumentUploadEmail";
		apiParam.tempFormat="Html";
		apiParam.tempVersion="1.0";
		
	}

	@When("^template should be added$")
	public void template_should_be_added() throws Throwable {
		apiParam.requestType = "/"+ apiParam.tempName +"/" + apiParam.tempVersion +"/" + apiParam.tempFormat + apiParam.requestType ;
	}

	@When("^version and format is given$")
	public void version_and_format_is_given() throws Throwable {
		apiParam.requestType = "/"+ apiParam.tempName +"/" + apiParam.tempVersion +"/" + apiParam.tempFormat + apiParam.requestType ;
	}

	@When("^Template version and format is given$")
	public void template_version_and_format_is_given() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.tempName +"/" + apiParam.tempVersion +"/" + apiParam.tempFormat;
	}
	
	
	
	
	
	
	
	@When("^Token is issued$")
	public void token_is_issued() throws Throwable {
	    apiParam.tokenEntityType ="application";
	    apiParam.tokenEntityId ="0000932";
	}
	
	
	
	
	
	
	@When("^application is recieved$")
	public void application_is_recieved() throws Throwable {
		apiParam.statusManagementEntitytype ="application";
		apiParam.statusManagementEntityid = GenerateId;
		apiParam.statusManagementProductId ="loc";
		apiParam.statusManagementStatusWorkFlowId= "LocApprovalFlow";
		apiParam.statusManagementParentStatus ="200.01";
		apiParam.statusManagementSubWorkFlowId ="ChargeOff";
		apiParam.statusManagementSubWorkflowStatus ="200.20.02";
		apiParam.statusManagementNewStatus="400.01";
		apiParam.requestType ="/" + apiParam.statusManagementEntitytype + "/" + apiParam.statusManagementEntityid + "/" + apiParam.statusManagementProductId + apiParam.requestType + apiParam.statusManagementStatusWorkFlowId + "/" +apiParam.statusManagementParentStatus;	
	}
	@When("^workflow is processed$")
	public void workflow_is_processed() throws Throwable {
		apiParam.requestType ="/" + apiParam.statusManagementEntitytype + "/" + apiParam.statusManagementEntityid + apiParam.requestType + apiParam.statusManagementSubWorkFlowId;
	}

	@When("^Intiated subworkflow$")
	public void intiated_subworkflow() throws Throwable {
		apiParam.requestType = apiParam.requestType +apiParam.statusManagementEntitytype + "/" +apiParam.statusManagementEntityid +"/" +apiParam.statusManagementStatusWorkFlowId +"/" + apiParam.statusManagementSubWorkFlowId + "/" +apiParam.statusManagementSubWorkflowStatus;
	}

	@When("^statusWorkFlowId & status is given$")
	public void statusworkflowid_status_is_given() throws Throwable {
		apiParam.requestType = "/" +apiParam.statusManagementEntitytype + "/" +apiParam.statusManagementEntityid + "/" +apiParam.statusManagementStatusWorkFlowId + "/" +apiParam.statusManagementParentStatus + apiParam.requestType;
	}

	@When("^statusWorkFlowId is given$")
	public void statusworkflowid_is_given() throws Throwable {
		apiParam.requestType = "/" +apiParam.statusManagementEntitytype + "/" +apiParam.statusManagementStatusWorkFlowId + "/" +apiParam.statusManagementParentStatus + apiParam.requestType;
	}

	@When("^substatus is given$")
	public void substatus_is_given() throws Throwable {
		apiParam.requestType = "/" +apiParam.statusManagementEntitytype + "/" +apiParam.statusManagementStatusWorkFlowId + "/" +apiParam.statusManagementParentStatus + "/" +apiParam.statusManagementSubWorkFlowId + "/" +apiParam.statusManagementSubWorkflowStatus +apiParam.requestType;
	}

	@When("^entityType & entityId is given$")
	public void entitytype_entityId_is_given() throws Throwable {
		apiParam.requestType = "/" +apiParam.statusManagementEntitytype + "/" +apiParam.statusManagementEntityid + apiParam.requestType;
	}

	@When("^productId is given to get history$")
	public void productid_is_given_to_get_history() throws Throwable {
		apiParam.requestType = "/" +apiParam.statusManagementEntitytype + "/" +apiParam.statusManagementEntityid + apiParam.requestType + apiParam.statusManagementProductId;
	}

	@When("^statusWorkFlowId is given to get status$")
	public void statusworkflowid_is_given_to_get_status() throws Throwable {
		apiParam.requestType = apiParam.requestType +apiParam.statusManagementEntitytype + "/" +apiParam.statusManagementEntityid +"/" +apiParam.statusManagementStatusWorkFlowId ;
	}

	@When("^status management entitytype is given$")
	public void status_management_entitytype_is_given() throws Throwable {
		apiParam.requestType = "/" +apiParam.statusManagementEntitytype +apiParam.requestType;
	}

	@When("^productId is given to get status by product$")
	public void productid_is_given_to_get_status_by_product() throws Throwable {
		apiParam.requestType ="/" + apiParam.statusManagementEntitytype + "/" +apiParam.statusManagementEntityid + "/" +apiParam.statusManagementProductId + apiParam.requestType;
	}

	@When("^application needs to change status$")
	public void application_needs_to_change_status() throws Throwable {
		apiParam.requestType = apiParam.requestType +apiParam.statusManagementEntitytype +"/" + apiParam.statusManagementEntityid +"/" +apiParam.statusManagementStatusWorkFlowId + "/" +apiParam.statusManagementNewStatus;
	}
	
	
	
	
	@When("^EventStore Id is given$")
	public void eventstore_Id_is_given() throws Throwable {
		apiParam.eventStoreId ="5b03b7fca65c8a0001923cfa";
		apiParam.requestType = apiParam.requestType + apiParam.eventStoreId;
	}
	
	
	
	
	
	
	@When("^ProductRule ProductId is given$")
	public void productrule_productid_is_given() throws Throwable {
		apiParam.productRuleEntityType ="application";
		apiParam.productRuleEntityId = "12345678";
		apiParam.productRuleProductId = "loc";
		apiParam.requestType =  "/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + apiParam.requestType;   
	}

	@When("^Added rule defination$")
	public void added_rule_defination() throws Throwable {	   
		apiParam.requestType = "/" + apiParam.productRuleRuleId + apiParam.requestType;
	}

	@When("^ruleType is given$")
	public void ruletype_is_given() throws Throwable {
		apiParam.requestType = "/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/" + apiParam.productRuleRuleType +apiParam.requestType;
	}

	@When("^ruleName is given$")
	public void rulename_is_given() throws Throwable {
		apiParam.requestType = "/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/" + apiParam.productRuleRuleName +apiParam.requestType;  
	}

	@When("^className is given$")
	public void classname_is_given() throws Throwable {
		apiParam.requestType =  "/" + apiParam.productRuleEntityType + apiParam.requestType  +  apiParam.productRuleClassName+"/" + apiParam.productRuleProductId;
	}

	@When("^productId and ruleName is given$")
	public void productid_and_ruleName_is_given() throws Throwable {
		apiParam.requestType = "/" + apiParam.productRuleEntityType +"/" +apiParam.productRuleEntityId +"/"+ apiParam.productRuleProductId + "/" + apiParam.productRuleRuleName +apiParam.requestType;  
	}

	@When("^added score card$")
	public void added_score_card() throws Throwable {
		apiParam.requestType = "/" + apiParam.productRuleScoreCardId + apiParam.requestType;
	}

	@When("^productId and scoreCardId is given$")
	public void productid_and_scoreCardId_is_given() throws Throwable {
		apiParam.requestType =  "/" + apiParam.productRuleEntityType +"/" +apiParam.productRuleEntityId +"/"+ apiParam.productRuleProductId + "/" + apiParam.productRuleScoreCardId +apiParam.requestType; 
	}

	@When("^scorecardid is given$")
	public void scorecardid_is_given() throws Throwable {
		apiParam.requestType =   "/" + apiParam.productRuleScoreCardId +apiParam.requestType; 
	}

	@When("^added score card variable$")
	public void added_score_card_variable() throws Throwable {
		apiParam.requestType =  "/" + apiParam.productRuleScoreCardId +"/"+ apiParam.productRuleScoreCardVariableId +apiParam.requestType; 
	}

	@When("^scorecard name is given$")
	public void scorecard_name_is_given() throws Throwable {
		apiParam.requestType =   "/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/" + apiParam.productRuleScoreCardName + apiParam.requestType; 
	}

	@When("^score card by entity type$")
	public void score_card_by_entity_type() throws Throwable {
		apiParam.requestType =  "/" + apiParam.productRuleEntityType + apiParam.requestType;
	}

	@When("^entity type, product id and score card id  is given$")
	public void entity_type_product_id_and_score_card_id_is_given() throws Throwable {
		apiParam.requestType =   "/" + apiParam.productRuleEntityType + "/" + apiParam.productRuleProductId + "/" + apiParam.productRuleScoreCardId +apiParam.requestType;
	}

	@When("^added pricing strategy$")
	public void added_pricing_strategy() throws Throwable {
		apiParam.requestType = "/" + apiParam.productRulePricingStrategyId + apiParam.requestType;   
	}

	@When("^productId and pricingStrategyName is given$")
	public void productid_and_pricingStrategyName_is_given() throws Throwable {
		apiParam.requestType =  "/" + apiParam.productRuleEntityType +"/" +apiParam.productRuleEntityId +"/"+ apiParam.productRuleProductId + "/" + apiParam.productRulePricingStrategyName+ "/" + apiParam.productRulePricingStrategyVersion +apiParam.requestType;
	}

	@When("^all pricing strategy$")
	public void all_pricing_strategy() throws Throwable {
		apiParam.requestType =   "/" + apiParam.productRuleEntityType + apiParam.requestType;
	}

	@When("^strategy history$")
	public void strategy_history() throws Throwable {
		apiParam.requestType =    "/" + apiParam.productRuleEntityType +"/" +apiParam.productRuleEntityId +"/"+ apiParam.productRuleProductId + "/" + apiParam.productRulePricingStrategyId +apiParam.requestType;
	}

	@When("^princing strategyId is given$")
	public void princing_strategyId_is_given() throws Throwable {
		apiParam.requestType =   "/" + apiParam.productRulePricingStrategyId +apiParam.requestType; 
	}


	@When("^added Pricindg strategy step$")
	public void added_Pricindg_strategy_step() throws Throwable {
		apiParam.requestType =  "/" + apiParam.productRulePricingStrategyId +"/"+ apiParam.productRulePricingStrategyStepId +apiParam.requestType;   
	}

	@When("^added segment$")
	public void added_segment() throws Throwable {
		apiParam.requestType =   "/" + apiParam.productRuleSegmentId +apiParam.requestType; 
	}

	@When("^all segments$")
	public void all_segments() throws Throwable {
		apiParam.requestType =  "/" + apiParam.productRuleEntityType + apiParam.requestType;
	}

	@When("^segment name is given$")
	public void segment_name_is_given() throws Throwable {
		apiParam.requestType =  "/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/" + apiParam.productRuleSegmentName +apiParam.requestType; 
	}

	@When("^product id with segmentid is given$")
	public void product_id_with_segmentid_is_given() throws Throwable {
		apiParam.requestType =   "/" + apiParam.productRuleEntityType +"/" +apiParam.productRuleEntityId +"/"+ apiParam.productRuleProductId + "/" + apiParam.productRuleSegmentId+ apiParam.requestType;
	}

	@When("^Added lookup$")
	public void added_lookup() throws Throwable {
		apiParam.requestType =   "/" + apiParam.productRuleLookupId+ apiParam.requestType; 
	}
 
	@When("^lookup name is given$")
	public void lookup_name_is_given() throws Throwable {
		apiParam.requestType =  "/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/" + apiParam.productRuleLookupName +apiParam.requestType; 
	}

	@When("^lookup data by entity type$")
	public void lookup_data_by_entity_type() throws Throwable {
		apiParam.requestType =   "/" + apiParam.productRuleEntityType + apiParam.requestType;
	}

	@When("^product id with lookup id is given$")
	public void product_id_with_lookup_id_is_given() throws Throwable {
		apiParam.requestType = "/" + apiParam.productRuleEntityType +"/" +apiParam.productRuleEntityId +"/"+ apiParam.productRuleProductId + "/" + apiParam.productRuleLookupId+ apiParam.requestType;  
	}

	@When("^added template$")
	public void added_template() throws Throwable {
		apiParam.requestType =   "/" + apiParam.productRuleTemplateId + apiParam.requestType; 
	}
	@When("^template data by entity type$")
	public void template_data_by_entity_type() throws Throwable {
		apiParam.requestType =   "/" + apiParam.productRuleEntityType + apiParam.requestType;
	}

	@When("^Event name is given$")
	public void event_name_is_given() throws Throwable {
		apiParam.requestType = "/" + apiParam.productRuleEntityType +"/" + apiParam.productRuleProductId + "/" + apiParam.productRuleTemplateName +apiParam.requestType;  
	}

	@When("^entity type and entity id is given$")
	public void entity_type_and_entity_id_is_given() throws Throwable {
		apiParam.requestType =   apiParam.requestType +"application/12345678";
	}
	
	
	
	
	
	
	
	@When("^emailId is given$")
	public void emailid_is_given() throws Throwable {
		apiParam.emailVeriEntityType="application";
		apiParam.emailVeriEntityId =ABtests.GenerateEntityId();
		apiParam.verificationEmailId = ABtests.GenerateEmail();
		apiParam.emailName = ABtests.GenerateUsername();
		apiParam.requestType = apiParam.requestType + apiParam.emailVeriEntityType + "/" + apiParam.emailVeriEntityId;
	}
	
	public void ExtractCode() {
		MongoClient mongo = new MongoClient( "10.100.0.21" , 27017 ); 
        DB db = mongo.getDB("email-verification");
        DBCollection collection = db.getCollection("email-verification");
        BasicDBObject query = new BasicDBObject();
        query.put("EntityId", apiParam.emailVeriEntityId);
        DBCursor cursor = collection.find(query);
        while(cursor.hasNext()){
            DBObject result= cursor.next();
            JSONObject output = new JSONObject(JSON.serialize(result));
            apiParam.emailVerificationCode = (String) output.get("Code");
        }

	}
	
	@When("^emailId is Intiated$")
	public void emailid_is_Intiated() throws Throwable {
		ExtractCode();
		apiParam.requestType =  apiParam.requestType + apiParam.emailVerificationCode ;
	}

	@When("^email is verified$")
	public void email_is_verified() throws Throwable {
		apiParam.requestType = apiParam.requestType + apiParam.verificationEmailId ;
		
	}
	
	
	
	@When("^Log Entries with ProductId Is Given$")
	public void log_Entries_with_ProductId_Is_Given() throws Throwable {
		apiParam.activityLogEntityType = "application";
		apiParam.activityLogEntityId ="0002326";
		apiParam.activityLogProductId="loc";
		apiParam.activityLogFacts="Facts";
		apiParam.requestType = "/" + apiParam.activityLogEntityType + "/" + apiParam.activityLogEntityId + apiParam.requestType + apiParam.activityLogProductId;
	    
	}

	@When("^activityLog ProductId is given$")
	public void activityLog_productid_is_given() throws Throwable {
		apiParam.requestType = "/" + apiParam.activityLogEntityType + "/" + apiParam.activityLogEntityId  + "/"+ apiParam.activityLogProductId + apiParam.requestType; 
	    
	}

	@When("^Log Entries without ProductId$")
	public void log_Entries_without_ProductId() throws Throwable {
		apiParam.requestType = apiParam.requestType  + apiParam.activityLogEntityType + "/" + apiParam.activityLogEntityId;  
	    
	}

	@When("^Without ProductId$")
	public void without_ProductId() throws Throwable {
		apiParam.requestType = "/"  + apiParam.activityLogEntityType + "/" + apiParam.activityLogEntityId + apiParam.requestType;   
	    
	}

	@When("^tags withProductId$")
	public void tags_withProductId() throws Throwable {
		apiParam.requestType = "/" + apiParam.activityLogEntityType + "/" + apiParam.activityLogEntityId  + "/"+ apiParam.activityLogProductId + apiParam.requestType + apiParam.activityLogFacts; 
	    
	}

	@When("^tags without ProductId$")
	public void tags_without_ProductId() throws Throwable {
		apiParam.requestType = "/" + apiParam.activityLogEntityType + "/" + apiParam.activityLogEntityId   + apiParam.requestType + apiParam.activityLogFacts; 
	    
	}
	


	
	
	


	@After
	public void AftereHook() throws IOException, InterruptedException, Throwable {

	}

}