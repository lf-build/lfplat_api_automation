package net.sigmainfo.lf.automation.common;

import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.HttpURLConnection;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.testng.Assert.fail;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;

import net.sigmainfo.lf.automation.api.constant.ApiParam;
import net.sigmainfo.lf.automation.listener.Reporter;

import org.openqa.selenium.Proxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.CaptureType;

@TestExecutionListeners(inheritListeners = false, listeners = { DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class })
@WebAppConfiguration
@Test
public class AbstractTests extends AbstractTestNGSpringContextTests {

	private static Logger logger = LoggerFactory.getLogger(AbstractTests.class);

	private static boolean setupDone = false;

	@Autowired
	ApiParam apiParam;

	public static int WAIT_TIME = 60;
	public static Properties prop = new Properties();
	public int INCORRECT_AUTHORIZATION = 401;
	public int INTERNAL_SERVER_ERROR = 500;
	public int BAD_REQUEST = 400;
	public int NOT_FOUND = 404;
	public int SUCCESS_RESPONSE = 200;
	public int SUCCESS_RESPONSE_WITH_NOCONTENT = 204;
	public int METHOD_NOT_ALLOWED = 405;

	public void postConstruct(String env) throws Exception {
		if (!setupDone) {
			logger.info("=========================== Post Construct Invoked. ==============================");
			setupDBParams(env);
			setupDone = true;
		}
	}

	public void setupDBParams(String env) throws FileNotFoundException, IOException {

		logger.info("------------------------------- READING API PROPERTIES FILE -----------------------------------");
		logger.info("Environment : " + env);
		String environmentenvironment = env + "/api.properties";
		logger.info("Configuration file path  : " + environmentenvironment);
		prop.load(new FileInputStream(environmentenvironment));
		logger.info("------------------------------------------------------------------------------------------------");

		logger.info("-----------------------  READING API Detail ---------------------------------");
		apiParam.baseresturl = prop.getProperty("baseresturl");
		logger.info("apiParam.baseresturl : " + apiParam.baseresturl);
		apiParam.AuthToken = prop.getProperty("AuthToken");
		logger.info("apiParam.AuthToken : " + apiParam.AuthToken);
		apiParam.contentType = prop.getProperty("contentType");
		logger.info("apiParam.contentType : " + apiParam.contentType);
		logger.info("-------------------------------------------------------------------------------------");

	}

	
	/* Generate Random subentityid */
	public String GenerateNumbers() {
		int count = 7;
		final String NUMERIC_STRING = "0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * NUMERIC_STRING.length());
			builder.append(NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}
	
	/* Generate Random Username */
	public String GenerateUsername() {
		int count = 4;
		final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return "TestUser_"+builder.toString();
	}
	
	/* Generate Random Email */
	public String GenerateEmail() {
		int count = 4;
		final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return "apitestmail_"+builder.toString()+"@testmailer.com";
	}
	
	
	/* Generate EntityId */
	public String GenerateEntityId() {
		int count = 7;
		final String ALPHA_NUMERIC_STRING = "0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}
	
	/* Generate Random Name */
	public String GenerateName() {
		int count = 8;
		final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

}
